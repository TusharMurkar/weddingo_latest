<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <!--[if ie]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <title>Weddingo - Online Wedding Invitation Card</title>
    <link rel="icon" href="../images/Favicon/android-icon-192x192.png" type="image/gif">
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <!--    <link rel="alternate" type="application/rss+xml" title="Nikah &raquo; Feed" href="https://www.weddingo.in/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Nikah &raquo; Comments Feed" href="https://www.weddingo.in/comments/feed/" /> 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->

    <style>
        /*menu styling*/

        /*! CSS Used from: Embedded */

        .weddingo-demo {
            background-color: #21b552;
            padding: 10px 15px !important;
            border-radius: 25px !important;
            color: #fff !important;
        }

        .logo-img {
            height: 30px;
        }

        /*! CSS Used from: http://localhost/weddingo_landingpage/wp-includes/css/contact-styles.min.css ; media=all */

        @media all {
            @media all {

                .sm,
                .sm a,
                .sm li,
                .sm:after,
                header,
                nav {
                    display: block;
                }

                .menu__link,
                .sm,
                .sm li {
                    -webkit-tap-highlight-color: transparent;
                }

                a,
                div,
                header,
                img,
                label,
                li,
                nav,
                span,
                ul {
                    margin: 0;
                    padding: 0;
                    border: 0;
                    font-size: 100%;
                    vertical-align: baseline;
                }

                .sm,
                .sm li {
                    list-style: none;
                    margin: 0;
                    padding: 0;
                    line-height: normal;
                    direction: ltr;
                    text-align: left;
                }

                .sm a,
                .sm li {
                    position: relative;
                }

                .sm:after {
                    content: "\00a0";
                    height: 0;
                    font: 0/0 serif;
                    clear: both;
                    visibility: hidden;
                    overflow: hidden;
                }

                .sm,
                .sm *,
                .sm :after,
                .sm :before {
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .sm-clean>li:first-child>a {
                    -webkit-border-radius: 5px 5px 0 0;
                    -moz-border-radius: 5px 5px 0 0;
                    -ms-border-radius: 5px 5px 0 0;
                    -o-border-radius: 5px 5px 0 0;
                    border-radius: 5px 5px 0 0;
                }

                .sm-clean>li:last-child>a {
                    -webkit-border-radius: 0 0 5px 5px;
                    -moz-border-radius: 0 0 5px 5px;
                    -ms-border-radius: 0 0 5px 5px;
                    -o-border-radius: 0 0 5px 5px;
                    border-radius: 0 0 5px 5px;
                }

                .sm-clean li {
                    border-top: 1px solid rgba(0, 0, 0, .05);
                }

                .sm-clean>li:first-child {
                    border-top: 0;
                }

                @media (min-width:769px) {
                    .sm-clean li {
                        float: left;
                        border-top: 0;
                    }

                    .sm-clean a {
                        white-space: nowrap;
                    }

                    .sm-clean a:active,
                    .sm-clean a:focus,
                    .sm-clean a:hover {
                        color: #d23600;
                    }
                }

                .animated {
                    -webkit-animation-duration: 1s;
                    animation-duration: 1s;
                    -webkit-animation-fill-mode: both;
                    animation-fill-mode: both;
                }

                .fadeInDown {
                    -webkit-animation-name: fadeInDown;
                    animation-name: fadeInDown;
                }

                .menu__link {
                    cursor: pointer;
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                    -webkit-touch-callout: none;
                    -khtml-user-select: none;
                }

                .menu__link:focus,
                .menu__link:hover {
                    outline: 0;
                }

                .header-style-1-wrap .menu--ferdinand .menu__link {
                    -webkit-transition: background .3s, color .3s;
                    transition: background .3s, color .3s;
                }

                .header-style-1-wrap .menu--ferdinand .menu__link::before {
                    content: '';
                    position: absolute;
                    bottom: 0;
                    left: 0;
                    width: 100%;
                    height: 2px;
                    background: #d94f5c;
                    -webkit-transform: scale3d(0, 5, 1);
                    transform: scale3d(0, 5, 1);
                    -webkit-transform-origin: 0 50%;
                    transform-origin: 0 50%;
                    -webkit-transition: -webkit-transform .3s;
                    transition: transform .3s;
                    -webkit-transition-timing-function: cubic-bezier(1, .68, .16, .9);
                    transition-timing-function: cubic-bezier(1, .68, .16, .9);
                }

                .header-style-1-wrap .menu--ferdinand .current-menu-item .menu__link::before,
                .header-style-1-wrap .menu--ferdinand .menu__item:hover .menu__link::before {
                    -webkit-transform: scale3d(1, 1, 1);
                    transform: scale3d(1, 1, 1);
                }

                a {
                    color: #000;
                }

                a {
                    -webkit-transition: all .2s ease-in-out;
                    -o-transition: all .2s ease-in-out;
                }

                a,
                input {
                    outline: 0;
                }

                input {
                    border-width: 1px;
                    border-style: solid;
                    background-color: #EFEFEF;
                    border-color: #EFEFEF;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                }

                #header .fl.horizontal .head-item,
                .header-style-1-wrap .fl,
                .logo {
                    float: left;
                }

                .sm-clean a {
                    font-family: lorabold;
                }

                img {
                    display: block;
                    max-width: 100%;
                    height: auto;
                }

                ul {
                    padding-left: 40px;
                    -webkit-padding-start: 40px;
                    -moz-padding-start: 40px;
                    padding-start: 40px;
                }

                a {
                    cursor: pointer;
                    text-decoration: none !important;
                    transition: all .2s ease-in-out;
                }

                a:active,
                a:focus,
                a:hover {
                    color: #666;
                    text-decoration: none;
                    outline: 0;
                }

                ::-moz-selection {
                    background-color: #000;
                    color: #fff;
                }

                ::selection {
                    background-color: #000;
                    color: #fff;
                }

                input {
                    height: 40px;
                    padding: 0 15px;
                    box-sizing: border-box;
                    max-width: 100%;
                }

                .container {
                    padding-right: 15px;
                    padding-left: 15px;
                    max-width: 100%;
                }

                ::-ms-clear {
                    display: none;
                }

                .clearfix:after,
                .clearfix:before {
                    content: "";
                    display: table;
                }

                .clearfix:after {
                    clear: both;
                }

                .clearfix {
                    zoom: 1;
                }

                .container {
                    width: 1200px;
                    margin: 0 auto;
                }

                .main-menu-btn {
                    position: relative;
                    width: 28px;
                    height: 28px;
                    text-indent: 28px;
                    white-space: nowrap;
                    overflow: hidden;
                    cursor: pointer;
                    -webkit-tap-highlight-color: transparent;
                    display: none;
                }

                #header {
                    max-width: 100%;
                }

                #header .header-clear {
                    display: -webkit-flex;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-flex-wrap: wrap;
                    -ms-flex-wrap: wrap;
                    flex-wrap: wrap;
                    webkit-align-items: flex-start;
                    -ms-flex-align: start;
                    align-items: flex-start;
                }

                .header-style-1-wrap .header-clear .fl {
                    max-width: 100%;
                    min-width: 0;
                    overflow: hidden;
                    margin: 0 auto 0 0;
                }

                #header.inner-head-wrap {
                    margin-left: auto;
                    margin-right: auto;
                }

                .header-style-1-wrap .header-clear .fr {
                    display: block;
                    -webkit-flex: 0 1 auto;
                    -ms-flex: 0 1 auto;
                    flex: 0 1 auto;
                }

                .main-menu-btn-icon,
                .main-menu-btn-icon:after,
                .main-menu-btn-icon:before {
                    position: absolute;
                    top: 50%;
                    left: 2px;
                    height: 2px;
                    width: 24px;
                    background: #bbb;
                    -webkit-transition: all .25s;
                    transition: all .25s;
                }

                .main-menu-btn-icon:before {
                    content: '';
                    top: -7px;
                    left: 0;
                }

                .main-menu-btn-icon:after {
                    content: '';
                    top: 7px;
                    left: 0;
                }

                #main-menu-state:checked~.main-menu-btn .main-menu-btn-icon {
                    height: 0;
                    background: 0 0;
                }

                #main-menu-state:checked~.main-menu-btn .main-menu-btn-icon:before {
                    top: 0;
                    -webkit-transform: rotate(-45deg);
                    transform: rotate(-45deg);
                }

                #main-menu-state:checked~.main-menu-btn .main-menu-btn-icon:after {
                    top: 0;
                    -webkit-transform: rotate(45deg);
                    transform: rotate(45deg);
                }

                #main-menu-state {
                    position: absolute;
                    width: 1px;
                    height: 1px;
                    margin: -1px;
                    border: 0;
                    padding: 0;
                    overflow: hidden;
                    clip: rect(1px, 1px, 1px, 1px);
                    visibility: hidden;
                }

                #header.inner-head-wrap {
                    z-index: 101;
                    position: relative;
                }

                .header-clear {
                    position: relative;
                    max-width: 100%;
                }

                .head-item {
                    margin-right: 20px;
                }

                .head-item:last-child {
                    margin-right: 0;
                }

                .logo {
                    position: relative;
                    z-index: 100;
                }

                .header-style-1-wrap .fr {
                    float: right;
                }

                .header-style-1-wrap .main-menu {
                    float: left;
                }

                .header-style-1-wrap .main-menu ul.sm-clean>li+li {
                    margin-left: 30px;
                }

                .header-style-1-wrap .main-menu ul.sm-clean>li {
                    padding-top: 10px;
                    padding-bottom: 10px;
                }

                .header-style-1-wrap .main-menu ul.sm-clean>li>a {
                    font-size: 16px;
                    color: #000;
                    position: relative;
                    display: block;
                    padding: .65em 0;
                    -webkit-transition: background .3s, color .3s;
                    transition: background .3s, color .3s;
                    transition: opacity .2s linear, color .2s linear;
                }
            }

            header#header.inner-head-wrap {
                background-repeat: no-repeat;
                background-attachment: inherit;
                background-position: center center;
                background-size: inherit;
                background-image: none;
            }

            @media all {
                @media only screen and (max-width: 1140px) {
                    .main-wrapper .container {
                        width: auto;
                        padding: 0 30px;
                        box-sizing: border-box;
                    }

                    .header-style-1-wrap .main-menu ul.sm-clean>li {
                        margin-right: 20px;
                    }
                }

                @media only screen and (max-width:1024px) {
                    .main-wrapper #header .container {
                        width: auto;
                        padding: 0 30px;
                        box-sizing: border-box;
                    }
                }

                @media only screen and (max-width:768px) {
                    .main-wrapper #header .container {
                        width: 100%;
                        padding: 0 30px;
                        box-sizing: border-box;
                    }

                    .header-style-1-wrap .fl,
                    .header-style-1-wrap .fr {
                        float: none;
                        width: 100%;
                    }

                    .header-style-1-wrap .header-clear .fr {
                        width: 100%;
                    }

                    .header-style-1-wrap .fl {
                        text-align: center;
                    }

                    #header.header-style-1-wrap .logo {
                        display: inline-block;
                        float: none;
                        text-align: center;
                    }

                    .header-style-1-wrap .main-menu {
                        display: block;
                        width: 100%;
                    }

                    .main-menu-btn {
                        position: absolute;
                        z-index: 2;
                        top: 0;
                        right: 0;
                        display: block;
                    }

                    .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a::before,
                    .header-style-1-wrap .main-menu ul.sm-clean>li>a::after,
                    .header-style-1-wrap .main-menu ul.sm-clean>li>a::before,
                    ul.sm-clean {
                        display: none;
                    }

                    .main-menu {
                        margin-top: 30px;
                    }

                    #main-wrapper #header .container {
                        padding: 0;
                    }

                    #primary-menu ul.sm-clean>li {
                        margin-left: 0;
                        padding: 0;
                        border-top: 0;
                    }

                    #primary-menu ul.sm-clean>li>a {
                        font-size: 14px;
                        padding: 12px 50px 13px;
                        text-align: center;
                    }
                }
            }

            #header .container,
            header#header .container {
                width: 1170px;
            }

            #header .logo-image {
                padding-top: 0;
                padding-bottom: 0;
            }

            header#header.inner-head-wrap {
                -webkit-transition: all .5s ease 0s;
                -moz-transition: all .5s ease 0s;
                transition: all .5s ease 0s;
                background-color: transparent;
                padding: 50px 0;
            }

            .fl.horizontal .head-item {
                margin-left: 0;
                margin-right: 0;
            }

            .fr .head-item {
                padding-top: 0 !important;
                padding-bottom: 0 !important;
            }

            @media only screen and (max-width:768px) {
                header#header.inner-head-wrap {
                    padding-left: 30px;
                    padding-right: 30px;
                }

                header#header .logo.head-item {
                    margin-top: 0;
                    margin-bottom: 0;
                }

                header#header .main-menu-btn {
                    margin-top: 15px !important;
                    margin-bottom: 0 !important;
                }
            }

            @media only screen and (max-width:768px) {
                #main-wrapper #header #primary-menu li a {
                    color: #fff;
                    font-weight: 500;
                    text-align: center;
                    font-size: 16px;
                    line-height: 25.6px;
                    word-spacing: 0;
                    letter-spacing: 0;
                }
            }

            .header-style-1-wrap {
                background: #f7f3f3;
            }

            .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a,
            .header-style-1-wrap .main-menu ul.sm-clean>li>a,
            .header-style-1-wrap .main-menu ul.sm-clean>li>a:active,
            .header-style-1-wrap .main-menu ul.sm-clean>li>a:hover {
                color: #000;
            }

            .header-style-1-wrap .main-menu ul.sm-clean>li>a:hover,
            .header-style-1-wrap .menu__item:hover .menu__link {
                color: #111;
            }

            .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a::after,
            .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a::before,
            .header-style-1-wrap .main-menu ul.sm-clean>li::after,
            .header-style-1-wrap .main-menu ul.sm-clean>li::before,
            .header-style-1-wrap .main-menu ul.sm-clean>li>a::after,
            .header-style-1-wrap .main-menu ul.sm-clean>li>a::before {
                background-color: #000;
                border-color: #000;
            }

            @media all {
                .fadeInDown {
                    animation-name: fadeInDown;
                }

                .animated {
                    -webkit-animation-duration: 1.25s;
                    animation-duration: 1.25s;
                }
            }

            #header #primary-menu li a {
                font-family: Poppins, Georgia, serif;
                text-align: inherit;
                line-height: 25.6px;
                font-weight: 500;
                font-style: normal;
                font-size: 16px;
            }

            .logo-img {
                height: 30px;
            }

            .weddingo-demo {
                background-color: #21b552;
                padding: 10px 15px !important;
                border-radius: 25px !important;
                color: #fff !important;
            }
        }

        /*! CSS Used from: http://localhost/weddingo_landingpage/wp-content/themes/nikah/css/responsive.css ; media=all */

        @media all {
            @media only screen and (max-width: 1140px) {
                .main-wrapper .container {
                    width: auto;
                    padding: 0 30px 0 30px;
                    box-sizing: border-box;
                }

                .header-style-1-wrap .main-menu ul.sm-clean>li {
                    margin-right: 20px;
                }
            }

            @media only screen and (max-width: 1024px) {
                .main-wrapper #header .container {
                    width: auto;
                    padding: 0 30px 0 30px;
                    box-sizing: border-box;
                }
            }

            @media only screen and (max-width: 768px) {
                .main-wrapper #header .container {
                    width: 100%;
                    padding: 0 30px 0 30px;
                    box-sizing: border-box;
                }

                .header-style-1-wrap .fr,
                .header-style-1-wrap .fl {
                    float: none;
                }

                .header-style-1-wrap .header-clear .fr {
                    width: 100%;
                }

                .header-style-1-wrap .fl {
                    text-align: center;
                }

                #header.header-style-1-wrap .logo {
                    display: inline-block;
                }

                .header-style-1-wrap .main-menu {
                    display: block;
                    width: 100%;
                }

                .header-style-1-wrap .fr,
                .header-style-1-wrap .fl {
                    float: none;
                    width: 100%;
                }

                .main-menu-btn {
                    position: absolute;
                    z-index: 2;
                    top: 0;
                    right: 0;
                    display: block;
                }

                #header.header-style-1-wrap .logo {
                    float: none;
                    text-align: center;
                }

                .main-menu {
                    margin-top: 30px;
                }

                ul.sm-clean {
                    display: none;
                }

                #main-wrapper #header .container {
                    padding: 0;
                }

                #primary-menu ul.sm-clean>li {
                    margin-left: 0;
                    padding: 0;
                    border-top: 0;
                }

                #primary-menu ul.sm-clean>li>a {
                    font-size: 14px;
                    padding: 12px 50px 13px 50px;
                    text-align: center;
                }

                .header-style-1-wrap .main-menu ul.sm-clean>li>a::before,
                .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a::before,
                .header-style-1-wrap .main-menu ul.sm-clean>li>a::after {
                    display: none;
                }
            }
        }

        /*! CSS Used keyframes */

        @-webkit-keyframes fadeInDown {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(0, -100%, 0);
                transform: translate3d(0, -100%, 0);
            }

            100% {
                opacity: 1;
                -webkit-transform: none;
                transform: none;
            }
        }

        @keyframes fadeInDown {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(0, -100%, 0);
                transform: translate3d(0, -100%, 0);
            }

            100% {
                opacity: 1;
                -webkit-transform: none;
                transform: none;
            }

            from {
                opacity: 0;
                transform: translate3d(0, -100%, 0);
            }

            to {
                opacity: 1;
                transform: none;
            }
        }

        /*! CSS Used fontfaces */

        @font-face {
            font-family: lorabold;
            src: url(https: //www.weddingo.in/wp-content/themes/nikah/css/fonts/lora-bold-webfont.eot);
                src: url(https: //www.weddingo.in/wp-content/themes/nikah/css/fonts/lora-bold-webfont.eot#iefix) format('embedded-opentype'), url(https://www.weddingo.in/wp-content/themes/nikah/css/fonts/lora-bold-webfont.woff2) format('woff2'), url(https://www.weddingo.in/wp-content/themes/nikah/css/fonts/lora-bold-webfont.woff) format('woff'), url(https://www.weddingo.in/wp-content/themes/nikah/css/fonts/lora-bold-webfont.ttf) format('truetype'), url(https://www.weddingo.in/wp-content/themes/nikah/css/fonts/lora-bold-webfont.svg#lorabold) format('svg');
                font-weight: 400;
            font-style: normal;
        }

        @font-face {
            font-family: Poppins;
            font-style: normal;
            font-weight: 500;
            src: local('Poppins Medium'), local('Poppins-Medium'), url(https: //fonts.gstatic.com/s/poppins/v5/pxiByp8kv8JHgFVrLGT9Z11lFc-K.woff2) format('woff2');
                unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
        }

        @font-face {
            font-family: Poppins;
            font-style: normal;
            font-weight: 500;
            src: local('Poppins Medium'), local('Poppins-Medium'), url(https: //fonts.gstatic.com/s/poppins/v5/pxiByp8kv8JHgFVrLGT9Z1JlFc-K.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        @font-face {
            font-family: Poppins;
            font-style: normal;
            font-weight: 500;
            src: local('Poppins Medium'), local('Poppins-Medium'), url(https: //fonts.gstatic.com/s/poppins/v5/pxiByp8kv8JHgFVrLGT9Z1xlFQ.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            L
        }

        /*menu styling end*/

        /*footer styling*/

        /*! CSS Used from: https://www.weddingo.in/wp-includes/css/contact-styles.min.css ; media=all */

        @media all {
            .fa {
                display: inline-block;
                font: normal normal normal 14px/1 FontAwesome;
                font-size: inherit;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }

            .fa-twitter:before {
                content: "\f099";
            }

            .fa-facebook:before {
                content: "\f09a";
            }

            .fa-instagram:before {
                content: "\f16d";
            }

            @media all {
                footer {
                    display: block;
                }

                a,
                div,
                footer,
                img,
                li,
                ul {
                    margin: 0;
                    padding: 0;
                    border: 0;
                    font-size: 100%;
                    vertical-align: baseline;
                }

                a {
                    color: #000;
                }

                a {
                    -webkit-transition: all .2s ease-in-out;
                    -o-transition: all .2s ease-in-out;
                }

                a {
                    outline: 0;
                }

                .column {
                    float: left;
                }

                img {
                    display: block;
                    max-width: 100%;
                    height: auto;
                }

                ul {
                    padding-left: 40px;
                    -webkit-padding-start: 40px;
                    -moz-padding-start: 40px;
                    padding-start: 40px;
                }

                a {
                    cursor: pointer;
                    text-decoration: none !important;
                    transition: all .2s ease-in-out;
                }

                a:active,
                a:focus,
                a:hover {
                    color: #666;
                    text-decoration: none;
                    outline: 0;
                }

                ::-moz-selection {
                    background-color: #000;
                    color: #fff;
                }

                ::selection {
                    background-color: #000;
                    color: #fff;
                }

                .column,
                .container {
                    padding-right: 15px;
                    padding-left: 15px;
                    max-width: 100%;
                }

                ::-ms-clear {
                    display: none;
                }

                .clearfix:after,
                .clearfix:before {
                    content: "";
                    display: table;
                }

                .clearfix:after {
                    clear: both;
                }

                .clearfix {
                    zoom: 1;
                }

                .container {
                    width: 1200px;
                    margin: 0 auto;
                }

                .column {
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    display: inline-block;
                }

                .column-3 {
                    width: 33.33333%;
                }

                .row {
                    margin-right: -15px;
                    margin-left: -15px;
                }

                #main-menu-state:checked~.main-menu-btn .main-menu-btn-icon {
                    height: 0;
                    background: 0 0;
                }

                #main-menu-state:checked~.main-menu-btn .main-menu-btn-icon:before {
                    top: 0;
                    -webkit-transform: rotate(-45deg);
                    transform: rotate(-45deg);
                }

                #main-menu-state:checked~.main-menu-btn .main-menu-btn-icon:after {
                    top: 0;
                    -webkit-transform: rotate(45deg);
                    transform: rotate(45deg);
                }

                #footer {
                    margin-left: auto;
                    margin-right: auto;
                    max-width: 100%;
                    position: absolute;
                    right: 0;
                    bottom: auto;
                    left: 0;
                }

                .footer-bottom .social-footer ul {
                    list-style: none;
                }

                .footer-bottom .social-footer ul li {
                    display: inline-block;
                    margin-left: 10px;
                    font-size: 14px;
                }

                .logo-footer img {
                    display: inline-block;
                }

                .social-footer ul {
                    padding: 0;
                }

                .foot-col.text-left {
                    text-align: left;
                }

                .foot-col.text-center {
                    text-align: center;
                }

                .foot-col.text-right {
                    text-align: right;
                }

                .foot-col.vertical .foot-col-item {
                    display: block;
                }
            }

            #footer {
                background-repeat: no-repeat;
                background-attachment: inherit;
                background-position: center center;
                background-size: inherit;
                background-image: none;
            }

            @media all {
                @media only screen and (max-width: 1140px) {
                    .main-wrapper .container {
                        width: auto;
                        padding: 0 30px;
                        box-sizing: border-box;
                    }

                    .row {
                        margin: 0;
                    }
                }

                @media only screen and (max-width:1024px) {
                    .main-wrapper #footer .container {
                        width: auto;
                        padding: 0 30px;
                        box-sizing: border-box;
                    }
                }

                @media only screen and (max-width:768px) {
                    .main-wrapper #footer .container {
                        width: 100%;
                        padding: 0 30px;
                        box-sizing: border-box;
                    }

                    .footer-bottom .column {
                        width: 100%;
                        text-align: center;
                        float: none;
                        margin-bottom: 20px;
                    }

                    .column {
                        float: none;
                        max-width: 100%;
                    }

                    .column-3 {
                        width: 100%;
                    }

                    .row {
                        margin-right: 0;
                        margin-left: 0;
                    }
                }
            }

            #footer .container {
                width: 1170px;
            }

            #footer .footer-wrap {
                padding-top: 0;
                padding-bottom: 0;
            }

            #footer {
                background-color: #fff;
            }

            .foot-col.item-col-1 .foot-col-item {
                margin: 25px 0 0;
            }

            .foot-col.item-col-1 .foot-col-item:first-child {
                margin-left: 0;
            }

            .foot-col.item-col-1 .foot-col-item:last-child {
                margin-right: 0;
            }

            .foot-col.item-col-2 .foot-col-item {
                margin: 0 0 20px;
            }

            .foot-col.item-col-2 .foot-col-item:first-child {
                margin-left: 0;
            }

            .foot-col.item-col-2 .foot-col-item:last-child {
                margin-right: 0;
            }

            .foot-col.item-col-3 .foot-col-item {
                margin: 25px 0 0;
            }

            .foot-col.item-col-3 .foot-col-item:first-child {
                margin-left: 0;
            }

            .foot-col.item-col-3 .foot-col-item:last-child {
                margin-right: 0;
            }

            .copyright-text,
            .copyright-text a {
                color: #000;
            }

            .copyright-text a:hover {
                color: #333;
            }

            @media all {
                .fa {
                    display: inline-block;
                    font: normal normal normal 14px/1 FontAwesome;
                    font-size: inherit;
                    text-rendering: auto;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                }

                .fa-twitter:before {
                    content: "\f099";
                }

                .fa-facebook:before {
                    content: "\f09a";
                }

                .fa-instagram:before {
                    content: "\f16d";
                }
            }

            #footer .footer-bottom {
                padding-top: 40px;
                padding-bottom: 10px;
            }

            .footer-bottom {
                border-top: 0 solid #efefef;
            }

            .fa {
                display: inline-block;
            }

            .fa {
                font: normal normal normal 14px/1 FontAwesome;
                font-size: inherit;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }

            .fa-twitter:before {
                content: "\f099";
            }

            .fa-facebook:before {
                content: "\f09a";
            }

            .fa-instagram:before {
                content: "\f16d";
            }
        }

        /*! CSS Used from: https://www.weddingo.in/wp-content/themes/nikah/css/responsive.css ; media=all */

        @media all {
            @media only screen and (max-width: 1140px) {
                .main-wrapper .container {
                    width: auto;
                    padding: 0 30px 0 30px;
                    box-sizing: border-box;
                }

                .row {
                    margin: 0;
                }
            }

            @media only screen and (max-width: 1024px) {
                .main-wrapper #footer .container {
                    width: auto;
                    padding: 0 30px 0 30px;
                    box-sizing: border-box;
                }
            }

            @media only screen and (max-width: 768px) {
                .main-wrapper #footer .container {
                    width: 100%;
                    padding: 0 30px 0 30px;
                    box-sizing: border-box;
                }

                .footer-bottom .column {
                    width: 100%;
                    text-align: center;
                    float: none;
                    margin-bottom: 20px;
                }

                .column {
                    float: none;
                    max-width: 100%;
                }

                .column-3 {
                    width: 100%;
                }

                .row {
                    margin-right: 0px;
                    margin-left: 0px;
                }
            }
        }

        /*! CSS Used fontfaces */

        @font-face {
            font-family: FontAwesome;
            src: url(https: //cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0);
                src: url(https: //cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.eot#iefix&v=4.7.0) format('embedded-opentype'), url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'), url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'), url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0) format('truetype'), url(https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');
                font-weight: 400;
            font-style: normal;
        }

        @font-face {
            font-family: FontAwesome;
            src: url(https: //www.weddingo.in/wp-includes/fonts/fontawesome-webfont.eot?v=4.7.0);
                src: url(https: //www.weddingo.in/wp-includes/fonts/fontawesome-webfont.eot?#iefix&v=4.7.0) format('embedded-opentype'), url(https://www.weddingo.in/wp-includes/fonts/fontawesome-webfont.woff2?v=4.7.0) format('woff2'), url(https://www.weddingo.in/wp-includes/fonts/fontawesome-webfont.woff?v=4.7.0) format('woff'), url(https://www.weddingo.in/wp-includes/fonts/fontawesome-webfont.ttf) format('truetype'), url(https://www.weddingo.in/wp-includes/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular) format('svg');
                font-weight: 400;
            font-style: normal;
        }




        /*footer styling end*/

        .weddingo-demo {
            background-color: #21b552;
            padding: 10px 15px !important;
            border-radius: 25px !important;
            color: #fff !important;
        }

        .templates-wrapper {
            margin-top: 70px;
            height: 30%;
        }

        .logo-img {
            height: 30px;
        }

        .title-wrapper {
            text-align: center;
        }

        .title-heading {
            font-size: 14px;
            line-height: 24px;
            letter-spacing: 10px;
            margin-top: 0;
            margin-bottom: 0;
            color: #000;
            font-family: Raleway;
            font-weight: 400;
        }

        .the-title {
            font-size: 40px;
            line-height: 40px;
            margin-top: 0;
            margin-bottom: 0;
            font-family: Raleway;
            font-weight: 400;
            color: #000;
        }


        .content-wrapper {
            width: 100%;
            background: #f7f3f3;
            display: flex;
        }

        .items-wrapper {
            align-items: flex-start;
            background: #f7f3f3;
            display: flex;
            width: 70%;
            flex-wrap: wrap;
        }

        .title-of-template {
            line-height: 19px;
            font-size: 1.1em;

            font-weight: 400;
        }

        .item {
            position: relative;
            text-align: center;
            color: white;
            background-position: center;
            background-size: cover;
            background-position: center;

            transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
            margin: 6px;
            flex-wrap: wrap;
            width: 32%;

        }

        @media only screen and (max-width: 800px) {
            .item {
                flex-direction: column;
                flex-basis: 100%;
            }

            .items-wrapper {
                width: 100%;
            }

            .sidebar-wrapped {
                display: none;
            }
        }

        .hover-overlay {
            background-color: rgba(65, 70, 67, 0.75);
            opacity: 0;
            transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
            height: 100%;
            width: 100%;
            top: 0;

            z-index: 0;
            left: 0;
            position: absolute;
        }


        .item:hover .hover-overlay {
            opacity: 1;
        }

        .item-img {
            width: 100%;
            height: 100%;

        }


        .template-name {
            margin: 0;
            position: absolute;
            top: 128%;
            left: 50%;
            transition: 1.2s all;
            transform: translate3d(-50%, -50%, 0);
            font-size: 1.1em;
            font-family: "Kaushan Script", Sans-serif;
            width: 80%;
            text-align: center;
        }

        .item:hover .template-name {
            top: 50%;
        }


        .btn-purchase {
            background: none;
            position: absolute;
            bottom: 30%;
            padding: 0px 20px;
            transition: 1.2s all;
            transform: translateX(-50%);
            border-radius: 5px;
            color: white;
            font-size: 16px;
            text-transform: uppercase;
            border: 1px solid;
            cursor: pointer;
            height: 40px;
        }

        .item:hover .btn-purchase {
            bottom: 20px;
        }

        .click-overlay {
            background-color: rgba(25, 136, 69, 0.75);
            opacity: 0;
            transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
            height: 100%;
            width: 100%;
            top: 0;
            z-index: 0;
            left: 0;
            position: absolute;
        }

        /*.go-img {
                                    background: none;
                                    position: absolute;
                                    bottom: 30%;
                                    padding: 0px 20px;
                                    transition: 1.2s all;
                                    transform: translateX(-50%);
                                    border-radius: 5px;

                                    cursor: pointer;
                                    height: 70px;
                                }
        */

        .go-img {
            text-align: center;
            position: absolute;
            top: 24%;
            left: 31%;
        }

        .click-overlay {
            opacity: 1;
        }

        .item:acive .go-img {}


        .template-img {
            width: 100%;
            position: relative;
        }


        .sidebar-wrapped {
            width: 22%;
            background: #f7f3f3;
            padding-top: 10px;

        }

        .sidebar-wrapper {
            background: white;
        }

        .p-10 {
            padding: 10px;
        }

        .m-0 {
            margin: 0;
            line-height: 20px;
        }

        .btn-purchase-fill {
            border: none;
            background: #21b552;
            padding: 0px 20px;
            width: 100%;
            border-radius: 10px;
            color: white;
            font-size: 16px;
            text-transform: uppercase;
            /*box-shadow: 0 0 10px rgba(0, 0, 0, 0.34);*/
            cursor: pointer;
            height: 40px;
        }

        .features-heading {
            background: #f7f3f3;
            padding: 5px;
        }

        .section-text {
            background: white;
            padding: 1px 0px 0px 10px;
        }

        .color-icons {
            background: white;
            padding: 5px;
        }

        .heart-svg {
            width: 25px;

        }

        .item-img-gif {
            margin-bottom: 10px;
            width: 100%;
            margin-top: 5px;
            border-radius: 10px;
        }

    </style>



    <!--<style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important; 
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }

    </style>
    <link rel='stylesheet' id='contact-form-7-css' href='https://www.weddingo.in/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.9' type='text/css' media='all' /> 
    <link rel='stylesheet' id='rs-plugin-settings-css' href='https://www.weddingo.in/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.3.1.5' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}

    </style>
    <link rel='stylesheet' id='nikah-plugin-css-css' href='https://www.weddingo.in/wp-content/themes/nikah/css/plugin.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nikah-style-css' href='https://www.weddingo.in/wp-content/themes/nikah/style.css?ver=4.9.8' type='text/css' media='all' /> 
    <link rel='stylesheet' id='nikah-font-css' href='https://www.weddingo.in/wp-content/themes/nikah/css/font.css' type='text/css' media='all' />-->
    <link rel='stylesheet' id='nikah-responsive-css-css' href='../wp-includes/css/demo-styles.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nikah-responsive-css-css' href='../wp-includes/css/contact-styles.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nikah-responsive-css-css' href='../wp-content/themes/nikah/css/responsive.css' type='text/css' media='all' />
    <!--<link rel='stylesheet' id='nikah-custom-style-css' href='https://www.weddingo.in/wp-content/themes/nikah/css/custom-style.css?ver=4.9.8' type='text/css' media='all' />
    <style id='nikah-custom-style-inline-css' type='text/css'> 
        /* theme options */

        .bordered {
            background-color: #111111; 
        }

        /* container */

        #header .container {
            width: 1170px;
        }

        #content .container {
            width: 1170px;
        }

        #footer .container {
            width: 1170px;
        }

        #status {
            background-size: 40px 40px;
        }

        .bordered-main-wrap {
            padding-left: 0px;
            padding-right: 0px;
        }

        @media only screen and (max-width: 768px) {
            header#header.inner-head-wrap.header-expanded {
                /*background-color: transparent;*/ 
                background-color: #000000;
            }

            header#header.inner-head-wrap.header-expanded.alt-head {
                background-color: #000000;
            }
        }

        #status {
            background-image: url(http: //www.weddingo.in/wp-content/uploads/sites/96/2018/03/loading.gif);
        }

        /* header area contents */

        #header .logo-image,
        #header .logo-title {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        header#header .container {
            width: 1170px;
            ;
        }

        header#header.inner-head-wrap {
            padding-top: 50px;
            padding-bottom: 50px;
            padding-right: 0px;
            padding-left: 0px;
            -webkit-transition: all 0.5s ease 0s;
            -moz-transition: all 0.5s ease 0s;
            transition: all 0.5s ease 0s;
            background-color: transparent;
            background-repeat: no-repeat;
            background-attachment: inherit;
            background-position: center center; 
            background-size: inherit;
            background-image: none;
        }

        .sticky-header-wrap.scrolled header#header.inner-head-wrap {
            padding-top: 30px;
            padding-bottom: 30px;
            padding-right: 0px;
            padding-left: 0px;
            -moz-transition: all 0.6s ease 0s;
            -ms-transition: all 0.6s ease 0s;
            -o-transition: all 0.6s ease 0s;
            transition: all 0.6s ease 0s;
        }

        .sticky-header-wrap.scrolled {
            background-color: #ffffff;
            -moz-transition: all 0.6s ease 0s;
            -ms-transition: all 0.6s ease 0s;
            -o-transition: all 0.6s ease 0s;
            transition: all 0.6s ease 0s;
        }

        .sticky-header-wrap.scrolled a,
        .sticky-header-wrap.scrolled i {
            color: #000000 !important;
            -moz-transition: all 0.6s ease 0s;
            -ms-transition: all 0.6s ease 0s;
            -o-transition: all 0.6s ease 0s;
            transition: all 0.6s ease 0s;
        }

        #header.alt-head #showMenu span {
            background-color: #ffffff;
        }

        .fl.vertical.header_left_nofloat {
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .fr.vertical.header_right_nofloat {
            padding-top: 0px;
            padding-bottom: 0px;
            border-top: 1px solid #efefef;
            border-bottom: 1px solid #efefef;
        }

        .fl.horizontal .head-item {
            margin-left: 0px; 
            margin-right: 0px;
        }

        .fr .head-item {
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }

        .fr.horizontal .head-item {
            margin-left: 0px !important;
            margin-right: 20px !important;
        }

        @media only screen and (max-width: 768px) {
            header#header.inner-head-wrap {
                padding-left: 30px;
                padding-right: 30px;
            }

            header#header .logo.head-item {
                margin-top: 0px;
                margin-bottom: 0px;
            }

            .header-style-1 .fr .head-item.search-wrap,
            header#header .main-menu-btn {
                margin-top: 15px !important;
                margin-bottom: 0px !important;
            }
        }

        /* footer area content */

        #footer .container {
            width: 1170px;
        }

        #footer .footer-wrap {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        #footer {
            background-color: #ffffff;
            background-repeat: no-repeat; 
            background-attachment: inherit;
            background-position: center center;
            background-size: inherit;
            background-image: none;
        }

        .foot-col.item-col-1 .foot-col-item {
            margin-top: 25px;
            margin-left: 0px;
            margin-bottom: 0px;
            margin-right: 0px;
        }

        .foot-col.item-col-1 .foot-col-item:first-child {
            margin-left: 0;
        }

        .foot-col.item-col-1 .foot-col-item:last-child {
            margin-right: 0;
        }

        .foot-col.item-col-2 .foot-col-item {
            margin-top: 0px;
            margin-left: 0px;
            margin-bottom: 20px;
            margin-right: 0px;
        }

        .foot-col.item-col-2 .foot-col-item:first-child {
            margin-left: 0;
        }

        .foot-col.item-col-2 .foot-col-item:last-child {
            margin-right: 0;
        }

        .foot-col.item-col-3 .foot-col-item {
            margin-top: 25px; 
            margin-left: 0px;
            margin-bottom: 0px;
            margin-right: 0px;
        }

        .foot-col.item-col-3 .foot-col-item:first-child {
            margin-left: 0;
        }

        .foot-col.item-col-3 .foot-col-item:last-child {
            margin-right: 0;
        }

        .foot-col.item-col-4 .foot-col-item {
            margin-top: 0px;
            margin-left: 0px;
            margin-bottom: 0px;
            margin-right: 0px;
        }

        .foot-col.item-col-4 .foot-col-item:first-child {
            margin-left: 0;
        }

        .foot-col.item-col-4 .foot-col-item:last-child {
            margin-right: 0;
        }

        /*fonts*/

        body,
        body p,
        .comment-respond form p.form-submit input,
        .blog-item .meta-wrapper .author a,
        .blog-item .meta-wrapper span.standard-post-categories {
            font-family: Raleway;
            font-weight: 400;
            text-align: inherit;
            font-size: 16px;
            line-height: 25.6px; 
            word-spacing: 0px;
            letter-spacing: 0px;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: Lora;
            font-weight: 400;
            text-align: inherit;
        }

        .meta.meta-comments .comments {
            font-family: Lora;
        }

        h1 {
            text-align: inherit;
            font-size: 42px;
            line-height: 47.25px;
            word-spacing: 0px;
            letter-spacing: 0px;
        }

        h2 {
            text-align: inherit;
            font-size: 36px;
            line-height: 45px;
            word-spacing: 0;
            letter-spacing: 0;
        }

        h3 {
            text-align: inherit;
            font-size: 24px;
            line-height: 30px;
            word-spacing: 0;
            letter-spacing: 0;
        }

        h4 {
            text-align: inherit;
            font-size: 18px; 
            line-height: 23.4px;
            word-spacing: 0;
            letter-spacing: 0;
        }

        h5 {
            text-align: inherit;
            font-size: 16px;
            line-height: 22px;
            word-spacing: 0;
            letter-spacing: 0;
        }

        h6 {
            text-align: inherit;
            font-size: 14px;
            line-height: 19.6px;
            word-spacing: 0;
            letter-spacing: 0;
        }

        @media only screen and (max-width: 768px) {
            #main-wrapper #header #primary-menu li a {
                color: white;
                font-weight: 500;
                text-align: center;
                font-size: 16px;
                line-height: 25.6px;
                word-spacing: 0;
                letter-spacing: 0;
            }
        }


        .header-style-1-wrap {
            background: #f7f3f3;
        }

        .header-style-1-wrap .main-menu ul.sm-clean>li>a,
        .header-style-1-wrap .main-menu ul.sm-clean>li>a:active,
        .header-style-1-wrap .search-wrap #btn-search i,
        .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a, 
        .header-style-1-wrap .main-menu ul.sm-clean>li>a:hover {
            color: #000000;
        }

        .sm-clean a span.sub-arrow {
            border-top-color: #000000;
        }

        .header-style-1-wrap .main-menu ul.sm-clean>li>a:hover,
        .header-style-1-wrap .menu__item:hover .menu__link {
            color: #111111;
        }

        .header-style-1-wrap .main-menu ul.sm-clean>li>a::before,
        .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a::before,
        .header-style-1-wrap .main-menu ul.sm-clean>li>a::after,
        .header-style-1-wrap .main-menu ul.sm-clean>li.current-menu-item>a::after,
        .header-style-1-wrap .main-menu ul.sm-clean>li::before,
        .header-style-1-wrap .main-menu ul.sm-clean>li::after {
            background-color: #000000;
            border-color: #000000;
        }

        .header-style-1-wrap ul.sm-clean ul { 
            background-color: #000000;
        }

        .header-style-1-wrap ul.sm-clean ul li a {
            color: #ffffff;
        }

        .header-style-1 .btn--search-close {
            color: #ffffff;
        }

        .header-style-1 .search__info {
            color: #ffffff;
        }

        .header-style-1 .search__suggestion h4 {
            color: #ffffff;
        }

        .header-style-1 .search__suggestion h4::before {
            background-color: #ffffff;
        }

        .header-style-1 .search__suggestion p {
            color: #ffffff;
        }

        .header-style-1 .search__input {
            color: #ffffff;
        }

        body .alt-head .main-menu ul.sm-clean>li>a:hover,
        .header-style-1-wrap.alt-head .main-menu ul.sm-clean>li>a:hover,
        .header-style-1-wrap.alt-head .menu__item:hover .menu__link { 
            color: #dddddd;
        }

        body .alt-head .main-menu ul.sm-clean>li>a::before,
        body .alt-head .main-menu ul.sm-clean>li.current-menu-item>a::before {
            background-color: #ffffff;
        }

        /* Header Style 2 --- */

        .header-style-5 #showMenu span,
        .header-style-4 #showMenu span,
        .header-style-6 #showMenu span {
            background-color: #000000;
        }

        .effect-moveleft .outer-nav a,
        .effect-airbnb .outer-nav a,
        .effect-movedown.animate .outer-nav a {
            color: #000000;
        }

        .effect-moveleft .outer-nav a:hover,
        .effect-airbnb .outer-nav a:hover,
        .effect-movedown.animate .outer-nav a:hover {
            color: #999999;
        }

        .post-masonry-style .loop-content .category a {
            color: #8db392;
        }

        .post-masonry-style .loop-content .category a:hover {
            color: #666666;
        }

        .post-masonry-style .loop-content .category:after {
            background-color: #8db392;
        }

        .post-masonry-style .loop-content h4.title a { 
            color: #555555;
        }

        .post-masonry-style .loop-content h4.title a:hover {
            color: #999999;
        }

        .post-masonry-style .loop-content .date {
            color: #888888;
        }

        .post-masonry-style .loop-content p {
            color: #666666;
        }

        .post-masonry-style a.more {
            color: #ffffff;
        }

        .post-masonry-style a.more:hover {
            color: #000000;
        }

        .post-masonry-style a.more {
            background-color: #8db392;
        }

        .post-masonry-style a.more:hover {
            background-color: #eaeaea;
        }

        .post-masonry-style .loop-content {
            background-color: #ffffff;
        } 

        .post-masonry-style .loop-content {
            border-bottom-color: #8db392;
        }

        /* Single Blog --- */

        .blog-item .meta-wrapper a,
        .single-post-style-3-inner-content h1.post-title a {
            color: #000000;
        }

        .blog-item .meta-wrapper a:hover,
        .single-post-style-3-inner-content h1.post-title a:hover {
            color: #666666;
        }

        .blog-style-2 .post-content-style-2,
        .blog-item .meta-wrapper .author a,
        .author-separator,
        .blog-item .meta-wrapper .date a,
        .date span,
        .blog-item .meta-wrapper .standard-post-categories a,
        .social-share-wrapper span,
        .blog-single .meta-info,
        .single-post-style-3-inner-content .post-meta span.vcard {
            color: #000000;
        }

        .blog-item .meta-wrapper .author a:hover,
        .blog-item .meta-wrapper .date a:hover,
        .blog-item .meta-wrapper .standard-post-categories a:hover,
        .single-post-style-3-inner-content .post-meta span.vcard:hover {
            color: #999999;
        }

        .magazine-post-style .standard-post-categories .post-categories a {
            background-color: #8db392;
        }

        .magazine-post-style .standard-post-categories .post-categories a:hover {
            background-color: #9cc3a1;
        }

        .magazine-post-style .standard-post-categories .post-categories a {
            color: #ffffff; 
        }

        .magazine-post-style .standard-post-categories .post-categories a:hover {
            color: #f5f5f5;
        }

        .meta.meta-comments a.comments span,
        .icon-simple-line-icons-124:before {
            color: #000000;
        }

        .meta.meta-comments a.comments:hover span {
            color: #666666;
        }

        .separator-line {
            background-color: #dedede;
        }

        .separator-line>span {
            background-color: #8db392;
        }

        .post-content .post-text p,
        .comment-content p {
            color: #777777;
        }

        .post-content blockquote p {
            color: #000000;
        }

        .post-text blockquote p:before {
            border-color: #eaeaea;
        }
 
        .post-text blockquote p:after {
            color: #111111;
        }

        .tag-wrapper a,
        .tag-wrapper span {
            color: #000000;
        }

        .tag-wrapper a:hover {
            color: #666666;
        }

        .single-post-style .tag-wrapper a {
            background-color: #f3f3f3;
        }

        .single-post-style .tag-wrapper a:hover {
            background-color: #e6e6e6;
        }

        .meta-content-bottom {
            background-color: #f2f2f2;
        }

        .blog-single .post-author {
            border-top-color: #8db392;
        }

        .magazine-post-style .author-desc span {
            color: #000000;
        }

        .magazine-post-style .author-desc a {
            color: #8db392;
        }

        .magazine-post-style .author-desc a:hover {
            color: #9cc3a1;
        }

        .blog-single .next-prev-post,
        .comment-list {
            border-top-color: #f2f2f2;
            border-bottom-color: #f2f2f2;
        }

        .magazine-post-style .next-prev-post .column p {
            color: #000000;
        }

        .magazine-post-style .next-prev-post .column p i { 
        }

        .blog-single .next-prev-post a {
            color: #000000;
        }

        .blog-single .next-prev-post a:hover {
            color: #999999;
        }

        .comment-respond h3.comment-reply-title,
        .comments-title h3 {
            color: #000000;
        }

        .comment-respond form p.logged-in-as a,
        .comment-respond form p.logged-in-as,
        .comment-action a {
            color: #999999;
        }

        .comment-respond form p.logged-in-as a:hover,
        .comment-action a:hover {
            color: #000000;
        }

        .comment-respond form p.form-submit input,
        .contact-form-style-1 .wpcf7-submit,
        .contact-form-style-2 .wpcf7-submit {
            background-color: #000000;
        }

        .comment-respond form p.form-submit input,
        .contact-form-style-1 .wpcf7-submit,
        .contact-form-style-2 .wpcf7-submit {
            color: #ffffff;
        }

        .archive .post-navigation .btn,
        .search-page .post-navigation .btn {
            color: #ffffff;
        }

        /* Sidebar & Widget --- */

        .sidebar .widget.widget_search input {
            background-color: #ffffff;
        }

        .sidebar .widget.widget_search button {
            background-color: #8db392;
        }

        .sidebar .widget.widget_search button i { 
            color: #ffffff;
        }

        .sidebar .widget.widget_search input {
            color: #aaaaaa;
        }

        .sidebar .widget {
            background-color: #ffffff;
        }

        .sidebar .widget h4.widget-title {
            color: #555555;
        }

        .sidebar .widget h4.widget-title:after {
            background-color: #8db392;
        }

        .sidebar #recent-posts-2 ul li a,
        .sidebar .widget .recent-news .post-content h5 a,
        .sidebar #recent-comments-2 ul li a,
        li.recentcomments,
        .sidebar #archives-2 ul li a,
        .sidebar #categories-2 ul li a,
        .sidebar #meta-2 ul li a,
        .latest-post-wrap h5 a,
        .latest-post-wrap .post-content h5 {
            color: #555555;
        }

        .sidebar #recent-posts-2 ul li a:hover,
        .sidebar .widget .recent-news .post-content h5 a:hover,
        .sidebar #recent-comments-2 ul li a:hover,
        li.recentcomments:hover,
        .sidebar #archives-2 ul li a:hover,
        .sidebar #categories-2 ul li a:hover,
        .sidebar #meta-2 ul li a:hover,
        .latest-post-wrap h5 a:hover,
        .latest-post-wrap .post-content h5:hover {
            color: #999999;
        }

        .widget.widget_nikah_news .nav-tabs li.active,
        .widget.widget_nikah_news .post-item:before { 
            background-color: #8db392;
        }

        .widget.widget_nikah_news .nav-tabs li.active a,
        .widget.widget_nikah_news .post-item:before {
            color: #ffffff;
        }

        .widget.widget_nikah_news .nav-tabs li {
            background-color: #ffffff;
        }

        .widget.widget_nikah_news .nav-tabs li a {
            color: #555555;
        }

        .widget.widget_nikah_news .nav-tabs li a:hover {
            color: #999999;
        }

        .widget.widget_nikah_news .nav-tabs {
            border-bottom-color: #8db392;
        }

        /* Contact --- */

        .contact-form-style-2 .contact-item2:before,
        .contact-ef .border-form-top,
        .contact-ef {
            background-color: #cdcdcc
        }

        .contact-form-style-2 .contact-item2:after {
            background-color: #000000
        }

        .contact-form-style-1 .contact-bordered input,
        .contact-form-style-2 .contact-item2 input,
        .contact-bordered.text-area textarea,
        .contact-form-style-2 .contact-item2 textarea {
            color: #000000
        }

        .contact-form-style-1 input.wpcf7-submit,
        .contact-form-style-2 input.wpcf7-submit {
            background-color: #8db392
        }

        .contact-form-style-1 input.wpcf7-submit,
        .contact-form-style-2 input.wpcf7-submit {
            color: #ffffff
        }

        .contact-form-style-1 input.wpcf7-submit:hover,
        .contact-form-style-2 input.wpcf7-submit:hover {
            background-color: #69b273
        }

        .contact-form-style-1 input.wpcf7-submit:hover,
        .contact-form-style-2 input.wpcf7-submit:hover {
            color: #ffffff
        }

        /*========== PORTFOLIO SINGLE ==========*/

        .header-style-1 .page-title h2,
        .post-title-porto-2 h1 {
            color: #000000;
        }

        .portfolio-style1 span.category,
        .post-title-porto-2 span.category {
            color: #8db392;
        }

        .portfolio-style1 .project-details ul li span,
        .portfolio-details .detail-title { 
            color: #000000;
        }

        .portfolio-style1 .project-details ul li p,
        .portfolio-details .detail-info {
            color: #888888;
        }

        .portfolio-style1 .project-details ul li,
        .portfolio-details .detail-item {
            border-bottom-color: #e5e5e5;
        }

        .portfolio-content p {
            color: #555555;
        }

        .portfolio-style1 .portfolio-pagination .prev-portfolio a,
        .portfolio-style1 .portfolio-pagination .all-portfolio a,
        .portfolio-style1 .portfolio-pagination .next-portfolio a {
            color: #000000;
        }

        .portfolio-style1 .portfolio-pagination a:hover {
            color: #666666;
        }



        .copyright-text,
        .footer-text-area {
            color: #000000; 
        }

        .copyright-text a,
        .footer-menu li a {
            color: #000000;
        }

        .copyright-text a:hover,
        .footer-menu li a:hover {
            color: #333333;
        }

        .footer-bottom .social-footer ul li a {
            color: #333333;
        }

        .footer-bottom .social-footer ul li a:hover {
            color: #8db392;
        }

        .footer-widget-wrapper {
            background-color: #ffffff;
        }

        .footer-widget .widget-footer h4.widget-title {
            color: #000000;
        } 

        .footer-widget .widget-footer .latest-post-widget a,
        .footer-widget .widget-footer .latest-post-wrap h5,
        .footer-widget .widget_nav_menu ul li a,
        .footer-widget .widget-footer a {
            color: #555555;
        }

        .footer-widget .widget-footer .latest-post-widget a:hover,
        .footer-widget .widget_nav_menu ul li a:hover,
        .footer-widget .widget-footer a:hover {
            color: #999999;
        }

        .footer-widget .textwidget {
            color: #000000;
        }

        .latest-post-wrap h5,
        .footer-widget .widget_nav_menu ul li a:before {
            border-bottom-color: #dddddd;
        }

        div.mail-response {

            padding: 1.2em 1em;
            border: 3px solid #8db392;
            border-radius: 8px;
            width: auto;
            box-shadow: 2px 2px 7px 0px #888888;
            position: fixed; 
            top: 50%;
            left: 50%;
            background-color: white;
            transform: translate3D(-50%, -50%, 0);
            width: 300px;
            margin: 0 !important;
        }

        div.mail-response-failure {
            border-color: red !important;
        }

        .response-text {
            padding-left: 15px;
            font-family: Raleway;
        }

        .message-tick {}

        .message-tick>path {
            fill: #8db392;

        }

    </style>-->
    <!--<link rel='stylesheet' id='elementor-icons-css' href='https://localhost/weddingo_landingpage/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=3.6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css' href='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.1' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-animations-css' href='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=2.1.2' type='text/css' media='all' /> 
    <link rel='stylesheet' id='elementor-frontend-css' href='https://www.weddingo.in/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.1.2' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-global-css' href='https://www.weddingo.in/wp-content/uploads/sites/96/elementor/css/global.css?ver=1531197692' type='text/css' media='all' /> 
    <link rel='stylesheet' id='elementor-post-29-css' href='https://www.weddingo.in/wp-content/uploads/sites/96/elementor/css/post-29.css?ver=1531197718' type='text/css' media='all' />
    <link rel='stylesheet' id='site-categories-styles-css' href='https://www.weddingo.in/wp-content/plugins/site-categories/css/site-categories-styles.css?ver=4.9.8' type='text/css' media='all' />
    <link rel='stylesheet' id='redux-google-fonts-nikah_framework-css' href='https://fonts.googleapis.com/css?family=Poppins%3A500%7CRaleway%3A400%7CLora%3A400&#038;ver=1520929810' type='text/css' media='all' />-->
    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
    <!--<script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/masonry.min.js?ver=3.3.2'></script> 
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/nikah-extra/inc/js/nikah-instafeed.min.js?ver=1'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.3.1.5'></script> 
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.3.1.5'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/modernizr.js?ver=4.9.8'></script> 
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/respond.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/classie.js?ver=4.9.8'></script>-->










    <!--<link rel='https://api.w.org/' href='https://www.weddingo.in/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.weddingo.in/xmlrpc.php?rsd" /> 
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.weddingo.in/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.8" /> 
    <link rel="canonical" href="https://www.weddingo.in/" />
        <link rel='shortlink' href='https://www.weddingo.in/?p=29' />
    <link rel="alternate" type="application/json+oembed" href="https://www.weddingo.in/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.weddingo.in%2Fcontact%2F" /> 
    <link rel="alternate" type="text/xml+oembed" href="https://www.weddingo.in/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwww.weddingo.in%2Fcontact%2F&#038;format=xml" />
    <style type="text/css">
        .recentcomments a {
            display: inline !important; 
            padding: 0 !important;
            margin: 0 !important;
        }

    </style> 
    <style type="text/css" id="wp-custom-css">
        .meta.meta-comments a.comments span,
        .icon-simple-line-icons-124:before {
            color: #ffffff;
        }

        .contact-map iframe {
            height: 520px;
        }

        .blog-item .read-more {
            border-radius: 50px;
            border: 2px solid #e4e4e4;
        }

    </style>
    <style type="text/css" title="dynamic-css" class="options-output">
        body .alt-head .main-menu ul.sm-clean>li>a,
        body .alt-head .search-wrap #btn-search i,
        body .alt-head .main-menu ul.sm-clean>li.current-menu-item>a,
        .alt-head .site-title a {
            color: #ffffff;
        }

        #header #primary-menu li a,
        #header #secondary-menu li a {
            font-family: Poppins, Georgia, serif;
            text-align: inherit;
            line-height: 25.6px;
            font-weight: 500; 
            font-style: normal;
            font-size: 16px;
        }

        .blog-content-wrap .blog {
            padding-top: 60px;
            padding-bottom: 60px;
        }

        .archive #content {
            padding-top: 60px;
            padding-bottom: 60px; 
        }

        .single-post-wrap .blog {
            padding-top: 0;
            padding-bottom: 150px;
        }

        .single-portfolio-wrap {
            padding-top: 20px;
            padding-bottom: 60px;
        }

        #footer .footer-widget-wrapper {
            padding-top: 0;
            padding-bottom: 0;
        }

        #footer .footer-bottom {
            padding-top: 40px;
            padding-bottom: 10px;
        }

        .footer-widget-wrapper {
            border-top: 0px solid #efefef;
        }

        .footer-bottom {
            border-top: 0px solid #efefef;
        }

        .logo-img {
            height: 30px;
        }

        .weddingo-demo {
            background-color: #21b552;
            padding: 10px 15px !important;
            border-radius: 25px !important;
            color: #fff !important;
        }

        .elementor-icon-box-wrapper {
            align-items: unset !important;

        }

        .contact-icon {
            top: 10px !important;
        }

    </style>-->



</head>

<body id="body" class="page-template page-template-template page-template-page-builder-template page-template-templatepage-builder-template-php page page-id-29 header-style-1 elementor-default elementor-page elementor-page-29">

    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <div id="main-wrapper" class="main-wrapper clearfix">

        <div id="sticky-wrap-head" class="sticky-header-wrap header_fixed_noscroll clearfix">
            <!-- Header
		============================================= -->
            <header id="header" class="header-style-1-wrap inner-head-wrap  animated  clearfix">

                <div class="container clearfix">

                    <div class="header-clear  clearfix">
                        <div class="fl header1-2 horizontal header_left_float clearfix">
                            <!-- Logo
============================================= -->
                            <div class="logo head-item">

                                <div class="logo-image">
                                    <a href="https://www.weddingo.in">
                                        <img class="logo-img" src="../images/logo2.png" alt="weddingo-logo" />
                                    </a>
                                </div>
                            </div>
                            <!-- end logo -->
                        </div>

                        <div class="fr header1-2 vertical header_right_float clearfix">
                            <!-- Mobile menu toggle button (hamburger/x icon) -->
                            <input id="main-menu-state" type="checkbox" />
                            <label class="main-menu-btn sub-menu-triger" for="main-menu-state">
                                <span class="main-menu-btn-icon"></span>
                            </label>

                            <!-- Primary Navigation
============================================= -->
                            <nav id="primary-menu" class="menu main-menu head-item">
                                <ul id="menu-menu" class="sm sm-clean menu--ferdinand">
                                    <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-8 current_page_item menu-item-41"><a class="menu__link" href="../">Home</a>
                                    </li>
                                    <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a class="menu__link" href="../about/">About Us</a>
                                    </li>
                                    <li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a class="menu__link" href="../testimonials/">Testimonial</a>
                                    </li>
                                    <li id="menu-item-40" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a class="menu__link" href="../contact/">Contact</a>
                                    </li>
                                    <li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87"><a class="weddingo-demo" href="https://weddingo.in/demo/" target="_blank">Weddingo Demo</a>
                                    </li>
                                </ul>
                            </nav>
                            <!-- end primary menu -->

                        </div>
                    </div>

                </div>

            </header>

        </div>



        <?php
$templates = json_decode('[{
"id": 1,
"title": "Template 1",
"gif": "../images/templates/template1.gif",
"image": "../images/couples/jonica.jpeg",
"sales": "30",
"colors": ["red", "blue", "#cfc"],
"sections": ["Slider", "About Groom and Bride", "Gallery"],
"extra": ["Blue Butterflies"]
},
{
"id": 2,
"title": "Template 2",
"gif": "../images/templates/ulemulem.gif",
"image": "../images/couples/jonica.jpeg",
"sales": "30",
"colors": ["red", "blue", "#cfc"],
"sections": ["Slider", "About Groom and Bride", "Gallery"],
"extra": ["Red Geart Animation"]
},
{
"id": 3,
"title": "Template 3",
"gif": "../images/templates/httpbestday-maskandesign-com.gif",
"image": "../images/couples/jonica.jpeg",
"sales": "30",
"colors": ["red", "blue", "#cfc"],
"sections": ["Slider", "About Groom and Bride", "Gallery"],
"extra": ["Blue Butterflies"]
},
{
"id": 4,
"title": "Template 4",
"gif": "../images/templates/redhearts-verothemes.gif",
"image": "../images/couples/jonica.jpeg",
"sales": "30",
"colors": ["red", "blue", "#cfc"],
"sections": ["Slider", "About Groom and Bride", "Gallery"],
"extra": ["Blue Butterflies"]
},
{
"id": 5,
"title": "Template 5",
"gif": "../images/templates/starkethemes-comhome-2-pink-animated-flowers.gif",
"image": "../images/couples/jonica.jpeg",
"sales": "30",
"colors": ["red", "blue", "#cfc"],
"sections": ["Slider", "About Groom and Bride", "Gallery"],
"extra": ["Blue Butterflies"]
}
]');
        
        

        
        ?>

        <script>
            var templateclicked = function(ind) {
                var templateid = 3;

                //remove other clicks
                document.getElementsByClassName('click-overlay')[0] ? document.getElementsByClassName('click-overlay')[0].outerHTML = "" : false;

                //change sidebar
                document.getElementById('template-image').src = "../images/templates/template1.gif";
                document.getElementById('template-title').innerHTML = "Title of the Template goes here";
                document.getElementById('template-sales').innerHTML = "40";
                document.getElementById('template-sections').innerHTML = `<p class="m-0">Top Couple Image Slider </p><p class="m-0">About The Couple</p><p class="m-0">The Story</p><p class="m-0">Gallery Of The Images</p><p class="m-0">Family Members</p><p class="m-0">Pre Wedding Video</p><p class="m-0">Ceremony Details</p><p class="m-0">RSVP</p>`;

                document.getElementsByClassName('item')[ind].innerHTML += `<div class="click-overlay">
                                    <img src="../images/Favicon/android-icon-144x144.png" class="go-img" alt="">
                                </div>`;
            };

        </script>

        <div class="sticky-header-gap header_fixed_noscroll clearfix"></div>

        <!-- HEADER END -->
        <!-- CONTENT WRAPPER
============================================= -->
        <div id="content" class="content-wrapper clearfix" style="margin-top: 130px;">

            <div class="page-content clearfix">

                <div class="templates-wrapper">
                    <div class="title-wrapper">
                        <p class="title-heading">Templates</p>
                        <p class="the-title">Online Wedding Invitation Designs</p>
                        <p>Choose one of the best designs that suits to your personality Choose one of the best designs that suits to your personality Choose one of the best designs that suits to your personality </p>
                    </div>
                    <div class="content-wrapper">
                        <div class="items-wrapper p-10">


                            <?php
                                foreach($templates as $temp){
                                    ?>
                            <div class="item" onclick="templateclicked(<?php echo (($temp->id)-1); ?>)">
                                <img src="<?php echo $temp->image; ?>" class="item-img" alt="">
                                <div class="hover-overlay">
                                    <p class="template-name">
                                        <?php echo $temp->title; ?>
                                    </p>
                                    <button class="btn-purchase">PURCHASE</button>

                                </div>



                            </div>
                            <?php
                                };
                            ?>






                        </div>
                        <div class="sidebar-wrapped">
                            <div class="sidebar-wrapper p-10">
                                <div class="sidebar-content-wrap">
                                    <button class="btn-purchase-fill">PURCHASE</button>
                                    <div class="" style="width: 100%;">
                                        <img src="../images/couples/jonica.jpeg" id="template-image" class="item-img-gif" alt="">
                                    </div>
                                    <p class="title-of-template" id="template-title">Title of the template goes here Title of the template goes here</p>

                                    <p class="features-heading"> Colors:</p>
                                    <div class="color-icons" id="template-colors">
                                        <svg enable-background="new 0 0 512 512" version="1.1" viewBox="0 0 512 512" class="heart-svg" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m376 30c-27.783 0-53.255 8.804-75.707 26.168-21.525 16.647-35.856 37.85-44.293 53.268-8.437-15.419-22.768-36.621-44.293-53.268-22.452-17.364-47.924-26.168-75.707-26.168-77.532 0-136 63.417-136 147.51 0 90.854 72.943 153.02 183.37 247.12 18.752 15.981 40.007 34.095 62.099 53.414 2.912 2.55 6.652 3.954 10.532 3.954s7.62-1.404 10.532-3.953c22.094-19.322 43.348-37.435 62.111-53.425 110.41-94.093 183.36-156.25 183.36-247.11 0-84.097-58.468-147.51-136-147.51z fill=red" /></svg>
                                        <svg enable-background="new 0 0 512 512" version="1.1" viewBox="0 0 512 512" class="heart-svg" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m376 30c-27.783 0-53.255 8.804-75.707 26.168-21.525 16.647-35.856 37.85-44.293 53.268-8.437-15.419-22.768-36.621-44.293-53.268-22.452-17.364-47.924-26.168-75.707-26.168-77.532 0-136 63.417-136 147.51 0 90.854 72.943 153.02 183.37 247.12 18.752 15.981 40.007 34.095 62.099 53.414 2.912 2.55 6.652 3.954 10.532 3.954s7.62-1.404 10.532-3.953c22.094-19.322 43.348-37.435 62.111-53.425 110.41-94.093 183.36-156.25 183.36-247.11 0-84.097-58.468-147.51-136-147.51z fill=red" /></svg>
                                        <svg enable-background="new 0 0 512 512" version="1.1" viewBox="0 0 512 512" class="heart-svg" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m376 30c-27.783 0-53.255 8.804-75.707 26.168-21.525 16.647-35.856 37.85-44.293 53.268-8.437-15.419-22.768-36.621-44.293-53.268-22.452-17.364-47.924-26.168-75.707-26.168-77.532 0-136 63.417-136 147.51 0 90.854 72.943 153.02 183.37 247.12 18.752 15.981 40.007 34.095 62.099 53.414 2.912 2.55 6.652 3.954 10.532 3.954s7.62-1.404 10.532-3.953c22.094-19.322 43.348-37.435 62.111-53.425 110.41-94.093 183.36-156.25 183.36-247.11 0-84.097-58.468-147.51-136-147.51z fill=red" /></svg>
                                        <svg enable-background="new 0 0 512 512" version="1.1" viewBox="0 0 512 512" class="heart-svg" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                            <path d="m376 30c-27.783 0-53.255 8.804-75.707 26.168-21.525 16.647-35.856 37.85-44.293 53.268-8.437-15.419-22.768-36.621-44.293-53.268-22.452-17.364-47.924-26.168-75.707-26.168-77.532 0-136 63.417-136 147.51 0 90.854 72.943 153.02 183.37 247.12 18.752 15.981 40.007 34.095 62.099 53.414 2.912 2.55 6.652 3.954 10.532 3.954s7.62-1.404 10.532-3.953c22.094-19.322 43.348-37.435 62.111-53.425 110.41-94.093 183.36-156.25 183.36-247.11 0-84.097-58.468-147.51-136-147.51z fill=red" /></svg>




                                    </div>
                                    <p class="features-heading"> Sections:</p>
                                    <div class="section-text" id="template-sections">
                                        <p class="m-0">Top Couple Image Slider </p>
                                        <p class="m-0">About The Couple</p>
                                        <p class="m-0">The Story</p>
                                        <p class="m-0">Gallery Of The Images</p>
                                        <p class="m-0">Family Members</p>
                                        <p class="m-0">Pre Wedding Video</p>
                                        <p class="m-0">Ceremony Details</p>
                                        <p class="m-0">RSVP</p>


                                    </div>
                                    <p class="features-heading"> Extra Elements:</p>
                                    <p>Blue Butterflies flying on the page</p>
                                    <p class="features-heading"> Number of Sale: <span id="template-sales">20</span></p>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- page-content -->
        </div>
        <!-- #content-wrapper end -->



        <!-- FOOTER -->

        <footer id="footer" class="footer clearfix">
            <div class="footer-wrap clearfix">

                <div class="footer-bottom clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="foot-col column column-3 item-col-1 vertical text-left clearfix">
                                <div id="copyright" class="copyright-text foot-col-item">
                                    © Copyright 2018, Design by <a href="https://pixoloproductions.com/" target="_blank">Pixolo Production</a>
                                </div>
                            </div>
                            <div class="foot-col column column-3 item-col-2 vertical text-center clearfix">
                                <div class="logo-footer foot-col-item">
                                    <a href="https://www.weddingo.in">
                                        <img src="../wp-content/uploads/sites/96/weddingo.png" style="width: 207px;" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="foot-col column column-3 item-col-3 vertical text-right clearfix">
                                <div class="social-footer foot-col-item">
                                    <ul>
                                        <li class="twitter soc-icon">
                                            <plink target="_blank" redirect-to="https://twitter.com/weddingo11" title="Twitter" class="fa fa-twitter" rel="nofollow"></plink>
                                        </li>
                                        <li class="facebook soc-icon">
                                            <plink target="_blank" redirect-to="https://www.facebook.com/weddingowebsite/" title="Facebook" class="fa fa-facebook" rel="nofollow"></plink>
                                        </li>
                                        <li class="instagram soc-icon">
                                            <plink target="_blank" redirect-to="https://www.instagram.com/weddin.go/" title="Instagram" class="fa fa-instagram" rel="nofollow"></plink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER END -->
        <!-- MAIN WRAPPER END -->

        <script type='text/javascript'>
            /* <![CDATA[ */
            var wpcf7 = {
                "apiSettings": {
                    "root": "http:\/\/www.weddingo.com\/wp-json\/contact-form-7\/v1",
                    "namespace": "contact-form-7\/v1"
                },
                "recaptcha": {
                    "messages": {
                        "empty": "Please verify that you are not a robot."
                    }
                },
                "cached": "1"
            };
            /* ]]> */

        </script>
        <!-- <script type='text/javascript' src=''></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/fitvids.js?ver=4.9.8'></script>-->
        <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/wow.js?ver=4.9.8'></script>
        <!-- <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/easing.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/smartmenus.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/owlcarousel.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/infinitescroll.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/isotope.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/headroom.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/animeonscroll.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/bootstrap.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/lightgallery.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/smoothscroll.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/stickykit.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/thumbsplugin.js?ver=4.9.8'></script>-->
        <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/main.js?ver=4.9.8'></script>
        <!--<script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/header1.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.4.1'></script>-->
        <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'></script>
        <!-- <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/swiper/swiper.jquery.min.js?ver=4.4.3'></script>-->
        <script type='text/javascript'>
            /* <![CDATA[ */
            var elementorFrontendConfig = {
                "isEditMode": "",
                "is_rtl": "",
                "breakpoints": {
                    "xs": 0,
                    "sm": 480,
                    "md": 768,
                    "lg": 1025,
                    "xl": 1440,
                    "xxl": 1600
                },
                "urls": {
                    "assets": "http:\/\/www.weddingo.in\/wp-content\/plugins\/elementor\/assets\/"
                },
                "settings": {
                    "page": [],
                    "general": {
                        "elementor_global_image_lightbox": "yes",
                        "elementor_enable_lightbox_in_editor": "yes"
                    }
                },
                "post": {
                    "id": 29,
                    "title": "Contact",
                    "excerpt": ""
                }
            };
            /* ]]> */

        </script>
        <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.1.2'></script>
        <script src="../wp-includes/js/global.js"></script>
    </div>


</body>

</html>
