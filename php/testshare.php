<?php
header("Access-Control-Allow-Origin: *");
// $imagename=$_GET['imagename'];
// // echo $imagename;
$uri=$_GET['uri'];

$imagename=$uri.'.png';
// print_r($imagename);
$couplenames=explode('-',(substr($uri,strrpos($uri,"/")+1)));
// print_r($imagename);
// /coupletag/askshay-ishita


?>



<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!--<title>
        <?php echo $couplenames[3].' - '.$couplenames[1].' and '.$couplenames[2].' have made their Weddingo Couple name using Couple Tag';  ?>
    </title>-->
    <title>#coupletag – couple name generator</title>
    
    <link rel="icon" href="images/Favicon/android-icon-144x144.png" type="image/gif">
    <!--<meta name="description" content="<?php echo $couplenames[1]." and ".$couplenames[2]." have made their Weddingo Couple name using Couple Tag" ; ?>"/>-->
    <meta name="description" content="couple tag a name generator is a couple name generator to provide you with options to build your own trending couple name.">
    
    <meta name="keywords" content="couple name, weddingo, couple name generator" />
    <meta property="og:title" content="<?php echo $couplenames[1]." and ".$couplenames[2]." have made their Weddingo Couple name using Couple Tag" ; ?>" />
    <meta property="og:description" content="<?php echo $couplenames[1]." and ".$couplenames[2]." have made their Weddingo Couple name using Couple Tag" ; ?>" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:site_name" content="Weddingo" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="https://www.weddingo.in/namegenimages/<?php echo $imagename?>" />
    <meta name="twitter:title" content="<?php echo $couplenames[1]." and ".$couplenames[2]." have made their Weddingo Couple name using Couple Tag" ; ?>"/>
    <meta name="twitter:description" content="<?php echo $couplenames[1]." and ".$couplenames[2]." have made their Weddingo Couple name using Couple Tag" ; ?>"/>
    <meta name="twitter:card" content="summary_large_image" />
    <meta property="twitter:account_id" content="1054048240444891136" />
    <meta name="twitter:widgets:csp" content="on">
    <meta name="twitter:domain" content="Weddingo" />
    <meta name="twitter:site" value="@weddingo11">
    <meta name="twitter:url" content="http://www.weddingo.in">
    <link rel="canonical" href="http://www.weddingo.in" />


    <meta property="og:url" content="http://weddingo.in/coupletag/<?php echo $uri;?>" />

</head>
<!--Schema-->
<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "WebSite",
        "name": "weddingo",
        "alternateName": "Online Wedding Invitation Design",
        "url": "http://www.weddingo.in/"
    }

</script>

<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "BreadcrumbList",
        "itemListElement": [{
                "@type": "ListItem",
                "position": "1",
                "item": {
                    "@id": "http://www.weddingo.in",
                    "name": "Home",
                    "image": "http://www.weddingo.in/images/logo.png"
                }
            },
            {
                "@type": "ListItem",
                "position": "2",
                "item": {
                    "@id": "http://www.weddingo.in/about/",
                    "name": "About Us",
                    "image": "http://www.weddingo.in/images/about.png"
                }
            },
            {
                "@type": "ListItem",
                "position": "3",
                "item": {
                    "@id": "http://www.weddingo.in/testimonials/",
                    "name": "Testimonial",
                    "image": "http://www.weddingo.in/images/couples/aksita.png"
                }
            },
            {
                "@type": "ListItem",
                "position": "4",
                "item": {
                    "@id": "http://www.weddingo.in/contact/",
                    "name": "Contact",
                    "image": "http://www.weddingo.in/images/logo2.png"
                }
            }
        ]
    }

</script>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "ProfessionalService",
        "name": "weddingo",
        "image": "http://www.weddingo.in/images/logo.png",
        "@id": "http://www.weddingo.in/",
        "url": "http://www.weddingo.in/",
        "telephone": "+919967552082",
        "priceRange": "600 - 6000",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Somaiya Vidyavihar, SIMSR",
            "addressLocality": "Mumbai",
            "postalCode": "400077",
            "addressCountry": "IN"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": 19.0728088,
            "longitude": 72.8978032
        },
        "openingHoursSpecification": {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": [
                "Monday",
                "Tuesday",
                "Wednesday",
                "Friday",
                "Saturday",
                "Sunday"
            ],
            "opens": "12:00",
            "closes": "19:00"
        },
        "sameAs": [
            "https://www.facebook.com/weddingowebsite/?modal=admin_todo_tour",
            "https://twitter.com/weddingo11",
            "https://plus.google.com/u/2/108912068653400595538",
            "https://www.instagram.com/weddin.go/"
        ]
    }

</script>

<body>
    
    <style>
        body {
            background: black;
            /*overflow-y: hidden;*/
        }

        .share-wrapper {
            width: 100%;
        }

        .logo-wrap {
            float: left;
            width: 100%;
        }

        .logo-img {
            padding: 10px;
            width: 212px;
        }

        .image-wrapper {
            width: 30%;
            margin: auto;
        }

        .couple-couple-wrapper {
            visibility: visible;
            position: absolute;
            top: 18%;
            z-index: 100;
            background-color: #f7f2f3;
            left: 50%;
            transform: translateX(-50%);
            box-shadow: 0 0 5px 2px #716e6e;
            height: 8%;
            width: 31%;
            margin: auto;

        }

        .couple-greeting-wrapper {
            border: 12px solid white;
            position: relative;
            background: white;

        }

        .hiding-class {
            display: block;
        }

        .perspective-wrapper {
            position: absolute;
            top: 21%;
            text-align: center;
            width: 100%;
            perspective: 900px;
        }

        .names-wrap {

            text-align: center;
            width: 80%;
            top: 80px;
            left: 10%;
            height: 50%;

            transform: rotateY(354deg) rotateX(412deg) rotateZ(23deg);
        }

        .couples-individual-name {}

        .generated-names {
            POSITION: absolute;
            /* bottom: 0px !important; */
            top: 254%;
            left: 30%;
            font-size: 1.2em;
            font-weight: 600;
            font-family: Raleway;
        }


        .btn-wrap {
            margin-top: 15px;
            width: 100%;
            position: relative;
            left: 25%;

        }

        .button {
            background-color: #4CAF50;
            /* Green */
            border: none;
            border-radius: 15px;
            color: white;
            padding: 12px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            margin: auto;
        }

        @media (max-width:876px) {

            .couple-couple-wrapper {
                width: 82% !important;
            }

            .btn-wrap {
                left: 5%;
            }
        }

        @media (max-width:650px) {

            .couple-couple-wrapper {
                width: 82% !important;
            }

            .btn-wrap {
                left: 20%;
            }
        }

        @media(max-width:400px) {

            .couple-couple-wrapper {
                width: 82% !important;
            }

            .btn-wrap {
                left: 5%;
            }
        }

    </style>

    <body>

        <div class="logo-wrap">
            <a href="http://www.weddingo.in">
                <img src="images/logo.png" class="logo-img">
            </a>

        </div>
        <!--<div class="share-wrapper">
                <div class="image-wrapper">
                    <img src="images/couple-greet.jpg">

                </div>
            </div>
-->

        <div class="couple-couple-wrapper wow animated zoomIn">
        
        
        
        
        
            <div class="couple-greeting-wrapper">
               
               <img src="http://www.weddingo.in/namegenimages/<?php echo $imagename;?>" title="<?php echo $couplenames[1]." and ".$couplenames[2]." have made their Weddingo Couple name using Couple Tag at Weddingo - the online wedding Invitaiton Card" ; ?> " alt="<?php echo $couplenames[1]." and ".$couplenames[2]." have made their Weddingo Couple name using Couple Tag at Weddingo - the online wedding Invitaiton Card" ; ?>"  class="greet-image" />
<!--
                <img src="./images/couple-greet.jpg" class="greet-image" style="width: 100%;">

                <div class="perspective-wrapper">
                    <div class="names-wrap">
                        <p class="his-name couples-individual-name"><span id="boy-name"></span> <span style="text-transform: lowercase; font-weight: 600; color: red;"> and </span> <span id="girl-name"></span></p>
                        <p class="his-name and-text">and</p>

                    <p class="his-name her-name">jhjkhjkg</p>
                    <p class="his-name known-as">a.k.a.</p>
                        <p class="generated-names" id="couple-name"><span class="hashtag">#</span></p>
                    </div>
                </div>
-->


            </div>
            <div class="btn-wrap animated zoomIn">
                <button class="button button1">Find Your Couple Name </button>
            </div>
        </div>


    </body>










</body>

</html>
