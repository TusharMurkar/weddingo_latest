<?php
    $url= 'savethedates.json';
    $data= file_get_contents($url);
    $data = json_decode($data);
?>

<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">


    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <meta https-equiv="expires" content="0">
    <meta name="description" content="Create save the dates cards online to send to your wedding guest list. Weddingo helps you to design a unique online invitation card.">
    <meta name="keywords" content="save the date cards, save the date templates, create a save the date, create a save the date online, design a save the date">

    <title>Save the Date Cards | Online Save the Dates Templates - Weddingo</title>


    <link rel='stylesheet' id='nikah-responsive-css-css' href='../wp-includes/css/couple_name_generator-styles.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='nikah-responsive-css-css' href='https://www.weddingo.in/wp-content/themes/nikah/css/responsive.css' type='text/css' media='all' />



    <link rel="icon" href="../images/Favicon/android-icon-192x192.png" type="image/gif">
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="canonical" href="https://www.weddingo.in/" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="src/css/style.css">

    <style type="text/css">
        #name-list>p {
            text-decoration: underline;
            text-transform: uppercase !important;
            cursor: pointer;
            font-size: 22px;
            transition: font-size .7s;
        }

        .gallery-a{
            color:white;
        }

        .gallery-a:hover{
            color:black;
        }

        .active-option {
            border: 2px solid #2da854 !important ;
            color:#23b74e !important;
        }

        .Link {
            transition: 0.3s;
            border: 2px solid #cab5b5 ;
        }

        .Link:hover {
            border: 2px solid #2da854 ;
            color: #5F5F5F ;
        }

        #name-list>p:hover {
            color: #f7f3f3;
            font-size: 30px;
            font-weight: bold;

        }

        .comname:hover {
            color: #23b74e !important;
        }

        #couple-name {
            position: static !important;
            font-size: 40px !important;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.7);
            background: white;
            padding: 12px 16px;
            z-index: 1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
            color: #403e3e !important;
        }

        .cool-link {
            padding-top: 12px;
            display: inline-block;
            text-decoration: none;
            color: #403e3e !important;
        }

        .cool-link:hover {
            color: #403e3e;
        }

        .cool-link::after {
            content: '';
            display: block;
            width: 0;
            height: 2px;
            background: #403e3e;
            transition: width .3s;
        }

        .fa {
            padding-left: 6px;
            cursor: pointer;
        }

        #link1:hover::after {
            width: 72% !important;
        }

        .cool-link:hover::after {
            width: 100%;
        }

        @-webkit-keyframes pulse {
            from {
                -webkit-transform: scale3d(1, 1, 1);
                transform: scale3d(1, 1, 1);
            }

            50% {
                -webkit-transform: scale3d(1.05, 1.05, 1.05);
                transform: scale3d(1.05, 1.05, 1.05);
            }

            to {
                -webkit-transform: scale3d(1, 1, 1);
                transform: scale3d(1, 1, 1);
            }
        }

        @keyframes pulse {
            from {
                -webkit-transform: scale3d(1, 1, 1);
                transform: scale3d(1, 1, 1);
            }

            50% {
                -webkit-transform: scale3d(1.05, 1.05, 1.05);
                transform: scale3d(1.05, 1.05, 1.05);
            }

            to {
                -webkit-transform: scale3d(1, 1, 1);
                transform: scale3d(1, 1, 1);
            }
        }

        .pulse {
            -webkit-animation-name: pulse;
            animation-name: pulse;
        }

        @media only screen and (max-width:768px) {
            .cool-link:hover::after {
                width: 0%;
            }

            #link1:hover::after {
                width: 0% !important;
            }

            .dropdown-content {
                width: 100%;
                background: white;
            }

            .dropdown:hover .dropdown-content {
                display: none;
                color: #403e3e !important;
            }

            .active-dropdown {
                display: block !important;
            }
        }

    </style>

    <!--Schema-->
    <script type="application/ld+json">
        {
            "@context": "https://schema.org/",
            "@type": "WebSite",
            "name": "weddingo",
            "alternateName": "Online Wedding Invitation Design",
            "url": "https://www.weddingo.in/"
        }

    </script>

    <script type="application/ld+json">
        {
            "@context": "https://schema.org/",
            "@type": "BreadcrumbList",
            "itemListElement": [{
                    "@type": "ListItem",
                    "position": "1",
                    "item": {
                        "@id": "https://www.weddingo.in",
                        "name": "Home",
                        "image": "https://www.weddingo.in/images/logo.png"
                    }
                },
                {
                    "@type": "ListItem",
                    "position": "2",
                    "item": {
                        "@id": "https://www.weddingo.in/about/",
                        "name": "About Us",
                        "image": "https://www.weddingo.in/images/about.png"
                    }
                },
                {
                    "@type": "ListItem",
                    "position": "3",
                    "item": {
                        "@id": "https://www.weddingo.in/testimonials/",
                        "name": "Testimonial",
                        "image": "https://www.weddingo.in/images/couples/aksita.png"
                    }
                },
                {
                    "@type": "ListItem",
                    "position": "4",
                    "item": {
                        "@id": "https://www.weddingo.in/contact/",
                        "name": "Contact",
                        "image": "https://www.weddingo.in/images/logo2.png"
                    }
                }
            ]
        }

    </script>
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "ProfessionalService",
            "name": "weddingo",
            "image": "https://www.weddingo.in/images/logo.png",
            "@id": "https://www.weddingo.in/",
            "url": "https://www.weddingo.in/",
            "telephone": "+919967552082",
            "priceRange": "600 - 6000",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Somaiya Vidyavihar, SIMSR",
                "addressLocality": "Mumbai",
                "postalCode": "400077",
                "addressCountry": "IN"
            },
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": 19.0728088,
                "longitude": 72.8978032
            },
            "openingHoursSpecification": {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Friday",
                    "Saturday",
                    "Sunday"
                ],
                "opens": "12:00",
                "closes": "19:00"
            },
            "sameAs": [
                "https://www.facebook.com/weddingowebsite/?modal=admin_todo_tour",
                "https://twitter.com/weddingo11",
                "https://plus.google.com/u/2/108912068653400595538",
                "https://www.instagram.com/weddin.go/"
            ]
        }

    </script>

    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>


</head>

<body id="body" style="background-color:#ecdddd !important;" class="page-template page-template-template page-template-page-builder-template page-template-templatepage-builder-template-php page page-id-42 header-style-1 elementor-default elementor-page elementor-page-42">


    <div id="main-wrapper" class="main-wrapper clearfix">

        <div id="sticky-wrap-head" class="sticky-header-wrap header_fixed_noscroll clearfix">
            <!-- Header
		============================================= -->
            <header id="header" class="header-style-1-wrap inner-head-wrap alt-head animated  clearfix">

                <div class="container clearfix">

                    <div class="header-clear  clearfix">
                        <div class="fl header1-2 horizontal header_left_float clearfix">
                            <!-- Logo
============================================= -->
                            <div class="logo head-item">

                                <div class="logo-image animated zoomIn">
                                    <a href="https://www.weddingo.in">
                                        <img class="logo-img" src="../images/logo.png" alt="logo-alt" />
                                    </a>
                                </div>
                            </div>
                            <!-- end logo -->
                        </div>

                        <div class="fr header1-2 vertical header_right_float clearfix">
                            <!-- Mobile menu toggle button (hamburger/x icon) -->
                            <input id="main-menu-state" name="" type="checkbox" />
                            <label class="main-menu-btn sub-menu-triger" for="main-menu-state">
                                <span class="main-menu-btn-icon"></span>
                            </label>

                            <!-- Primary Navigation
============================================= -->
                            <nav id="primary-menu" class="menu main-menu head-item">
                                <ul id="menu-menu" class="sm sm-clean menu--ferdinand">
                                    <li id="menu-item-41" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home  page_item page-item-8 current_page_item menu-item-41">
                                        <a class="menu__link" href="../">Home</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40 dropdown">
                                        <a class="menu__link dropbtn">More Products<i class="fa fa-caret-down"></i></a>
                                        <div class="dropdown-content">
                                            <a id="link1" class="cool-link" href="../couple-name-generator/">Couplename</a>
                                            <a id="link1" class="cool-link" href="https://albums.weddingo.in/" target="_blank" rel="noopener">Photo Albums</a>
                                            <a class="cool-link" href="../invitation-designs/">Wedding Invitation</a>
                                            <a class="cool-link" href="">Save the date</a>
                                        </div>
                                    </li>
                                    <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a class="menu__link" href="../about/">About Us</a>
                                    </li>
                                    <li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a class="menu__link" href="../portfolio/">Our Couples</a>
                                    </li>
                                    <li id="menu-item-40" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a class="menu__link" href="../contact/">Contact</a>
                                    </li>
                                    <li id="menu-item-87" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87"><a class="weddingo-demo" href="../invitation-designs/">Weddingo Demo</a>
                                    </li>
                                </ul>
                            </nav>

                            <!-- end primary menu -->

                        </div>
                    </div>

                </div>

            </header>
        </div>

        <!-- HEADER END -->
        <!-- CONTENT WRAPPER
============================================= -->
        <!--main section-->
        <style>
            .first{
                width:100%;
                margin-top:70px;
            }
        </style>
        <div class="first-wrapper">
            <div class="overlay">
                <div class="container header-wrapper">
                   <div class="first">
                    <h1 class="centered">Save The Date </h1>
                    <p class="centered_small"> Templates</p>
                    <p class="centered_last">Our templates are hand created to look <br>beautiful and elegant on mobile
                        devices
                        too.</p>
    </div>

    <button type="button" style="margin:3% 0% 0% 0%" class="btn btn-outline-secondary couple-name-btn"><a class="gallery-a" href="#gallery">Get Save the Dates</a></button>

    <a href="#gallery" class="fa fa-angle-double-down wow animated bounce infinite animated" style="font-size: 48px; color: white; margin-top: 40px; visibility: visible; animation-name: bounce;"></a>



    <div class="elementor-shape elementor-shape-bottom" data-negative="false">
                                    <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
                                        <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                        <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                        <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                    </svg> </div>


                </div>
            </div>
        </div>
        <!---->
        <div class="" style="padding: 50px;font-family: Raleway;color: #000000; background-color: white;">
            <h3 style="text-align:center;font-weight: bold;margin: 40px 0 21px; font-family:  letter-spacing: 0px;font-size: 1.8em; animation: increase-spacing;animation-duration: 10s;animation-fill-mode: forwards;">#savethedate</h3>
            <h3 style="text-align:center;">Save The Date</h3>
            <p style="font-size: 1em;"> Congratulations on your engagement!  Once you’ve booked a wedding venue, it’s time to start giving out save the dates. Just like the <a href="https://www.weddingo.in/couple-name-generator/" style="color:#2da854">couple name generator</a>, the Save the Date feature of Weddingo helps you to design a unique online invitation card. This little piece of wedding stationery serves as a heads-up to guests so they can mark their calendars and begin making travel plans to attend your celebration. Save the Date feature from Weddingo lets them know that there is a formal wedding invitation on the way with all the details. This means you can keep your Save the Date short, simple, and to the point.</p>
        </div>

        <!-- GALLERY START -->
        <section class="gallery section-padding" style="margin:2%" id="gallery">

            <div class="container">
                <div class="row">


                    <div class="textfont galery-filter-btns">
                        <button class="Link  filter-button  active-option" style="font-size: 16px !important" data-filter="all">ALL</button>
                        <?php foreach ($data as $d) { ?>
                        <button class="Link filter-button " style="font-size: 16px !important" data-filter="<?php echo $d->code;?>">
                            <?php echo $d->name; ?>
                        </button>
                        <?php } ?>


                    </div>
                    <br />


                    <!-- <div class="wrapper">

                        <?php 
                        foreach ($data as $d) {
                            foreach($d->images as $image){
                                ?>
                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter <?php echo $d->code;?>">

                            <img class="adjust" src="./src/image/<?php echo $image->img; ?>" class="img-responsive ">

                        </div>
                        <?php }} ?>

                    </div> -->

                    <div class="wrapper-mobile">
                    <?php 
                        foreach ($data as $d) {
                            foreach($d->images as $image){
                                ?>
                    <div class="filter outer-div <?php echo $d->code;?>">
                    <img class="phone-back" src="./src/image/mobile_phone.png">
                            <img class="image-mob" src="./src/image/<?php echo $image->img; ?>" alt="">

                        </div>
                        <?php }} ?>

                    </div>

                    <style>

                        .outer-div{
                            position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-basis: 20%;
    margin: 2%;
                        }
                            .wrapper-mobile {
                                display: flex;
    flex-wrap: wrap;
    min-height: 48vh;
    justify-content: center;
                              
                            }

                            .image-mob{
                                width: 92.5%;
    z-index: 100;
    position: absolute;
    margin-top: -5%;
                            }


                            .phone-back {
                                
  
                                background-size: 100%;
    /* width: 23%; */
    /* margin: 21%; */
    background-repeat: no-repeat;
                                     }

                                    @media screen and (max-width: 640px)
                                    {
                                        .wrapper-mobile{
                                            display: flex;
    flex-wrap: wrap;
    /* min-height: 48vh; */
    justify-content: center;
                                        }

                                        .phone-back{
                                            background-image: url(./src/image/mobile_phone.png);
    background-size: contain;
    width: 93%;
    /* height: 106%; */
    background-repeat: no-repeat;
                                        }

                                        .image-mob{
                                            max-width: 100%;
    height: auto;
    /* padding: 26% 3.4% 45% 3.4%;  */
    padding: 21% 2.58% 45% 2.58%;
    }
                                    }


                    


                                                            
                        </style>


                </div>

            </div>


        </section>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>


        <!-- #content-wrapper end -->

        <!--POPUP OVERLAY-->
        <div class="popup-overlay">


            <!--SHARE POPUP-->
            <div class="couple-couple-wrapper couple-popup" id="couple-image-wrapper">
                <!--Use class="hiding-class" to hide this popup here-->
                <div class="couple-greeting-wrapper">
                    <div id="coupletagimage">
                        <img src="../images/greeting.jpg" class="greet-image" style="width: 100%;" alt="">

                        <div class="perspective-wrapper">
                            <div class="names-wrap">
                                <p class="his-name couples-individual-name"><span id="boy-name"></span> <span style="text-transform: lowercase; font-weight: 600; color: red;"> and </span>
                                    <span id="girl-name"></span></p>
                                <!--<p class="his-name and-text">and</p>

                    <p class="his-name her-name">jhjkhjkg</p>
                    <p class="his-name known-as">a.k.a.</p>-->
                                <p class="generated-names" id="couple-name"><span class="hashtag">#</span></p>
                            </div>
                        </div>
                    </div>
                    <p class="share-title">You finally have a cool couple name for you and your partner. We can’t wait
                        to hear reactions from your loved ones on your new couple name. Do you wish to share the results
                        with your friends and family </p>

                    <div class="row social-icons-wrapper">
                        <img onclick="uploadcouplenameimage('whatsapp')" src="../images/social-icons/whatsapp-o.png" class="social-icons animated pulse infinite" alt="">
                        <img onclick="uploadcouplenameimage('facebook')" src="../images/social-icons/facebook-o.png" class="social-icons animated pulse infinite" alt="">
                        <img onclick="uploadcouplenameimage('twitter')" src="../images/social-icons/twitter-o.png" class="social-icons animated pulse infinite" alt="">
                    </div>
                </div>
            </div>


            <!--VISIT POPUP-->
            <div class="visit-wrapper couple-popup" id="visit-wrapper">
                <img src="../images/logo2.png" class="visit-logo" style="margin: auto;width: 36%;" alt="">
                <p class="visit-title">Visit our Website to check out more exciting new Features and Products</p>
                <a href="../"><button type="button" class="btn btn-outline-secondary couple-name-btn visit-btn">Visit Now</button></a>
            </div>

        </div>

        

        <!-- FOOTER -->
        <footer id="footer" class="footer clearfix">
            <div class="footer-wrap clearfix">

                <div class="footer-bottom clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="foot-col column column-3 item-col-1 vertical text-left clearfix">
                                <div id="copyright" class="copyright-text foot-col-item">
                                    © Copyright <span id="current_year"></span>, Design by <a href="https://pixoloproductions.com/" target="_blank" class="comname" rel="noopener">Pixolo Production</a>
                                </div>
                            </div>
                            <div class="foot-col column column-3 item-col-2 vertical text-center clearfix">
                                <div class="logo-footer foot-col-item">
                                    <a href="https://www.weddingo.in">
                                        <img src="../wp-content/uploads/sites/96/weddingo.png" style="width: 207px;" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="foot-col column column-3 item-col-3 vertical text-right clearfix">
                                <div class="social-footer foot-col-item">
                                    <ul>
                                        <li class="twitter soc-icon">
                                            <plink target="_blank" redirect-to="https://twitter.com/weddingo11" title="Twitter" class="fa fa-twitter" rel="nofollow"></plink>
                                        </li>
                                        <li class="facebook soc-icon">
                                            <plink target="_blank" redirect-to="https://www.facebook.com/weddingowebsite/" title="Facebook" class="fa fa-facebook" rel="nofollow"></plink>
                                        </li>
                                        <li class="instagram soc-icon">
                                            <plink target="_blank" redirect-to="https://www.instagram.com/weddin.go/" title="Instagram" class="fa fa-instagram" rel="nofollow"></plink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER END -->
        <style>
            .hiring-popup {
                position: fixed;
                bottom: 0;
                left: 0;
                width: 150px;
                cursor: pointer;
            }

            .hiring-popup img {
                margin: auto;
            }

            .hiring-popup-text {
                color: white;
                text-align: center;
                text-transform: uppercase;
                text-shadow: 0px 0px 5px #292929;
            }
        </style>
<!--         <div class="hiring-popup">
            <a href="../career/" target="_blank">
                <img src="../images/hiring-img.png">
                <p class="hiring-popup-text">We Are Hiring</p>
            </a>
        </div> -->

        <div id="popup"> </div>
        <script>
            jQuery(function() {
                jQuery("#popup").load("../popup/popup.html");
                console.log("popup");
            });

            jQuery(".dropbtn").on('click', function() {
                jQuery(".dropdown-content").toggleClass("active-dropdown");
                console.log("happening");

            });

            jQuery(document).ready(function() {
                console.log("ready!");
                jQuery("#main-menu-state").prop("checked", false);
            });

        </script>

    </div>
    <!-- MAIN WRAPPER END -->

    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.2'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/fitvids.js?ver=4.9.8'></script>-->
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/wow.js?ver=4.9.8'></script>
    <!--<script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/easing.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/smartmenus.js?ver=4.9.8'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/owlcarousel.js?ver=4.9.8'></script>-->
    <!--<script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/infinitescroll.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/isotope.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/headroom.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/animeonscroll.js?ver=4.9.8'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/bootstrap.js?ver=4.9.8'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/lightgallery.js?ver=4.9.8'></script>-->
    <!--<script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/smoothscroll.js?ver=4.9.8'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/stickykit.js?ver=4.9.8'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/thumbsplugin.js?ver=4.9.8'></script>-->
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/main.js?ver=4.9.8'></script>
    <!--<script type='text/javascript' src='https://www.weddingo.in/wp-content/themes/nikah/js/header1.js?ver=4.9.8'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>-->
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.4.1'></script>-->
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'></script>
    <!--    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/lib/swiper/swiper.jquery.min.js?ver=4.4.3'></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var elementorFrontendConfig = {
            "isEditMode": "",
            "is_rtl": "",
            "breakpoints": {
                "xs": 0,
                "sm": 480,
                "md": 768,
                "lg": 1025,
                "xl": 1440,
                "xxl": 1600
            },
            "urls": {
                "assets": "https:\/\/www.weddingo.in\/wp-content\/plugins\/elementor\/assets\/"
            },
            "settings": {
                "page": [],
                "general": {
                    "elementor_global_image_lightbox": "yes",
                    "elementor_enable_lightbox_in_editor": "yes"
                }
            },
            "post": {
                "id": 42,
                "title": "About",
                "excerpt": ""
            }
        };
        /* ]]> */

    </script>
    <script type='text/javascript' src='couplenamegen.js'></script>
    <script type='text/javascript' src='https://www.weddingo.in/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.1.2'></script>
    <!--	script to run plinks-->
    <script src="../wp-includes/js/global.js"></script>
    <script src="isotopes.min.js"></script>
    <script>
        $(document).ready(function() {

            $(".filter-button").click(function() {
                var value = $(this).attr('data-filter');
                console.log(value);
                if (value == "all") {
                    //$('.filter').removeClass('hidden');
                    $('.filter').show('1000');
                } else {
                    //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
                    //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                    $(".filter").not('.' + value).hide('3000');
                    $('.filter').filter('.' + value).show('3000');

                }


                if ($(".filter-button").removeClass("active-option")) {
                    $(this).removeClass("active-option");
                }
                $(this).addClass("active-option");
            });



        });

    </script>

    <!-- anchor smooth scroll -->
<script>
$(document).on('click', 'a[href^="#gallery"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
});
</script>


</body>

</html>
