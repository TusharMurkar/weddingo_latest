<?php
    $url= 'portfolio/portfolio.json';
    $data= file_get_contents($url);
    $data = json_decode($data);
?>



<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <!--[if ie]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link href="https://fonts.googleapis.com/css?family=Montserrat|Montserrat+Alternates&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mukta&display=swap" rel="stylesheet">
    <meta http-equiv="expires" content="0">
    <meta name="description"
        content="We at Weddingo an online wedding invitation card design aim to give you the best experience at creating your own online wedding invitation card.">
    <meta name="keywords"
        content="online wedding invitation,wedding invitation, invite, invitation card,wedding invitation card design,wedding invitations">

    <title>Online Wedding Invitation Design | Weddingo</title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="icon" href="images/Favicon/android-icon-144x144.png" type="image/gif">
    <link rel='stylesheet' id='elementor-global-css' href='wp-includes/css/index-styles.min.css?ver=1.5' type='text/css'
        media='all' />
    <link rel='stylesheet' id='nikah-responsive-css-css' href='wp-content/themes/nikah/css/responsive.css'
        type='text/css' media='all' />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <script type="application/ld+json">
        {
            "@context": "https://schema.org/",
            "@type": "WebSite",
            "name": "weddingo",
            "alternateName": "Online Wedding Invitation Design",
            "url": "https://www.weddingo.in/"
        }

    </script>

    <script type="application/ld+json">
        {
            "@context": "https://schema.org/",
            "@type": "BreadcrumbList",
            "itemListElement": [{
                "@type": "ListItem",
                "position": "1",
                "item": {
                    "@id": "https://www.weddingo.in",
                    "name": "Home",
                    "image": "https://www.weddingo.in/images/logo.png"
                }
            }, {
                "@type": "ListItem",
                "position": "2",
                "item": {
                    "@id": "https://www.weddingo.in/about/",
                    "name": "About Us",
                    "image": "https://www.weddingo.in/images/about.png"
                }
            }, {
                "@type": "ListItem",
                "position": "3",
                "item": {
                    "@id": "https://www.weddingo.in/testimonials/",
                    "name": "Testimonial",
                    "image": "https://www.weddingo.in/images/couples/aksita.png"
                }
            }, {
                "@type": "ListItem",
                "position": "4",
                "item": {
                    "@id": "https://www.weddingo.in/contact/",
                    "name": "Contact",
                    "image": "https://www.weddingo.in/images/logo2.png"
                }
            }]
        }

    </script>










    <script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>



    <script type='text/javascript'
        src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.3.1.5'></script>
    <script type='text/javascript'
        src='wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.3.1.5'></script>

    <link rel="canonical" href="https://weddingo.in/" />
    <style type="text/css">
        #titlelove {
            line-height: 5 !important;
        }



        .comname:hover {
            color: #23b74e !important;
        }

        #otherbtn:hover {
            background-color: #328332 !important;
        }

        #btnhover {
            font-size: 22px !important;
            transition: font-size .7s;
        }

        #btnhover:hover {
            font-size: 24px !important;
        }

        #tryitnow {
            width: 140px !important;
            background-color: #21b552 !important;
        }

        #tryitnow:hover {
            background-color: #328332 !important;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.7);
            background: white;
            padding: 12px 16px;
            z-index: 1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
            color: #403e3e !important;
        }

        .cool-link {
            padding-top: 12px;
            display: inline-block;
            text-decoration: none;
            color: #403e3e !important;
        }

        .cool-link:hover {
            color: #403e3e;
        }

        .cool-link::after {
            content: '';
            display: block;
            width: 0;
            height: 2px;
            background: #403e3e;
            transition: width .3s;
        }

        .fa {
            padding-left: 6px;
        }

        #link1:hover::after {
            width: 72% !important;
        }

        .cool-link:hover::after {
            width: 100%;
        }

        .view-btn-logo {
            display: inline;
            width: 120px;
        }

        @-webkit-keyframes animatedarrow {
            0% {
                margin-left: 4px;
            }

            50% {
                margin-left: 12px;
            }

            100% {
                margin-left: 4px;
            }
        }

        /* Standard syntax */
        @keyframes animatedarrow {
            0% {
                margin-left: 4px;
            }

            50% {
                margin-left: 12px;
            }

            100% {
                margin-left: 4px;
            }
        }

        .view-btn-arrow {
            width: 10px;
            height: 10px;
            margin-left: 4px;
            -webkit-animation: animatedarrow 2s infinite;
            animation: animatedarrow 2s infinite;
        }

        .view-text {
            margin: 0;
            padding: 9px 5px 0 0 !important;
            font-size: 15px;
            line-height: 15px;
        }

        .view-link {
            width: 100%;
            display: flex;
            align-items: center;
            /*			justify-content: center;*/
            font-size: 20px;
        }

        @media only screen and (max-width:768px) {
            .cool-link:hover::after {
                width: 0%;
            }

            #link1:hover::after {
                width: 0% !important;
            }

            .dropdown-content {
                width: 100%;
                background: white;
            }

            .dropdown:hover .dropdown-content {
                display: none;
                color: #403e3e !important;
            }

            .active-dropdown {
                display: block !important;
            }
        }
    </style>




</head>

<body id="body"
    class="home page-template page-template-template page-template-page-builder-template page-template-templatepage-builder-template-php page page-id-8 header-style-1 elementor-default elementor-page elementor-page-8">

    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <div id="main-wrapper" class="main-wrapper clearfix">

        <div id="sticky-wrap-head" class="sticky-header-wrap header_fixed_noscroll clearfix">

            <header id="header" class="header-style-1-wrap inner-head-wrap alt-head animated  clearfix">

                <div class="container clearfix">

                    <div class="header-clear clearfix">
                        <div class="fl header1-2 horizontal header_left_float clearfix">

                            <div class="logo head-item">

                                <div class="logo-image">
                                    <a href="">
                                        <img class="logo-img" src="images/logo.png" alt="weddingo-logo" />
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="fr header1-2 vertical header_right_float clearfix">
                            <input id="main-menu-state" type="checkbox" />
                            <label class="main-menu-btn sub-menu-triger" for="main-menu-state">
                                <span class="main-menu-btn-icon"></span>
                            </label>


                            <nav id="primary-menu" class="menu main-menu head-item">
                                <ul id="menu-menu" class="sm sm-clean menu--ferdinand">
                                    <li id="menu-item-41"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-41">
                                        <a class="menu__link" href="" >Home</a>
                                    </li>
                                    <li
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40 dropdown">
                                        <a class="menu__link dropbtn">More Products<i class="fa fa-caret-down"></i></a>
                                        <div class="dropdown-content">
                                            <a id="link1" class="cool-link" target="_blank"
                                                href="couple-name-generator/" >Couplename</a>
                                            <a id="link1" class="cool-link" href="https://albums.weddingo.in/"
                                                target="_blank" rel="noopener">Photo Albums</a>
                                            <a class="cool-link" target="_blank" href="invitation-designs/">Wedding
                                                Invitation</a>
                                            <a class="cool-link" target="_blank" href="save-the-date/" >Save The
                                                Dates</a>
                                        </div>
                                    </li>
                                    <li id="menu-item-48"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a
                                            class="menu__link" href="about/" >About Us</a>
                                    </li>
                                    <li id="menu-item-56"
                                        class="menu-item menu-item-type-post_type  menu-item-object-page menu-item-56">
                                        <a class="menu__link" href="portfolio/" >Our Couples</a>
                                    </li>
                                    <li id="menu-item-40"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-40"><a
                                            class="menu__link" href="contact/">Contact</a>
                                    </li>
                                    <li id="menu-item-87"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-87"><a
                                            class="weddingo-demo" target="_blank" href="invitation-designs/" onclick="invitations()">Weddingo
                                            Demo</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>

                </div>

            </header>

        </div>


        <div id="content" class="content-wrapper clearfix">

            <div class="page-content clearfix">
                <div class="elementor elementor-8">
                    <div class="elementor-inner">
                        <div class="elementor-section-wrap">
                            <section data-id="dmqfnvk"
                                class="elementor-element elementor-element-dmqfnvk elementor-section-height-full elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                data-element_type="section">
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="klxsdgu"
                                            class="elementor-element elementor-element-klxsdgu elementor-column elementor-col-100 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="1evp12m"
                                                        class="elementor-element elementor-element-1evp12m elementor-widget elementor-widget-nikah-slider"
                                                        data-element_type="nikah-slider.default">
                                                        <div class="elementor-widget-container">


                                                            <link
                                                                href="https://fonts.googleapis.com/css?family=Raleway:400%2C500|Roboto:500|Open+Sans:300"
                                                                rel="stylesheet" property="stylesheet" type="text/css"
                                                                media="all">
                                                            <div id="rev_slider_1_1_wrapper"
                                                                class="rev_slider_wrapper fullscreen-container"
                                                                data-source="gallery"
                                                                style="background-color:#ffffff;padding:0px;">

                                                                <div id="rev_slider_1_1"
                                                                    class="rev_slider fullscreenbanner"
                                                                    style="display:none;" data-version="5.3.1.5">
                                                                    <ul>

                                                                        <li data-index="rs-1"
                                                                            data-transition="slidingoverlaydown"
                                                                            data-slotamount="default"
                                                                            data-hideafterloop="0"
                                                                            data-hideslideonmobile="off"
                                                                            data-easein="default" data-easeout="default"
                                                                            data-masterspeed="default" data-rotate="0"
                                                                            data-saveperformance="off"
                                                                            data-title="Online Invitation"
                                                                            data-param1="" data-param2="" data-param3=""
                                                                            data-param4="" data-param5="" data-param6=""
                                                                            data-param7="" data-param8="" data-param9=""
                                                                            data-param10="" data-description="">

                                                                            <img src="wp-content/uploads/sites/96/2017/11/slider1s.jpeg"
                                                                                alt="Online Wedding Invitation - Weddingo"
                                                                                title="slider1" width="1920"
                                                                                height="1280"
                                                                                data-bgposition="center center"
                                                                                data-bgfit="cover"
                                                                                data-bgrepeat="no-repeat"
                                                                                class="rev-slidebg" data-no-retina>



                                                                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                                                                id="slide-1-layer-3"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['middle','middle','middle','middle']"
                                                                                data-voffset="['0','0','0','0']"
                                                                                data-width="full" data-height="full"
                                                                                data-whitespace="nowrap"
                                                                                data-type="shape" data-basealign="slide"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":800,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 5;    background-color: rgba(0, 0, 0, 0.64);">
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-1-layer-1"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['178','151','218','186']"
                                                                                data-fontsize="['200','160','130','90']"
                                                                                data-lineheight="['180','150','130','90']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['center','center','center','center']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 6; white-space: nowrap; font-size: 200px; line-height: 180px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:hensaregular, hensa;">
                                                                                <h1
                                                                                    style="color: inherit; font: inherit;">
                                                                                    Online <br> invitation</h1>
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-1-layer-2"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['581','494','523','394']"
                                                                                data-fontsize="['20','20','20','16']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">
                                                                                Go Paperless with Online Wedding Invitations</div>


                                                                            <div class="tp-caption rev-btn rev-hiddenicon "
                                                                                id="slide-1-layer-8"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['632','536','575','444']"
                                                                                data-fontsize="['14','14','14','12']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2200,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.30);bw:1 1 1 1;"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[12,12,12,12]"
                                                                                data-paddingright="[35,35,35,35]"
                                                                                data-paddingbottom="[12,12,12,12]"
                                                                                data-paddingleft="[35,35,35,35]"
                                                                                style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:uppercase;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:1px;">
                                                                                <a href="contact/" style="color: white;"
                                                                                    target="_new" onclick="purchase('')">Purchase</a> <i
                                                                                    class="fa-icon-chevron-right"></i>
                                                                            </div>


                                                                            <div class="tp-caption rev-scroll-btn "
                                                                                id="slide-1-layer-10"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['bottom','bottom','bottom','bottom']"
                                                                                data-voffset="['50','50','50','50']"
                                                                                data-width="35" data-height="55"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2600,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 300;font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:3px 3px 3px 3px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                                                                <span>
                                                                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li data-index="rs-2"
                                                                            data-transition="slidingoverlayright"
                                                                            data-slotamount="default"
                                                                            data-hideafterloop="0"
                                                                            data-hideslideonmobile="off"
                                                                            data-easein="default" data-easeout="default"
                                                                            data-masterspeed="default" data-rotate="0"
                                                                            data-saveperformance="off"
                                                                            data-title="Online Invitation"
                                                                            data-param1="" data-param2="" data-param3=""
                                                                            data-param4="" data-param5="" data-param6=""
                                                                            data-param7="" data-param8="" data-param9=""
                                                                            data-param10="" data-description="">

                                                                            <img src="wp-content/uploads/sites/96/2017/11/slider3s.jpg"
                                                                                alt="Online Wedding Invitation - Weddingo"
                                                                                title="slider2" width="1920"
                                                                                height="1280"
                                                                                data-bgposition="center center"
                                                                                data-bgfit="cover"
                                                                                data-bgrepeat="no-repeat"
                                                                                class="rev-slidebg" data-no-retina>



                                                                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                                                                id="slide-2-layer-3"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['middle','middle','middle','middle']"
                                                                                data-voffset="['0','0','0','0']"
                                                                                data-width="full" data-height="full"
                                                                                data-whitespace="nowrap"
                                                                                data-type="shape" data-basealign="slide"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":800,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 5;    background-color: rgba(0, 0, 0, 0.64);">
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-2-layer-1"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['178','151','218','186']"
                                                                                data-fontsize="['200','160','130','90']"
                                                                                data-lineheight="['180','150','130','90']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['center','center','center','center']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 6; white-space: nowrap; font-size: 200px; line-height: 180px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:hensaregular, hensa;">
                                                                                <h1
                                                                                    style="color: inherit; padding-bottom: 0px;  font: inherit;">
                                                                                    Make Your <br> Couplename</h1>
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-2-layer-2"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['581','494','523','394']"
                                                                                data-fontsize="['20','20','20','16']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">
                                                                                Make your own #coupletag with couple name generator</div>


                                                                            <div class="tp-caption rev-btn rev-hiddenicon "
                                                                                id="slide-2-layer-8"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['632','536','575','444']"
                                                                                data-fontsize="['14','14','14','12']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2200,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.30);bw:1 1 1 1;"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[12,12,12,12]"
                                                                                data-paddingright="[35,35,35,35]"
                                                                                data-paddingbottom="[12,12,12,12]"
                                                                                data-paddingleft="[35,35,35,35]"
                                                                                style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:uppercase;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:1px;">
                                                                                <a href="https://www.weddingo.in/couple-name-generator/"
                                                                                    target="_blank"
                                                                                    style="color: white;" onclick="couplename()">Try it now</a>
                                                                                <i class="fa-icon-chevron-right"></i>
                                                                            </div>


                                                                            <div class="tp-caption rev-scroll-btn "
                                                                                id="slide-2-layer-10"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['bottom','bottom','bottom','bottom']"
                                                                                data-voffset="['50','50','50','50']"
                                                                                data-width="35" data-height="55"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2600,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 300;font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:3px 3px 3px 3px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                                                                <span>
                                                                                </span>
                                                                            </div>
                                                                        </li>

                                                                        <!--
                                                                        <li data-index="rs-2" data-transition="slidingoverlayleft" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Personalise" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                            
                                                                            <img src="wp-content/uploads/sites/96/2017/11/slider2s.jpeg" alt="Online Wedding Invitation - Weddingo" title="slider2s" width="1950" height="1300" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                                            

                                                                            
                                                                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-2-layer-3" data-x="['center','center','center','center']" data-hoffset="['1','1','1','1']" data-y="['middle','middle','middle','middle']" data-voffset="['-2','-2','-2','-2']" data-width="full" data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="on" data-frames='[{"delay":800,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;    background-color: rgba(0, 0, 0, 0.64);"> </div>

                                                                            
                                                                            <div class="tp-caption   tp-resizeme" id="slide-2-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-10','-40','-47','-37']" data-fontsize="['240','220','160','90']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 240px; line-height: 150px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:hensaregular, hensa;">Couplename <br> Generator</div>

                                                                            
                                                                            <div class="tp-caption   tp-resizeme" id="slide-2-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['92','81','43','21']" data-fontsize="['20','20','20','16']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">Customize your wedding invitation card</div>

                                                                            
                                                                            <div class="tp-caption rev-btn rev-hiddenicon " id="slide-2-layer-8" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['566','506','564','410']" data-fontsize="['14','14','14','12']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":2200,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.30);bw:1 1 1 1;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:uppercase;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:1px;cursor:pointer;"><a href="contact/index.html" style="color: white">Purchase</a> <i class="fa-icon-chevron-right"></i> </div>

                                                                            
                                                                            <div class="tp-caption rev-scroll-btn " id="slide-2-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','50','50','50']" data-width="35" data-height="55" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":2600,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 300;font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:3px 3px 3px 3px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                                                                <span>
                                                                                </span>
                                                                            </div>

                                                                            
                                                                            <div class="tp-caption   tp-resizeme" id="slide-2-layer-12" data-x="['left','left','left','left']" data-hoffset="['246','165','143','100']" data-y="['top','top','top','top']" data-voffset="['311','250','356','270']" data-fontsize="['30','30','24','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 10; white-space: nowrap; font-size: 30px; line-height: 22px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Raleway;"> </div>
                                                                        </li>
-->

                                                                        <li data-index="rs-3"
                                                                            data-transition="slidingoverlayleft"
                                                                            data-slotamount="default"
                                                                            data-hideafterloop="0"
                                                                            data-hideslideonmobile="off"
                                                                            data-easein="default" data-easeout="default"
                                                                            data-masterspeed="default" data-rotate="0"
                                                                            data-saveperformance="off"
                                                                            data-title="Personalise" data-param1=""
                                                                            data-param2="" data-param3="" data-param4=""
                                                                            data-param5="" data-param6="" data-param7=""
                                                                            data-param8="" data-param9=""
                                                                            data-param10="" data-description="">

                                                                            <img src="wp-content/uploads/sites/96/2017/11/slider2s.jpeg"
                                                                                alt="Online Wedding Invitation - Weddingo"
                                                                                title="slider2s" width="1950"
                                                                                height="1300"
                                                                                data-bgposition="center center"
                                                                                data-bgfit="cover"
                                                                                data-bgrepeat="no-repeat"
                                                                                class="rev-slidebg" data-no-retina>



                                                                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                                                                id="slide-3-layer-3"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['1','1','1','1']"
                                                                                data-y="['middle','middle','middle','middle']"
                                                                                data-voffset="['-2','-2','-2','-2']"
                                                                                data-width="full" data-height="full"
                                                                                data-whitespace="nowrap"
                                                                                data-type="shape" data-basealign="slide"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":800,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 5;    background-color: rgba(0, 0, 0, 0.64);">
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-3-layer-1"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['middle','middle','middle','middle']"
                                                                                data-voffset="['-10','-40','-47','-37']"
                                                                                data-fontsize="['240','220','160','90']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['center','center','center','center']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 6; white-space: nowrap; font-size: 240px; line-height: 150px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:hensaregular, hensa;">
                                                                                Personalize</div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-3-layer-2"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['middle','middle','middle','middle']"
                                                                                data-voffset="['92','81','43','21']"
                                                                                data-fontsize="['20','20','20','16']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">
                                                                                Customize your wedding invitation card
                                                                            </div>


                                                                            <div class="tp-caption rev-btn rev-hiddenicon "
                                                                                id="slide-3-layer-8"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['566','506','564','410']"
                                                                                data-fontsize="['14','14','14','12']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2200,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.30);bw:1 1 1 1;"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[12,12,12,12]"
                                                                                data-paddingright="[35,35,35,35]"
                                                                                data-paddingbottom="[12,12,12,12]"
                                                                                data-paddingleft="[35,35,35,35]"
                                                                                style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:uppercase;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:1px;cursor:pointer;">
                                                                                <a href="contact/"
                                                                                    style="color: white" onclick="purchase('')">Purchase</a> <i
                                                                                    class="fa-icon-chevron-right"></i>
                                                                            </div>


                                                                            <div class="tp-caption rev-scroll-btn "
                                                                                id="slide-3-layer-10"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['bottom','bottom','bottom','bottom']"
                                                                                data-voffset="['50','50','50','50']"
                                                                                data-width="35" data-height="55"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2600,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 300;font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:3px 3px 3px 3px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                                                                <span>
                                                                                </span>
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-3-layer-12"
                                                                                data-x="['left','left','left','left']"
                                                                                data-hoffset="['246','165','143','100']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['311','250','356','270']"
                                                                                data-fontsize="['30','30','24','18']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 10; white-space: nowrap; font-size: 30px; line-height: 22px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">
                                                                            </div>
                                                                        </li>

                                                                        <li data-index="rs-4"
                                                                            data-transition="slidingoverlaydown"
                                                                            data-slotamount="default"
                                                                            data-hideafterloop="0"
                                                                            data-hideslideonmobile="off"
                                                                            data-easein="default" data-easeout="default"
                                                                            data-masterspeed="default" data-rotate="0"
                                                                            data-saveperformance="off"
                                                                            data-title="Online Invitation"
                                                                            data-param1="" data-param2="" data-param3=""
                                                                            data-param4="" data-param5="" data-param6=""
                                                                            data-param7="" data-param8="" data-param9=""
                                                                            data-param10="" data-description="">

                                                                            <img src="wp-content/uploads/sites/96/2017/11/slider4.jpg"
                                                                                alt="Online Wedding Invitation - Weddingo"
                                                                                title="slider1" width="1920"
                                                                                height="1280"
                                                                                data-bgposition="center center"
                                                                                data-bgfit="cover"
                                                                                data-bgrepeat="no-repeat"
                                                                                class="rev-slidebg" data-no-retina>



                                                                            <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                                                                                id="slide-1-layer-3"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['middle','middle','middle','middle']"
                                                                                data-voffset="['0','0','0','0']"
                                                                                data-width="full" data-height="full"
                                                                                data-whitespace="nowrap"
                                                                                data-type="shape" data-basealign="slide"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":800,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 5;    background-color: rgba(0, 0, 0, 0.64);">
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-1-layer-1"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['178','151','218','186']"
                                                                                data-fontsize="['200','160','130','90']"
                                                                                data-lineheight="['180','150','130','90']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1800,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['center','center','center','center']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 6; white-space: nowrap; font-size: 100px; line-height: 100px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:hensaregular, hensa !important;">
                                                                                
                                                                                <style> 
                                                                                .head-4{

                                                                                    font-size: 130px;
                                                                                    line-height: 150px;
                                                                              }

                                                                              @media screen and (max-width: 700px) {
                                                                                  .head-4{
                                                                                    line-height: 55px  !important;
                                                                                    font-size: 40px !important;
                                                                                   



                                                                                  }

                                                                            
                                                                              }
                                                                                
                                                                                </style>
                                                                                
                                                                                <h1
                                                                                    style="color: inherit;font-family:hensaregular, hensa !important" class="head-4">
                                                                                  Make Your <br> Own Website</h1>
                                                                            </div>


                                                                            <div class="tp-caption   tp-resizeme"
                                                                                id="slide-1-layer-2"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['581','494','523','394']"
                                                                                data-fontsize="['20','20','20','16']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="text"
                                                                                data-responsive_offset="on"
                                                                                data-frames='[{"delay":1900,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">
                                                                                Create wedding website with Weddingo</div>


                                                                            <div class="tp-caption rev-btn rev-hiddenicon "
                                                                                id="slide-1-layer-8"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['top','top','top','top']"
                                                                                data-voffset="['632','536','575','444']"
                                                                                data-fontsize="['14','14','14','12']"
                                                                                data-width="none" data-height="none"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2200,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.30);bw:1 1 1 1;"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[12,12,12,12]"
                                                                                data-paddingright="[35,35,35,35]"
                                                                                data-paddingbottom="[12,12,12,12]"
                                                                                data-paddingleft="[35,35,35,35]"
                                                                                style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;text-transform:uppercase;background-color:rgba(10, 10, 10, 0);border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:30px 30px 30px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;letter-spacing:1px;">
                                                                                <a href="contact/" style="color: white;"
                                                                                    target="_new" onclick="purchase('')">Purchase</a> <i
                                                                                    class="fa-icon-chevron-right"></i>
                                                                            </div>


                                                                            <div class="tp-caption rev-scroll-btn "
                                                                                id="slide-1-layer-10"
                                                                                data-x="['center','center','center','center']"
                                                                                data-hoffset="['0','0','0','0']"
                                                                                data-y="['bottom','bottom','bottom','bottom']"
                                                                                data-voffset="['50','50','50','50']"
                                                                                data-width="35" data-height="55"
                                                                                data-whitespace="nowrap"
                                                                                data-type="button"
                                                                                data-responsive_offset="on"
                                                                                data-responsive="off"
                                                                                data-frames='[{"delay":2600,"speed":2000,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                                                                                data-textAlign="['inherit','inherit','inherit','inherit']"
                                                                                data-paddingtop="[0,0,0,0]"
                                                                                data-paddingright="[0,0,0,0]"
                                                                                data-paddingbottom="[0,0,0,0]"
                                                                                data-paddingleft="[0,0,0,0]"
                                                                                style="z-index: 9; min-width: 35px; max-width: 35px; max-width: 55px; max-width: 55px; white-space: nowrap; font-size: px; line-height: px; font-weight: 300;font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:3px 3px 3px 3px;border-radius:23px 23px 23px 23px;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                                                                <span>
                                                                                </span>
                                                                            </div>
                                                                        </li>

                                                                        



                                                                    </ul>
                                                                    <script>
                                                                        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                                                        var htmlDivCss = "";
                                                                        if (htmlDiv) {
                                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                                        } else {
                                                                            var htmlDiv = document.createElement("div");
                                                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                                        }

                                                                    </script>
                                                                    <div class="tp-bannertimer tp-bottom"
                                                                        style="visibility: hidden !important;"></div>
                                                                </div>
                                                                <script>
                                                                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                                                    var htmlDivCss = "";
                                                                    if (htmlDiv) {
                                                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                                    } else {
                                                                        var htmlDiv = document.createElement("div");
                                                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                                    }

                                                                </script>
                                                                <script type="text/javascript">
                                                                    /******************************************
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                				-	PREPARE PLACEHOLDER FOR SLIDER	-
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                			******************************************/

                                                                    var setREVStartSize = function () {
                                                                        try {
                                                                            var e = new Object,
                                                                                i = jQuery(window).width(),
                                                                                t = 9999,
                                                                                r = 0,
                                                                                n = 0,
                                                                                l = 0,
                                                                                f = 0,
                                                                                s = 0,
                                                                                h = 0;
                                                                            e.c = jQuery('#rev_slider_1_1');
                                                                            e.responsiveLevels = [1240, 1024, 778, 480];
                                                                            e.gridwidth = [1240, 1024, 778, 480];
                                                                            e.gridheight = [868, 768, 960, 720];

                                                                            e.sliderLayout = "fullscreen";
                                                                            e.fullScreenAutoWidth = 'off';
                                                                            e.fullScreenAlignForce = 'off';
                                                                            e.fullScreenOffsetContainer = '';
                                                                            e.fullScreenOffset = '';
                                                                            if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                                                                                f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                                                                            }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                                                                                var u = (e.c.width(), jQuery(window).height());
                                                                                if (void 0 != e.fullScreenOffsetContainer) {
                                                                                    var c = e.fullScreenOffsetContainer.split(",");
                                                                                    if (c) jQuery.each(c, function (e, i) {
                                                                                        u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                                                                    }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                                                                                }
                                                                                f = u
                                                                            } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                                                            e.c.closest(".rev_slider_wrapper").css({
                                                                                height: f
                                                                            })

                                                                        } catch (d) {
                                                                            console.log("Failure at Presize of Slider:" + d)
                                                                        }
                                                                    };

                                                                    setREVStartSize();

                                                                    var tpj = jQuery;

                                                                    var revapi1;
                                                                    tpj(document).ready(function () {
                                                                        if (tpj("#rev_slider_1_1").revolution == undefined) {
                                                                            revslider_showDoubleJqueryError("#rev_slider_1_1");
                                                                        } else {
                                                                            revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                                                                sliderType: "standard",
                                                                                jsFileLocation: "wp-content/plugins/revslider/public/assets/js/",
                                                                                sliderLayout: "fullscreen",
                                                                                dottedOverlay: "none",
                                                                                delay: 9000,
                                                                                navigation: {
                                                                                    keyboardNavigation: "off",
                                                                                    keyboard_direction: "horizontal",
                                                                                    mouseScrollNavigation: "off",
                                                                                    mouseScrollReverse: "default",
                                                                                    onHoverStop: "off",
                                                                                    arrows: {
                                                                                        style: "hermes",
                                                                                        enable: true,
                                                                                        hide_onmobile: true,
                                                                                        hide_under: 768,
                                                                                        hide_onleave: false,
                                                                                        //                                                                                        tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div>	<div class="tp-arr-titleholder">{{title}}</div>	</div>',
                                                                                        left: {
                                                                                            h_align: "left",
                                                                                            v_align: "center",
                                                                                            h_offset: 0,
                                                                                            v_offset: 0
                                                                                        },
                                                                                        right: {
                                                                                            h_align: "right",
                                                                                            v_align: "center",
                                                                                            h_offset: 0,
                                                                                            v_offset: 0
                                                                                        }
                                                                                    }
                                                                                },
                                                                                responsiveLevels: [1240, 1024, 778, 480],
                                                                                visibilityLevels: [1240, 1024, 778, 480],
                                                                                gridwidth: [1240, 1024, 778, 480],
                                                                                gridheight: [868, 768, 960, 720],
                                                                                lazyType: "none",
                                                                                shadow: 0,
                                                                                spinner: "spinner4",
                                                                                stopLoop: "off",
                                                                                stopAfterLoops: -1,
                                                                                stopAtSlide: -1,
                                                                                shuffle: "off",
                                                                                autoHeight: "off",
                                                                                fullScreenAutoWidth: "off",
                                                                                fullScreenAlignForce: "off",
                                                                                fullScreenOffsetContainer: "",
                                                                                fullScreenOffset: "",
                                                                                disableProgressBar: "on",
                                                                                hideThumbsOnMobile: "off",
                                                                                hideSliderAtLimit: 0,
                                                                                hideCaptionAtLimit: 0,
                                                                                hideAllCaptionAtLilmit: 0,
                                                                                debugMode: false,
                                                                                fallbacks: {
                                                                                    simplifyAll: "off",
                                                                                    nextSlideOnWindowFocus: "off",
                                                                                    disableFocusListener: false,
                                                                                }
                                                                            });
                                                                        }
                                                                    }); /*ready*/

                                                                </script>
                                                                <script>
                                                                    var htmlDivCss = '	#rev_slider_1_1_wrapper .tp-loader.spinner4 div { background-color: #FFFFFF !important; } ';
                                                                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                                    if (htmlDiv) {
                                                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                                    } else {
                                                                        var htmlDiv = document.createElement('div');
                                                                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                                    }

                                                                </script>
                                                                <script>
                                                                    var htmlDivCss = unescape(".hermes.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A30px%3B%0A%09height%3A110px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A%0A.hermes.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A15px%3B%0A%09color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20110px%3B%0A%09text-align%3A%20center%3B%0A%20%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A.hermes.tparrows.tp-leftarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A.hermes.tparrows.tp-rightarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%0A.hermes%20.tp-arr-allwrapper%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%09width%3A180px%3B%0A%20%20%20%20height%3A140px%3B%0A%20%20%20%20top%3A0px%3B%0A%20%20%20%20left%3A0px%3B%0A%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20-webkit-transform%200.3s%200.3s%3B%0A%20%20transition%3A%20transform%200.3s%200.3s%3B%0A%20%20-webkit-perspective%3A%201000px%3B%0A%20%20perspective%3A%201000px%3B%0A%20%20%20%20%7D%0A.hermes.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20right%3A0px%3Bleft%3Aauto%3B%0A%20%20%20%20%20%20%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20visibility%3Avisible%3B%0A%20%20%20%20%20%20%20%20%20%20%7D%0A.hermes%20.tp-arr-imgholder%20%7B%0A%20%20width%3A180px%3Bposition%3Aabsolute%3B%0A%20%20left%3A0px%3Btop%3A0px%3Bheight%3A110px%3B%0A%20%20transform%3Atranslatex%28-180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-180px%29%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20transition-delay%3A0.3s%3B%0A%7D%0A.hermes.tp-rightarrow%20.tp-arr-imgholder%7B%0A%20%20%20%20transform%3Atranslatex%28180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28180px%29%3B%0A%20%20%20%20%20%20%7D%0A%20%20%0A.hermes.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%20%20%20%20%20%20%20%20%20%20%20%20%0A%7D%0A.hermes%20.tp-arr-titleholder%20%7B%0A%20%20top%3A110px%3B%0A%20%20width%3A180px%3B%0A%20%20text-align%3Aleft%3B%20%0A%20%20display%3Ablock%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20line-height%3A30px%3B%20background%3A%23000%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.75%29%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20font-weight%3A600%3B%20position%3Aabsolute%3B%0A%20%20font-size%3A12px%3B%0A%20%20white-space%3Anowrap%3B%0A%20%20letter-spacing%3A1px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transform%3A%20rotatex%28-90deg%29%3B%0A%20%20transform%3A%20rotatex%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%2050%25%200%3B%0A%20%20transform-origin%3A%2050%25%200%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%0A%7D%0A.hermes.tparrows%3Ahover%20.tp-arr-titleholder%20%7B%0A%20%20%20%20-webkit-transition-delay%3A%200.6s%3B%0A%20%20transition-delay%3A%200.6s%3B%0A%20%20-webkit-transform%3A%20rotatex%280deg%29%3B%0A%20%20transform%3A%20rotatex%280deg%29%3B%0A%7D%0A%0A");
                                                                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                                    if (htmlDiv) {
                                                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                                    } else {
                                                                        var htmlDiv = document.createElement('div');
                                                                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                                    }

                                                                </script>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                         
                            <section class="elementor-container " style="
    text-align: center;
    /* max-width: 1400px; */
    margin: auto;

">

<style> 

.how-div{
    display: flex;
    /* width: 48%; */
    /* align-items: center; */
    /* padding: 0% 25%; */
    justify-content: center;
    margin: 2% 0%;
}

.info-how{
    text-align: left;
    width: 29%;
    padding-left: 2%;
}

.how-num{
    font-style: normal;
    font-weight: 800;
    font-size: 50px;
    font-family: 'Mukta', sans-serif;
    margin:0% 1% 0% 0%;
   
    color: rgba(33, 182, 82, 0.5);
   

span{
    counter-reset: how-number;
}

}

span :: before {
    counter-increment: how-number;
    content: counter(how-number);
}

.how-head{
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    /* line-height: 5px; */
    color: #000000;
    margin: 2% 0%;
    /* margin: -10px 0px; */
    
}


.how-text{
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    line-height: 16px;
    color: #5F5F5F;
    /* width: 70%; */
    margin: 0px;
}

.reverse-how{
    flex-direction: row-reverse;
}




@media only screen and (max-width:768px) {
    .info-how{
        text-align:center;
        width:100%;
        padding:2%;
        
    }

.how-div{
    flex-direction: column;
    align-items: center;
    margin:55px 0;
}


}
</style> 

<div style="background-color: #FFFCF3; padding:40px 0px"> 
<h2 style="font-weight: 600;
font-size: 40px;
line-height: 49px;"> HOW IT WORKS </h2>

  <div class="how-div">
            <svg width="123" height="123" viewBox="0 0 123 123" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="61.5" cy="61.5" r="61" fill="white" stroke="#FFDF7D"/>
            <path d="M92.9873 53.4103C92.9808 53.3328 92.9664 53.2562 92.9443 53.1819C92.9299 53.1156 92.9098 53.0509 92.8845 52.9884C92.855 52.9273 92.8199 52.8696 92.7795 52.8161C92.739 52.7527 92.6921 52.6943 92.6398 52.6417C92.623 52.626 92.6157 52.6045 92.5979 52.5888L82.4995 44.1846V35.4744C82.4995 33.6101 81.0891 32.0987 79.3493 32.0987H67.9699L63.4136 28.3078C62.288 27.3671 60.7096 27.3671 59.5841 28.3078L55.0268 32.0987H43.6473C41.9076 32.0987 40.4972 33.61 40.4972 35.4744V44.1848L30.402 52.589C30.3842 52.6048 30.3768 52.6261 30.3601 52.6418C30.3078 52.6943 30.2609 52.7529 30.2204 52.8163C30.18 52.8697 30.1448 52.9273 30.1154 52.9885C30.09 53.0509 30.07 53.1157 30.0556 53.1821C30.0337 53.2552 30.0193 53.3307 30.0126 53.4072C30.0126 53.4308 30 53.4511 30 53.4747V91.7352C30.0015 92.451 30.2176 93.1472 30.6164 93.7212C30.6226 93.7314 30.6238 93.7438 30.631 93.7527C30.6384 93.7617 30.6542 93.7718 30.6646 93.7842C31.2545 94.616 32.172 95.1064 33.147 95.1109H89.8498C90.8286 95.1077 91.7499 94.615 92.3406 93.7787C92.3489 93.7675 92.3616 93.7641 92.3689 93.7529C92.3762 93.7416 92.3772 93.7315 92.3836 93.7213C92.7824 93.1472 92.9985 92.451 93 91.7354V53.4778C92.9999 53.4542 92.9884 53.4339 92.9873 53.4103ZM60.8682 30.0833C61.2349 29.7711 61.7542 29.7711 62.1209 30.0833L64.5434 32.0986H58.4531L60.8682 30.0833ZM33.4115 92.8605L60.8684 70.0062C61.2353 69.6944 61.7542 69.6944 62.121 70.0062L89.5852 92.8605H33.4115ZM90.8999 91.1063L63.4136 68.2305C62.2878 67.2906 60.7099 67.2906 59.5841 68.2305L32.0968 91.1063V55.1915L49.3041 69.51C49.7626 69.8909 50.4224 69.8015 50.7779 69.3102C51.1334 68.8189 51.05 68.1118 50.5915 67.7309L33.1102 53.1852L40.4973 47.0336V55.7282C40.4973 56.3496 40.9675 56.8534 41.5473 56.8534C42.1273 56.8534 42.5973 56.3495 42.5973 55.7282V35.4742C42.5973 34.8528 43.0674 34.3491 43.6473 34.3491H79.3491C79.9291 34.3491 80.3991 34.8528 80.3991 35.4742V55.7282C80.3991 56.3496 80.8692 56.8534 81.4491 56.8534C82.0291 56.8534 82.4991 56.3495 82.4991 55.7282V47.0336L89.8863 53.1852L72.3746 67.7569C72.0733 68.0015 71.9179 68.4007 71.9679 68.8023C72.0178 69.2038 72.2653 69.5456 72.6159 69.6971C72.9665 69.8486 73.366 69.7866 73.662 69.5346L90.8996 55.1915V91.1063H90.8999Z" fill="black"/>
            <path d="M74.0997 54.6037V50.1029C74.0997 42.6456 68.4582 36.6002 61.4991 36.6002C54.5399 36.6002 48.8984 42.6456 48.8984 50.1029C48.8984 57.5602 54.5399 63.6055 61.4991 63.6055C62.079 63.6055 62.5491 63.1018 62.5491 62.4804C62.5491 61.8589 62.079 61.3552 61.4991 61.3552C55.6998 61.3552 50.9986 56.3174 50.9986 50.103C50.9986 43.8886 55.6998 38.8508 61.4991 38.8508C67.2983 38.8508 71.9996 43.8886 71.9996 50.103V54.6038C71.9996 55.8468 71.0593 56.8543 69.8994 56.8543C68.7395 56.8543 67.7993 55.8468 67.7993 54.6038V50.103C67.7993 49.4815 67.3292 48.9778 66.7493 48.9778C66.1693 48.9778 65.6992 49.4815 65.6992 50.103C65.6992 52.5887 63.8187 54.6038 61.4991 54.6038C59.1794 54.6038 57.2989 52.5887 57.2989 50.103C57.2989 47.6173 59.1794 45.6022 61.4991 45.6022C62.079 45.6022 62.5491 45.0985 62.5491 44.477C62.5491 43.8555 62.079 43.3518 61.4991 43.3518C58.6793 43.3451 56.1987 45.3469 55.4259 48.2529C54.6531 51.1588 55.7762 54.2611 58.1758 55.8482C60.5753 57.4353 63.6667 57.1207 65.7444 55.0778C65.973 57.4573 67.9031 59.2267 70.1324 59.1002C72.3617 58.9736 74.1067 56.996 74.0997 54.6037Z" fill="black"/>
            </svg>

                <div class="info-how"> 

                <p class="how-head"> <span class="how-num">1</span> <span>YOU SEND US ALL YOUR WEDDING INFORMATION </span></p>

                <p class="how-text">  We will require all the details about your wedding, from your Venue to dates. Information about the Bride and the Groom, and other details you would want to put on the website.</p>
                </div>
      



    </div>

   

   <div class="how-div reverse-how">
   <svg width="123" height="123" viewBox="0 0 123 123" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="61.5" cy="61.5" r="61" fill="white" stroke="#FFEBAB"/>
<path d="M99.5071 47.0186L62.6844 29.4279C62.2565 29.221 61.7392 29.3999 61.5375 29.826L53.5234 46.5995H57.89L63.7716 34.275L95.6964 49.5245L85.713 70.419L80.0927 67.7359V78.7821L83.5491 80.4322C83.9805 80.6409 84.4908 80.4533 84.6959 80.0254L99.9139 48.1602C100.121 47.7358 99.9385 47.222 99.5071 47.0186Z" fill="#030104"/>
<path d="M76.6732 48.6249H35.8628C35.3911 48.6249 35 49.0089 35 49.4806V84.797C35 85.2704 35.3911 85.658 35.8628 85.658H76.6715C77.1467 85.658 77.5343 85.2704 77.5343 84.797V49.4806C77.536 49.0089 77.1485 48.6249 76.6732 48.6249ZM74.3164 75.6904H71.7298C70.2234 71.8271 68.3698 66.4189 65.5552 67.1414C62.2566 67.9832 60.61 75.6904 60.61 75.6904C60.61 75.6904 58.9212 66.9941 54.2671 62.3382C49.6112 57.6824 45.1009 75.6904 45.1009 75.6904H38.9386V52.5302H74.3164V75.6904Z" fill="#030104"/>
<path d="M44.6501 60.3865C46.2443 60.3865 47.5366 59.0942 47.5366 57.5C47.5366 55.9058 46.2443 54.6135 44.6501 54.6135C43.056 54.6135 41.7637 55.9058 41.7637 57.5C41.7637 59.0942 43.056 60.3865 44.6501 60.3865Z" fill="#030104"/>
<path d="M64.0148 59.5377C64.6619 59.5377 65.2687 59.4693 65.8088 59.3623C66.4383 59.6289 67.2064 59.792 68.0324 59.792C70.163 59.792 71.8851 58.7521 71.8851 57.4684C71.8851 56.1883 70.163 55.1484 68.0324 55.1484C67.2345 55.1484 66.4944 55.2939 65.8807 55.5482C65.6264 55.3009 65.2879 55.1484 64.9109 55.1484H64.3533C63.6378 55.1484 63.0749 55.6902 62.9942 56.3794C61.2721 56.56 59.9902 57.1843 59.9902 57.9279C59.9902 58.817 61.7912 59.5377 64.0148 59.5377Z" fill="#030104"/>
</svg>




                <div class="info-how"> 

                <p class="how-head"> <span class="how-num">2</span> SHARE YOUR BEST PHOTOS TO MAKE THE WEBSITE COME ALIVE </p>

                <p class="how-text"> Next, you will have to send us the best pictures you have of the bride, groom, family members and friends. We put them up on your website making it totally customized.</p>
                </div>
      



    </div>

    <div class="how-div">
    <svg width="123" height="123" viewBox="0 0 123 123" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="61.5" cy="61.5" r="61" fill="white" stroke="#FFEFBE"/>
<path d="M86.1327 33.3975H35.8677C33.7351 33.3975 32 35.1339 32 37.2677V69.1885C32 69.7224 32.4331 70.1558 32.9666 70.1558H89.0336C89.5671 70.1558 90.0002 69.7224 90.0002 69.1885V37.2677C90.0003 35.1339 88.2652 33.3975 86.1327 33.3975ZM88.0669 68.2212H33.9334V37.2677C33.9334 36.1998 34.8015 35.3322 35.8677 35.3322H86.1327C87.1989 35.3322 88.0669 36.1998 88.0669 37.2677V68.2212Z" fill="black"/>
<path d="M89.0337 68.2212H32.9666C32.4331 68.2212 32 68.6546 32 69.1885V74.0241C32 76.1581 33.7351 77.8944 35.8677 77.8944H86.1327C88.2652 77.8944 90.0003 76.1581 90.0003 74.0241V69.1885C90.0003 68.6546 89.5673 68.2212 89.0337 68.2212ZM88.067 74.0242C88.067 75.0922 87.1989 75.9598 86.1327 75.9598H35.8677C34.8015 75.9598 33.9334 75.0922 33.9334 74.0242V70.1559H88.0671V74.0242H88.067Z" fill="black"/>
<path d="M63.9014 72.0905H58.1014C57.5678 72.0905 57.1348 72.5238 57.1348 73.0578C57.1348 73.5917 57.5678 74.0251 58.1014 74.0251H63.9014C64.435 74.0251 64.8681 73.5917 64.8681 73.0578C64.8681 72.5238 64.435 72.0905 63.9014 72.0905Z" fill="black"/>
<path d="M85.1654 37.2667H36.8319C36.2983 37.2667 35.8652 37.7001 35.8652 38.234V69.1885C35.8652 69.7224 36.2983 70.1558 36.8319 70.1558H85.1655C85.6991 70.1558 86.1321 69.7224 86.1321 69.1885V38.234C86.1321 37.7001 85.6991 37.2667 85.1654 37.2667ZM84.1988 68.2211H37.7985V39.2013H84.1988V68.2211Z" fill="black"/>
<path d="M73.5666 83.6984H48.4332C46.8344 83.6984 45.5332 85.0004 45.5332 86.6003C45.5332 88.2003 46.8344 89.5023 48.4332 89.5023H73.5667C75.1656 89.5023 76.4668 88.2003 76.4668 86.6003C76.4668 85.0004 75.1655 83.6984 73.5666 83.6984ZM73.5666 87.5676H48.4332C47.8997 87.5676 47.4666 87.1334 47.4666 86.6003C47.4666 86.0673 47.8997 85.6331 48.4332 85.6331H73.5667C74.1003 85.6331 74.5334 86.0673 74.5334 86.6003C74.5334 87.1334 74.1003 87.5676 73.5666 87.5676Z" fill="black"/>
<path d="M71.6357 83.6985C68.4283 83.6985 66.8023 81.4204 66.8023 76.9271C66.8023 76.3932 66.3692 75.9598 65.8357 75.9598H56.169C55.6355 75.9598 55.2024 76.3932 55.2024 76.9271C55.2024 81.4204 53.5764 83.6985 50.369 83.6985C49.8354 83.6985 49.4023 84.1318 49.4023 84.6658C49.4023 85.1997 49.8354 85.6331 50.369 85.6331H71.6357C72.1693 85.6331 72.6024 85.1997 72.6024 84.6658C72.6024 84.1318 72.1693 83.6985 71.6357 83.6985ZM55.0775 83.6985C56.2676 82.3791 56.9683 80.4143 57.1095 77.8944H64.896C65.0361 80.4143 65.737 82.3791 66.928 83.6985H55.0775Z" fill="black"/>
<path d="M78.1914 61.8986L75.8579 59.5636L77.1561 58.2645C77.3968 58.0236 77.4934 57.6735 77.4103 57.3436C77.3271 57.0137 77.0768 56.7525 76.751 56.6539L67.0843 53.752C66.7431 53.6494 66.3748 53.7432 66.1235 53.9948C65.8712 54.2463 65.7784 54.6157 65.8808 54.9563L68.7808 64.6294C68.8785 64.9564 69.1404 65.2069 69.47 65.2901C69.7987 65.3714 70.1496 65.2765 70.3903 65.0357L71.7572 63.6669L74.0907 66.003C74.2715 66.1848 74.518 66.2864 74.7741 66.2864C75.0302 66.2864 75.2767 66.1848 75.4575 66.003L78.1913 63.2664C78.5693 62.8883 78.5693 62.2769 78.1914 61.8986ZM74.7741 63.9513L72.6494 61.8242C72.4029 61.5775 72.08 61.4546 71.7572 61.4546C71.4333 61.4546 71.1105 61.5784 70.865 61.8231L70.1671 62.5215L68.2482 56.1208L74.6446 58.041L73.8964 58.7897C73.6905 58.9967 73.5774 59.2715 73.5774 59.5636C73.5774 59.8567 73.6915 60.1333 73.8983 60.3384L76.141 62.5825L74.7741 63.9513Z" fill="black"/>
<path d="M72.5998 41.136H45.533C44.9995 41.136 44.5664 41.5694 44.5664 42.1033V46.9399C44.5664 47.4739 44.9995 47.9072 45.533 47.9072H72.5998C73.1334 47.9072 73.5665 47.4739 73.5665 46.9399V42.1033C73.5665 41.5694 73.1334 41.136 72.5998 41.136ZM71.6332 45.9727H46.4997V43.0707H71.6332V45.9727Z" fill="black"/>
<path d="M57.1336 51.7767H49.4002C48.8667 51.7767 48.4336 52.2101 48.4336 52.744V56.6132C48.4336 57.1471 48.8667 57.5804 49.4002 57.5804H57.1336C57.6672 57.5804 58.1003 57.1471 58.1003 56.6132V52.744C58.1003 52.2101 57.6672 51.7767 57.1336 51.7767ZM56.1669 55.6459H50.3669V53.7113H56.1669V55.6459Z" fill="black"/>
<path d="M72.5998 41.136H45.533C44.9995 41.136 44.5664 41.5694 44.5664 42.1033V60.4825C44.5664 61.0165 44.9995 61.4498 45.533 61.4498H68.5457C68.8521 61.4498 69.1392 61.3057 69.321 61.06C69.5037 60.8143 69.5598 60.497 69.4718 60.2039L68.248 56.1208L72.3225 57.3445C72.6154 57.4326 72.9325 57.3765 73.177 57.1936C73.4225 57.0118 73.5666 56.7235 73.5666 56.4178V42.1033C73.5664 41.5694 73.1334 41.136 72.5998 41.136ZM71.6332 55.1178L67.083 53.752C66.7418 53.6494 66.3735 53.7432 66.1221 53.9948C65.8699 54.2472 65.7771 54.6157 65.8795 54.9573L67.2463 59.5154H46.4997V43.0707H71.6332V55.1178Z" fill="black"/>
</svg>

                <div class="info-how"> 

                <p class="how-head"> <span class="how-num">3</span> WE MAKE YOUR WEDDING WEBSITE </p>

                <p class="how-text"> With everything that you have shared with us, our engineers make the perfect and beautiful looking website for you. The website is hosted on a custom domain that is named after you. It can also be found in a Google search.</p>
                </div>
      



    </div>

    
    <div class="how-div reverse-how">
    <svg width="123" height="123" viewBox="0 0 123 123" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle cx="61.5" cy="61.5" r="61" fill="white" stroke="#FFEFBE"/>
<path d="M40.6348 84.5089C40.4242 84.2979 40.132 84.1768 39.834 84.1768C39.5361 84.1768 39.2438 84.2979 39.0331 84.5089C38.8224 84.7199 38.7012 85.0122 38.7012 85.3104C38.7012 85.6085 38.8223 85.901 39.0331 86.1117C39.2438 86.3226 39.5361 86.444 39.834 86.444C40.132 86.444 40.4242 86.3227 40.6348 86.1117C40.8455 85.901 40.9669 85.6085 40.9669 85.3104C40.9669 85.0122 40.8456 84.7198 40.6348 84.5089Z" fill="black"/>
<path d="M84.1857 53.676C83.975 53.4651 83.6839 53.3439 83.3848 53.3439C83.0869 53.3439 82.7946 53.4651 82.5839 53.676C82.3733 53.8869 82.252 54.1793 82.252 54.4775C82.252 54.7756 82.3732 55.0681 82.5839 55.2788C82.7946 55.4897 83.0869 55.6111 83.3848 55.6111C83.6839 55.6111 83.975 55.4898 84.1857 55.2788C84.3975 55.0681 84.5187 54.7756 84.5187 54.4775C84.5187 54.1793 84.3975 53.8869 84.1857 53.676Z" fill="black"/>
<path d="M88.6656 33.2703C88.4531 33.0577 88.1649 32.9382 87.8644 32.9382L59.9975 32.9397C59.3719 32.9397 58.8647 33.4473 58.8647 34.0733V37.315L48.1268 40.1943C47.5224 40.3564 47.1637 40.9779 47.3257 41.5826L52.891 62.3661L48.0766 67.1838C47.4638 66.2885 46.435 65.6997 45.2712 65.6997H34.3964C32.5225 65.6997 30.998 67.2253 30.998 69.1004V87.5775C30.998 89.4527 32.5225 90.9783 34.3964 90.9783H45.2714C46.8633 90.9783 48.2026 89.8769 48.5696 88.396L49.9165 89.5714C50.9563 90.4787 52.2886 90.9784 53.6682 90.9784H87.8646C88.4902 90.9784 88.9974 90.4708 88.9974 89.8448V34.0718C88.9972 33.7712 88.878 33.4828 88.6656 33.2703ZM46.404 87.5776C46.404 88.2026 45.8958 88.7112 45.2711 88.7112H34.3963C33.7717 88.7112 33.2635 88.2027 33.2635 87.5776V69.1005C33.2635 68.4754 33.7717 67.9669 34.3963 67.9669H38.7009V81.0253C38.7009 81.6513 39.2082 82.1589 39.8338 82.1589C40.4593 82.1589 40.9666 81.6513 40.9666 81.0253V67.9669H45.2712C45.8959 67.9669 46.4041 68.4754 46.4041 69.1005V87.5776H46.404ZM49.8073 42.0908L58.8647 39.6621V43.8627L58.1055 44.0663C57.8152 44.1442 57.5679 44.334 57.4177 44.5944C57.2675 44.8548 57.2267 45.1642 57.3045 45.4546C57.7398 47.0803 56.7723 48.7572 55.1477 49.1928C54.8571 49.2708 54.6094 49.4611 54.4593 49.722C54.3092 49.9828 54.2687 50.2928 54.3472 50.5834L58.7629 66.9455H56.4893L55.3176 62.3585C55.2804 62.2125 55.2151 62.078 55.1278 61.961L49.8073 42.0908ZM58.8647 49.0612V58.6266L56.7986 50.9709C57.6617 50.5249 58.368 49.8577 58.8647 49.0612ZM75.1394 88.7111H53.668C52.8359 88.7111 52.0324 88.4098 51.4053 87.8626L48.6696 85.4751V69.7967L53.6153 64.8477L54.5123 68.3596C54.6405 68.8614 55.0922 69.2124 55.6098 69.2124L77.8817 69.2137C78.7562 69.2137 79.4676 69.9256 79.4676 70.8008C79.4676 71.6759 78.7562 72.3878 77.8817 72.3878H66.4695C65.844 72.3878 65.3367 72.8954 65.3367 73.5214C65.3367 74.1473 65.844 74.655 66.4695 74.655H81.4274C82.302 74.655 83.0134 75.3669 83.0134 76.242C83.0134 77.1171 82.302 77.829 81.4274 77.829H66.4695C65.844 77.829 65.3367 78.3367 65.3367 78.9626C65.3367 79.5886 65.844 80.0962 66.4695 80.0962H79.0035C79.878 80.0962 80.5895 80.8081 80.5895 81.6833C80.5895 82.5584 79.878 83.2703 79.0035 83.2703H66.4695C65.844 83.2703 65.3367 83.7779 65.3367 84.4039C65.3367 85.0299 65.844 85.5375 66.4695 85.5375H75.1394C76.014 85.5375 76.7254 86.2494 76.7254 87.1245C76.7253 87.9992 76.0139 88.7111 75.1394 88.7111ZM86.7318 88.7111H86.7317H78.6479C78.8676 88.2267 78.9909 87.6897 78.9909 87.1241C78.9909 86.5584 78.8676 86.0214 78.6479 85.537H79.0035C81.1272 85.537 82.855 83.8081 82.855 81.6829C82.855 81.0631 82.7072 80.4773 82.4462 79.9579C84.077 79.5097 85.2788 78.0139 85.2788 76.2418C85.2788 75.3821 84.9958 74.5873 84.5186 73.9457V59.5785C84.5186 58.9525 84.0113 58.4449 83.3857 58.4449C82.7602 58.4449 82.2529 58.9525 82.2529 59.5785V72.4779C81.9867 72.4195 81.7106 72.3878 81.4272 72.3878H81.39C81.6097 71.9034 81.7331 71.3664 81.7331 70.8008C81.7331 68.6755 80.0053 66.9466 77.8815 66.9466H76.0605C77.9942 66.1169 79.3522 64.1936 79.3522 61.9583C79.3522 58.9668 76.9202 56.5331 73.9308 56.5331C70.9415 56.5331 68.5094 58.9668 68.5094 61.9583C68.5094 64.1936 69.8676 66.1169 71.8011 66.9466H65.6087V45.3922C67.6324 44.9509 69.2292 43.3531 69.6701 41.328H78.1914C78.6323 43.3531 80.2291 44.9509 82.2528 45.3922V48.9166C82.2528 49.5426 82.7601 50.0502 83.3856 50.0502C84.0112 50.0502 84.5185 49.5426 84.5185 48.9166V44.3802C84.5185 43.7542 84.0112 43.2466 83.3856 43.2466C81.7037 43.2466 80.3356 41.8773 80.3356 40.1944C80.3356 39.5684 79.8283 39.0608 79.2028 39.0608H68.6588C68.0333 39.0608 67.526 39.5684 67.526 40.1944C67.526 41.8774 66.1578 43.2466 64.476 43.2466C63.8504 43.2466 63.3432 43.7542 63.3432 44.3802V66.9465H61.1305V35.2067L86.7318 35.2054V88.7111ZM73.931 65.1162C72.1909 65.1162 70.7753 63.6996 70.7753 61.9584C70.7753 60.2172 72.191 58.8004 73.931 58.8004C75.6711 58.8004 77.0868 60.2171 77.0868 61.9583C77.0868 63.6995 75.6712 65.1162 73.931 65.1162Z" fill="black"/>
</svg>


                <div class="info-how"> 

                <p class="how-head"> <span class="how-num">4</span> YOU PAY US THE COST </p>

                <p class="how-text">  Everything is done and delivered, and now you pay us for the cost of making and hosting your website. You pay us according to the plan you have chosen. Payments can be via cheques, Online Transfer or Google Pay</p>
                </div>
      



    </div>


</div>


                                <div>
                                    <h2 class="home-about-title" style="
    font-weight: bold;
    margin: 40px 0 21px;
    letter-spacing: 0px;
    font-size: 1.5em;
">WEDDING INVITATIONS</h2>
                                    <p style="
    /* font-weight: 600; */
    font-size: 20px;
    margin: 7px;
">Did you know, there exists more than one type of invitation card?</p>
                                </div>
                                <div style="
                                                align-items: center;
    font-size: 15px;
    padding: 10px 10vw;
    max-width: 1280px;
    margin: auto;
">
                                    <p class="wow animated fadeInDown resp-text-1" style="
    flex-basis: 50%;
    margin: 0;
    font-size: 14px;
">Yes! Apart from our traditional printed invitation cards, there also exists online invitation GIFs, electronic video wedding cards, invitation E-card images for WhatsApp and invitation videos. Many people have been investing their time and money into either one or all of the above without 
realizing that it can drain down your bank balance, take up most of your time and can be very stressful. Why spend so much on Book post and wait till the time the wedding invitation card reaches the attendees when you have Weddingo providing you with the facility to create your wedding invitation card online and share it to your chosen guests through Whatsapp or other social media platforms.

                                    </p>
                                    <p class="wow animated fadeInDown resp-text-1" style="
    margin: 0;
    padding: 18px 60px 50px 60px;
    flex-basis: 50%;
    font-size: 14px;
">
                                       On average, most couples spend between 10,000 up to lakhs on just printing the finest wedding invitations plus a WhatsApp image invite and/or other means of invitations. While Weddingo gives you all that for the price of one. Our online wedding invitation cards are not only environment-friendly but also helps save time that would have otherwise gone into printing and mailing the invitations by post. It allows you to put your energy in planning your perfect wedding that you have always wished for. We, at Weddingo, wish to help you design your customized online wedding invitations, giving complete control to you.

                                    </p>
                                </div>
                            </section>





                            <section data-id="orzgbei"
                                class="elementor-element elementor-element-orzgbei elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                data-element_type="section">
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="uufxyjq"
                                            class="elementor-element elementor-element-uufxyjq elementor-column elementor-col-50 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="4vw85gf"
                                                        class="elementor-element elementor-element-4vw85gf animated fadeInUp the-title-left elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-left clearfix">
                                                                <p class="the-title resp-text-center">
                                                                    Why weddingo you ask?</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-id="b0k0cm1"
                                                        class="elementor-element elementor-element-b0k0cm1 animated fadeInUp the-title-left elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-left clearfix">
                                                                <p class="the-title resp-text-center">
                                                                    Exciting features </p>
                                                            </div>
                                                            <ul class="feature-lists animated wow slideInDown">
                                                                <li>Couple Name generator</li>
                                                                <li>Save the date - Invitations</li>
                                                                <li>Colourful designs for your online wedding invitation
                                                                </li>
                                                                <li> Custom wedding invitation design</li>
                                                                <li>Gallery section specifically for your online wedding
                                                                    invitation</li>
                                                                <li>Our wedding invitation card manages your guest list
                                                                    for you</li>
                                                                <li>Social Media Campaigning</li>
                                                                <li>Access Map within the Online Invitation</li>
                                                                <li>Customised Wedding Logo</li>



                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div data-id="qfdjkcv"
                                            class="elementor-element elementor-element-qfdjkcv elementor-column elementor-col-50 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="qzog4cw"
                                                        class="elementor-element elementor-element-qzog4cw animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-text"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-text.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="nikah-text clearfix resp-text-center">
                                                                <p>We at Weddingo want to give you the best experience while creating and sharing your wedding invitation. With advancement and a society prone to constant changes, we want to adapt to the changing shift in the culture from paper to paperless, this includes going paperless with your invites. Benefits? Saves your time, money, travel and the job gets done fast and effectively. The best part is you don’t have to worry about the invite limit!

                                                                </p>
                                                                <p>We like to think of it as an eco-friendly solution to reducing paper usage and wish to bring this change in the wedding culture with your help.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>





                                    </div>





                                </div>






                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="features-wrapper">
                                        <div class="feature-item">

                                            <div class="text-wrapper">
                                                <p class="feature-title wow animated slideInLeft resp-text-center">
                                                    Couple Name Generator</p>
                                                <p class="feature-desc  wow animated slideInLeft resp-text-center">
                                                    Weddingo is not just any online wedding invitation card website, it
                                                    is one that comprises many exhilarating features like Notifications,
                                                    Weddingo maps, gallery, guest list, and many more. We are thrilled
                                                    to introduce you to our new couple name generator feature – <a
                                                        href="https://www.weddingo.in/couple-name-generator/"
                                                        style="font-weight: bold;">#Coupletag</a></p>
                                                <a href="https://www.weddingo.in/couple-name-generator/"
                                                    style="color: white !important;">
                                                    <div class="purchase-title try-btn btn-hover-effect wow animated slideInLeft"
                                                        id="tryitnow" onclick="couplename()">Try it now </div>
                                                </a>
                                            </div>


                                            <div class="features-photo-wrapper">
                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation - Couple Name Generator"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/namegenerator.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="feature-item">
                                            <div class="features-photo-wrapper">

                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation - Save The Date"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/feature1.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="text-wrapper">
                                                <h2 class="feature-title wow animated slideInRight resp-text-center">
                                                    Save The Date - Invitations</h2>

                                                <p class="feature-desc wow animated slideInRight">This feature allows
                                                    you to make announcements regarding your wedding, you can add a few
                                                    details about the bride and the groom in the wedding announcement
                                                    section of your invitation card.</p>
                                            </div>
                                        </div>



                                        <div class="feature-item">

                                            <div class="text-wrapper">
                                                <h3 class="feature-title wow animated slideInLeft resp-text-center">
                                                    Colourful designs for your online wedding invitation </h3>
                                                <p class="feature-desc  wow animated slideInLeft resp-text-center">
                                                    Experiment with our wide range of themes, colours, designs, and
                                                    background styles to reflect your wedding invitation as per your
                                                    desires, best fitting yours and your partner’s personalities. After
                                                    all your online wedding invitations look will determine how great
                                                    your wedding is going to be, so why not excite your guests too to
                                                    look forward to it with an extravagant wedding invite. </p>
                                            </div>


                                            <div class="features-photo-wrapper">

                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation -  Colorful Background"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/feature2.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>



                                        <div class="feature-item">
                                            <div class="features-photo-wrapper">

                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation -  Custom Wedding Invitation"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/feature3.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="text-wrapper">
                                                <h3 class="feature-title wow animated slideInRight resp-text-center">
                                                    Custom wedding invitation design</h3>
                                                <p class="feature-desc wow animated slideInRight resp-text-center">You
                                                    can customise your wedding invitation by setting up an invite code,
                                                    you get to choose who receives them, and your visitors will only see
                                                    the events you wish for them to see.This feature is unique only at
                                                    weddingo for online wedding invitation card. </p>
                                            </div>
                                        </div>



                                        <div class="feature-item">

                                            <div class="text-wrapper">
                                                <p class="feature-title wow animated slideInLeft resp-text-center">
                                                    Gallery section specifically for your online wedding invitation</p>
                                                <p class="feature-desc wow animated slideInLeft resp-text-center">Ever
                                                    seen a wedding card with pictures to play around with to add some
                                                    interesting themes to your wedding invite content? With the Weddingo
                                                    online custom wedding invitation, you can also add your own
                                                    pictures, family portraits videos and other relevant images to the
                                                    weddingo invite to allow your visitors a sneak peak of whom the
                                                    bride and groom are and help them know whom they are looking forward
                                                    to meet.</p>
                                            </div>

                                            <div class="features-photo-wrapper">

                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation -  Gallery"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/feature4.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="feature-item">
                                            <div class="features-photo-wrapper">

                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation -  Guestlist"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/feature5.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="text-wrapper">
                                                <p class="feature-title wow animated slideInRight resp-text-center">Our
                                                    wedding invitation card manages your guest list for you</p>
                                                <p class="feature-desc wow animated slideInRight resp-text-center">
                                                    Weddingo allows you to predict the number of guests planning to
                                                    attend your wedding, with the RSVP feature followed within the
                                                    wedding invitation itself. This will help you plan your wedding
                                                    without having to worry about over booking or under booking
                                                    attributes required for the preparation of your wedding ceremonies.
                                                    Wouldn’t it be wonderful to have a hands-on on how many people to
                                                    expect before your wedding? What better way than to do it through
                                                    your online wedding invitation card, convenience at its best. </p>
                                            </div>
                                        </div>



                                        <div class="feature-item">

                                            <div class="text-wrapper">
                                                <p class="feature-title wow animated slideInLeft resp-text-center">
                                                    Social Media</p>
                                                <p class="feature-desc  wow animated slideInLeft resp-text-center">Sync
                                                    your social media such as WhatsApp profiles for easy updates on your
                                                    wedding card for ease of access to your visitors whom you choose to
                                                    invite at your wedding. Share your wedding pictures with your
                                                    friends and family with one click of the button on all or selected
                                                    social media platforms as per your convenience. </p>
                                            </div>


                                            <div class="features-photo-wrapper">

                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation -  Social Media"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/feature8.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>

                                        </div>



                                        <div class="feature-item">
                                            <div class="features-photo-wrapper">

                                                <img src="images/features/feature7.png"
                                                    alt="Online Wedding Invitation -  Map"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/features/feature7.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="text-wrapper">
                                                <p class="feature-title wow animated slideInRight resp-text-center">Map
                                                </p>
                                                <p class="feature-desc wow animated slideInRight resp-text-center">
                                                    Another exciting feature we’ve added, to make it easier for your
                                                    visitors to plan their trip to make it to your special wedding day.
                                                    This allows users to locate the distance between their locations to
                                                    the venue using our maps feature.</p>
                                            </div>
                                        </div>

                                        

                                        <div class="feature-item">
                                            
                                            <div class="text-wrapper">
                                                <p class="feature-title wow animated slideInRight resp-text-center">Customised Wedding Logo
                                                </p>
                                                <p class="feature-desc wow animated slideInRight resp-text-center">
                                                    A wedding logo is a unique symbol that represents your union and your new life as a couple. It’s a great way to bring coherence to your theme and everything that flows from it. Create a beautiful wedding logo that will stay with you for your married days and will look great on your wedding invitations, menu, facebook and save the date invitations. Weddingo provides you with the best wedding logos that are often made with a combination of initials of the bride and groom, to add more uniqueness to your wedding.                                                </p>
                                            </div>

                                            <div class="features-photo-wrapper">

                                                <img src="images/logocustom.png"
                                                    alt="Online Wedding Invitation - Logo"
                                                    class="progressive__img progressive--not-loaded"
                                                    data-progressive="images/logocustom.png">

                                                <div class="elementor-shape elementor-shape-bottom"
                                                    data-negative="false" style="width:80%;left:10%;">
                                                    <svg class="wow animated svgheight"
                                                        xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                                        preserveAspectRatio="none">
                                                        <path class="elementor-shape-fill"
                                                            d="M0 0L2600 0 2600 69.1 0 0z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                                            d="M0 0L2600 0 2600 69.1 0 69.1z"></path>
                                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                                            d="M2600 0L0 0 0 130.1 2600 69.1z"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>





                                    </div>
                                </div>





                            </section>
                            <section data-id="r9x9k7r"
                                class="elementor-element elementor-element-r9x9k7r elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-tilt&quot;,&quot;shape_divider_bottom&quot;:&quot;tilt&quot;}"
                                data-element_type="section">
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-shape elementor-shape-top" data-negative="false">
                                    <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                        preserveAspectRatio="none">
                                        <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                            d="M0 0L2600 0 2600 69.1 0 69.1z" />
                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                            d="M2600 0L0 0 0 130.1 2600 69.1z" />
                                    </svg>
                                </div>
                                <div class="elementor-shape elementor-shape-bottom" data-negative="false">
                                    <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 1000 100"
                                        preserveAspectRatio="none">
                                        <path class="elementor-shape-fill" d="M0,6V0h1000v100L0,6z" />
                                    </svg>
                                </div>
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="hcgyn2d"
                                            class="elementor-element elementor-element-hcgyn2d elementor-column elementor-col-100 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="rkhqqtc"
                                                        class="elementor-element elementor-element-rkhqqtc the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-center clearfix">
                                                                <p class="the-title" id="titlelove">
                                                                    We are going to... </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-id="rr6laid"
                                                        class="elementor-element elementor-element-rr6laid the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-center clearfix">
                                                                <p class="the-title">
                                                                    Celebrate Your Love </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="with-weddingo">With Weddingo</div>
                                                    <div class="purchase-title try-btn btn-hover-effect" id="otherbtn">
                                                        <a href="invitation-designs/"
                                                            style="color: white !important;" onclick="invitaions()">View Demo</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section data-id="bnjsyof"
                                class="elementor-element elementor-element-bnjsyof elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                data-element_type="section">
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="mmdlumg"
                                            class="elementor-element elementor-element-mmdlumg elementor-column elementor-col-100 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <section data-id="usqvnpe"
                                                        class="elementor-element elementor-element-usqvnpe elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                        data-element_type="section">
                                                        <div class="elementor-container elementor-column-gap-no">
                                                            <div class="elementor-row">
                                                                <div data-id="qupcqji"
                                                                    class="elementor-element elementor-element-qupcqji elementor-column elementor-col-100 elementor-inner-column"
                                                                    data-element_type="column">
                                                                    <div
                                                                        class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div data-id="6zjo058"
                                                                                class="elementor-element elementor-element-6zjo058 horizontal-item-left extra-padding-use elementor-widget elementor-widget-nikah-post-block"
                                                                                data-element_type="nikah-post-block.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="blog clearfix">
                                                                                        <div
                                                                                            class="blog-section blog-style-1-block content-section clearfix">




                                                                                            <article
                                                                                                class="blog-item selector-padding column-4 tablet-column-2 mobile-column-1">

                                                                                                <div class="post-thumb">
                                                                                                    <a href=""
                                                                                                        title="Quisque non fermentum Proin orci quis est.">
                                                                                                        <img src="images/pricing/Free Trail.jpg"
                                                                                                            class="pricing-image"
                                                                                                            alt="Quisque non fermentum Proin orci quis est.">
                                                                                                        <div
                                                                                                            class="nikah-overlay">
                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>




                                                                                                <div
                                                                                                    class="post-content-wrap">
                                                                                                    <div
                                                                                                        class="post-content">


                                                                                                        <div
                                                                                                            class="meta-wrapper clearfix">
                                                                                                            <span
                                                                                                                class="standard-post-categories">•
                                                                                                                Make an
                                                                                                                online
                                                                                                                wedding
                                                                                                                invitation
                                                                                                                for free
                                                                                                            </span>
                                                                                                            <span
                                                                                                                class="pricing-sub-title"></span>
                                                                                                        </div>



                                                                                                        <p class="post-title"
                                                                                                            style="font-size: 17px; text-align: center;">
                                                                                                            <strike></strike>
                                                                                                            <br><span
                                                                                                                class="new-price">Free</span>
                                                                                                        </p>

                                                                                                        <div
                                                                                                            class="separator-line">
                                                                                                            <span></span>
                                                                                                        </div>



                                                                                                        <div
                                                                                                            class="post-text">
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Upload
                                                                                                                Pictures
                                                                                                                and
                                                                                                                Videos
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Add
                                                                                                                content
                                                                                                                with
                                                                                                                your
                                                                                                                details
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Share
                                                                                                                with
                                                                                                                your
                                                                                                                friends
                                                                                                                and
                                                                                                                family
                                                                                                            </p>
                                                                                                        </div>



                                                                                                        <!--
                                                                                                        <a href="contact/" target="_blank" class="read-more purchase-btn">
                                                                                                            Trial </a>
-->
                                                                                                        <a onclick="purchase('Free')" href="contact/"
                                                                                                            target="_blank"
                                                                                                            class="read-more purchase-btn">
                                                                                                            Contact Us
                                                                                                        </a>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </article>


                                                                                            <article
                                                                                                class="blog-item selector-padding column-4 tablet-column-2 mobile-column-1">

                                                                                                <div class="post-thumb">
                                                                                                    <a href=""
                                                                                                        title="Quisque non fermentum Proin orci quis est.">
                                                                                                        <img src="images/pricing/HTML Template.jpg"
                                                                                                            alt="Quisque non fermentum Proin orci quis est."
                                                                                                            class="pricing-image">
                                                                                                        <div
                                                                                                            class="nikah-overlay">
                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>




                                                                                                <div
                                                                                                    class="post-content-wrap">
                                                                                                    <div
                                                                                                        class="post-content">


                                                                                                        <div
                                                                                                            class="meta-wrapper clearfix">
                                                                                                            <span
                                                                                                                class="standard-post-categories">•
                                                                                                                Make
                                                                                                                your own
                                                                                                                wedding
                                                                                                                invitation
                                                                                                                design</span>
                                                                                                            <span
                                                                                                                class="pricing-sub-title"
                                                                                                                style="    font-size: 0.8em;">For
                                                                                                                the Tech
                                                                                                                Savy</span>
                                                                                                        </div>



                                                                                                        <p class="post-title"
                                                                                                            style="font-size: 17px; text-align: center;    margin-top: -25.5px;
">
                                                                                                            <br><span
                                                                                                                class="new-price"><strike>
                                                                                                                    &#8377;1200
                                                                                                                </strike></span>
                                                                                                            <br><span
                                                                                                                class="new-price">&#8377;600</span>
                                                                                                        </p>

                                                                                                        <div
                                                                                                            class="separator-line">
                                                                                                            <span></span>
                                                                                                        </div>



                                                                                                        <div
                                                                                                            class="post-text">
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                HTML/CSS/JS
                                                                                                                files
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Tech
                                                                                                                Support
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Free
                                                                                                                Weddingo
                                                                                                                Sub
                                                                                                                Domain
                                                                                                                and web
                                                                                                                hosting
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Gallery
                                                                                                            </p>
                                                                                                        </div>



                                                                                                        <!--
                                                                                                        <a href="contact/" target="_blank" class="read-more purchase-btn">
                                                                                                            Download </a>
-->
                                                                                                        <a onclick="purchase('600')" href="contact/"
                                                                                                            target="_blank"
                                                                                                            class="read-more purchase-btn">
                                                                                                            Contact Us
                                                                                                        </a>


                                                                                                    </div>
                                                                                                </div>
                                                                                            </article>

                                                                                            <article
                                                                                                class="blog-item selector-padding column-4 tablet-column-2 mobile-column-1">

                                                                                                <div class="post-thumb">
                                                                                                    <a href=""
                                                                                                        title="Quisque non fermentum Proin orci quis est.">
                                                                                                        <img src="images/pricing/Weddingo Invite.jpg"
                                                                                                            alt="Quisque non fermentum Proin orci quis est."
                                                                                                            class="pricing-image">
                                                                                                        <div
                                                                                                            class="nikah-overlay">
                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>




                                                                                                <div
                                                                                                    class="post-content-wrap">
                                                                                                    <div
                                                                                                        class="post-content">


                                                                                                        <div
                                                                                                            class="meta-wrapper clearfix">
                                                                                                            <span
                                                                                                                class="standard-post-categories">•
                                                                                                                Wedding
                                                                                                                invitation
                                                                                                                card
                                                                                                            </span>
                                                                                                            <span
                                                                                                                class="pricing-sub-title">For
                                                                                                                Everyone</span>
                                                                                                        </div>



                                                                                                        <p class="post-title"
                                                                                                            style="font-size: 17px; text-align: center;">
                                                                                                            <br><span
                                                                                                                class="new-price"><strike>
                                                                                                                    &#8377;5000
                                                                                                                </strike></span>
                                                                                                            <br><span
                                                                                                                class="new-price">&#8377;3000</span>
                                                                                                        </p>

                                                                                                        <div
                                                                                                            class="separator-line">
                                                                                                            <span></span>
                                                                                                        </div>



                                                                                                        <div
                                                                                                            class="post-text">
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                We Make
                                                                                                                It For
                                                                                                                You</p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Free sub
                                                                                                                domain
                                                                                                                Name and
                                                                                                                Web
                                                                                                                Hosting
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Tech
                                                                                                                Support
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Gallery
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Share
                                                                                                                with
                                                                                                                your
                                                                                                                friends
                                                                                                                and
                                                                                                                family
                                                                                                            </p>
                                                                                                        </div>



                                                                                                        <!--
                                                                                                        <a href="contact/" target="_blank" class="read-more purchase-btn">
                                                                                                            Purchase </a>
-->
                                                                                                        <a onclick="purchase('3000')" href="contact/"
                                                                                                            target="_blank"
                                                                                                            class="read-more purchase-btn">
                                                                                                            Contact
                                                                                                            Us</a>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </article>

                                                                                            <article
                                                                                                class="blog-item selector-padding column-4 tablet-column-2 mobile-column-1">

                                                                                                <div class="post-thumb">
                                                                                                    <a href=""
                                                                                                        title="Quisque non fermentum Proin orci quis est.">
                                                                                                        <img src="images/pricing/Weddingo destination.jpg"
                                                                                                            alt="Quisque non fermentum Proin orci quis est."
                                                                                                            class="pricing-image">
                                                                                                        <div
                                                                                                            class="nikah-overlay">
                                                                                                        </div>
                                                                                                    </a>
                                                                                                </div>




                                                                                                <div
                                                                                                    class="post-content-wrap">
                                                                                                    <div
                                                                                                        class="post-content">


                                                                                                        <div
                                                                                                            class="meta-wrapper clearfix">
                                                                                                            <span
                                                                                                                class="standard-post-categories">•
                                                                                                                Custom
                                                                                                                wedding
                                                                                                                invitation
                                                                                                            </span>
                                                                                                            <span
                                                                                                                class="pricing-sub-title">For
                                                                                                                Everyone</span>
                                                                                                        </div>



                                                                                                        <p class="post-title"
                                                                                                            style="font-size: 17px; text-align: center;">

                                                                                                            <br><span
                                                                                                                class="new-price"><strike>
                                                                                                                    &#8377;8000
                                                                                                                </strike></span>
                                                                                                            <br>
                                                                                                            <span
                                                                                                                class="new-price">&#8377;6000</span>
                                                                                                        </p>

                                                                                                        <div
                                                                                                            class="separator-line">
                                                                                                            <span></span>
                                                                                                        </div>



                                                                                                        <div
                                                                                                            class="post-text">
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                We Make
                                                                                                                It For
                                                                                                                You</p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Free sub
                                                                                                                domain
                                                                                                                Name and
                                                                                                                Web
                                                                                                                Hosting
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Tech
                                                                                                                Support
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                More
                                                                                                                Templates
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                We Send
                                                                                                                Notifications
                                                                                                                To Your
                                                                                                                Guests
                                                                                                            </p>
                                                                                                            <p
                                                                                                                class="pricing-item-text">
                                                                                                                Guestlist
                                                                                                                Dashboard
                                                                                                            </p>
                                                                                                        </div>



                                                                                                        <!--                                                                                                        <a href="contact/" target="_blank" class="read-more purchase-btn">Get It Now </a>-->

                                                                                                        <a onclick="purchase('6000')" href="contact/"
                                                                                                            target="_blank"
                                                                                                            class="read-more purchase-btn">Contact
                                                                                                            Us</a>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </article>


                                                                                        </div>


                                                                                    </div>



                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="counter-section">
                                <p class="counter-heading">Here We Have Some Cool Numbers For You...</p>
                                <div id="counter">
                                    <div class="counter-div">
                                        <div class="counter-value" data-count="40">0</div>
                                        <p class="counter-value-title">Number Of People Visit Us Daily</p>
                                    </div>
                                    <div class="counter-div">
                                        <div class="counter-value" data-count="10">0</div>
                                        <p class="counter-value-title">Number Of People Approach Us Daily</p>
                                    </div>
                                    <div class="counter-div">
                                        <div class="counter-value" data-count="150">0</div>
                                        <p class="counter-value-title">Our Happy Couples</p>
                                    </div>
                                </div>
                            </section>
                            <style>
                                .counter-section {
                                    /* max-width: 1200px;
                                    margin: 80px auto; */

                                    background-color: white;
    padding: 87px;

                                }

                                .counter-heading {
                                    text-align: center;
                                    font-weight: bold;
                                    letter-spacing: 0px;
                                    font-size: 1.5em;
                                    line-height: 1.5em;
                                }

                                #counter {
                                    display: flex;
                                }

                                .counter-div {
                                    width: 33.3%;
                                    text-align: center;
                                }

                                .counter-value {
                                    font-size: 50px;
                                    margin: 20px 0;
                                    color: #8db392;
                                    font-weight: bold;
                                    line-height: 52px;
                                }

                                .counter-value-title {
                                    margin: 0;
                                }

                                @media only screen and (max-width:600px) {
                                    #counter {
                                        flex-direction: column;
                                    }

                                    .counter-div {
                                        width: 100%;
                                    }
                                }
                            </style>



                            <section data-id="gsulzki"
                                class="elementor-element elementor-element-gsulzki elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}"
                                data-element_type="section">
                                <div class="elementor-background-overlay"></div>
                                <div class="elementor-shape elementor-shape-bottom" data-negative="false">
                                    <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 2600 131.1"
                                        preserveAspectRatio="none">
                                        <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
                                        <path class="elementor-shape-fill" style="opacity:0.5"
                                            d="M0 0L2600 0 2600 69.1 0 69.1z" />
                                        <path class="elementor-shape-fill" style="opacity:0.25"
                                            d="M2600 0L0 0 0 130.1 2600 69.1z" />
                                    </svg>
                                </div>
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="dehrloj"
                                            class="elementor-element elementor-element-dehrloj elementor-column elementor-col-100 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="wittpf0"
                                                        class="elementor-element elementor-element-wittpf0 the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-center clearfix">
                                                                <p class="the-title">
                                                                    The Culture We Always Keep </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <section data-id="umowyyq"
                                                        class="elementor-element elementor-element-umowyyq animated fadeInUp elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-invisible elementor-section elementor-inner-section"
                                                        data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="section">
                                                        <div class="elementor-container elementor-column-gap-no">
                                                            <div class="elementor-row">
                                                                <div data-id="itusrkg"
                                                                    class="elementor-element elementor-element-itusrkg elementor-column elementor-col-25 elementor-inner-column"
                                                                    data-element_type="column">
                                                                    <div
                                                                        class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div data-id="tunqpck"
                                                                                class="elementor-element elementor-element-tunqpck elementor-widget elementor-widget-heading"
                                                                                data-element_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <p
                                                                                        class="elementor-heading-title elementor-size-default">
                                                                                        01</p>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="pi77tre"
                                                                                class="elementor-element elementor-element-pi77tre the-title-center the-title-left elementor-widget elementor-widget-nikah-head-title"
                                                                                data-element_type="nikah-head-title.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div
                                                                                        class="head-title head-title-2 text-left clearfix">
                                                                                        <a class="weddingo-demo the-title"
                                                                                            href="contact/"
                                                                                            id="btnhover" onclick="purchase('')">Purchase</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="bricvgd"
                                                                    class="elementor-element elementor-element-bricvgd elementor-column elementor-col-25 elementor-inner-column"
                                                                    data-element_type="column">
                                                                    <div
                                                                        class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div data-id="cosbhjo"
                                                                                class="elementor-element elementor-element-cosbhjo elementor-widget elementor-widget-heading"
                                                                                data-element_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <p
                                                                                        class="elementor-heading-title elementor-size-default">
                                                                                        02</p>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="okofsjq"
                                                                                class="elementor-element elementor-element-okofsjq the-title-center the-title-left elementor-widget elementor-widget-nikah-head-title"
                                                                                data-element_type="nikah-head-title.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div
                                                                                        class="head-title head-title-2 text-left clearfix">
                                                                                        <p class="the-title"
                                                                                            id="titlebtn">
                                                                                            Customize </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="xxexhzq"
                                                                    class="elementor-element elementor-element-xxexhzq elementor-column elementor-col-25 elementor-inner-column"
                                                                    data-element_type="column">
                                                                    <div
                                                                        class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div data-id="ajgdfne"
                                                                                class="elementor-element elementor-element-ajgdfne elementor-widget elementor-widget-heading"
                                                                                data-element_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <p
                                                                                        class="elementor-heading-title elementor-size-default">
                                                                                        03</p>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="je7fcm5"
                                                                                class="elementor-element elementor-element-je7fcm5 the-title-center the-title-left elementor-widget elementor-widget-nikah-head-title"
                                                                                data-element_type="nikah-head-title.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div
                                                                                        class="head-title head-title-2 text-left clearfix">
                                                                                        <p class="the-title"
                                                                                            id="titlebtn">
                                                                                            Make it live</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-id="grrfpez"
                                                                    class="elementor-element elementor-element-grrfpez elementor-column elementor-col-25 elementor-inner-column"
                                                                    data-element_type="column">
                                                                    <div
                                                                        class="elementor-column-wrap elementor-element-populated">
                                                                        <div class="elementor-widget-wrap">
                                                                            <div data-id="hmwmbjn"
                                                                                class="elementor-element elementor-element-hmwmbjn elementor-widget elementor-widget-heading"
                                                                                data-element_type="heading.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <p
                                                                                        class="elementor-heading-title elementor-size-default">
                                                                                        04</p>
                                                                                </div>
                                                                            </div>
                                                                            <div data-id="9g0ms4a"
                                                                                class="elementor-element elementor-element-9g0ms4a the-title-center the-title-left elementor-widget elementor-widget-nikah-head-title"
                                                                                data-element_type="nikah-head-title.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div
                                                                                        class="head-title head-title-2 text-left clearfix">
                                                                                        <p class="the-title"
                                                                                            id="titlebtn">
                                                                                            Share </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section data-id="icfhdpu"
                                class="elementor-element elementor-element-icfhdpu elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                data-element_type="section">
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="fwbyeex"
                                            class="elementor-element elementor-element-fwbyeex elementor-column elementor-col-100 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="i9xbf17"
                                                        class="elementor-element elementor-element-i9xbf17 animated fadeInUp the-title-center elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-center clearfix">
                                                                <p class="the-title">
                                                                    Portfolio </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-id="uaqvmof"
                                                        class="elementor-element elementor-element-uaqvmof animated fadeInUp the-title-center the-title-center elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-center clearfix">
                                                                <p class="the-title">
                                                                    Our Happy Couples </p>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div data-id="rdnoioe" class="elementor-element elementor-element-rdnoioe extra-padding-use mix-filter-use elementor-widget elementor-widget-nikah-portfolio-block" data-element_type="nikah-portfolio-block.default">
                                                        <div class="elementor-widget-container home-testimonials-slider">

 <!-- <div class="item">
      -->
                                                                <!-- <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded"
                                                                            src="images/couples/compressedimg/../images/couples/mohika1.jpg"
                                                                            data-progressive="images/couples/mohika1.jpg"
                                                                            alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Anshika</p>
                                                                            <p class="couple-name-text"
                                                                                style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text">Mohit</p>
                                                                        </div>
                                                                        <p class="testimonial-text"> Weddingo is
                                                                            amazing, they did a fantastic job with our
                                                                            invites. Very professional and quick
                                                                            service.</p>
                                                                        <div class="view-btn"><a class="view-link"
                                                                                href="https://mohitwedsanshika.weddingo.in/"
                                                                                target="_new">
                                                                                <p class="view-text">View </p><img
                                                                                    class="view-btn-logo"
                                                                                    src="images/logo2.png"
                                                                                    alt="weddingo-logo"> <svg
                                                                                    class="view-btn-arrow"
                                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                                    width="12" height="12"
                                                                                    viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" />
                                                                                </svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                            <!-- </div> -->
                                                        <?php foreach ($data as $d) { ?>
                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/<?php echo $d->image; ?>" data-progressive="images/couples/<?php echo $d->image; ?>" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text"><?php echo $d->bridename; ?></p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> <?php echo $d->groomname; ?></p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                           <?php echo $d->review; ?>
                                                                        </p>
                                                                        <?php if($d->link != "") {  ?>
                                                                        <div class="view-btn" onclick="weddingo(<?php echo $d->link; ?>)"><a class="view-link" href="<?php echo $d->link; ?>" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>

                                                                        <?php } ?>
                                                                    </div>

                                                                </div>
                                                            </div>
                            <?php } ?>
                                                            <!-- <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/gandhaamit.jpg" data-progressive="images/couples/gandhaamit.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Gandha</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> Amit</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                           We were so impressed by the online wedding invitations from weddingo. They were trendy, cool and also environment friendly. It was a great 
                                                                           experience!
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="https://gandhawedsamit.weddingo.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/compressedimg/aksita.jpg" data-progressive="images/couples/aksita.png" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Ishita</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> Akshay</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                            We loved the way weddingo presented our online wedding invitation card. It was something different and amazing. Our friends were amazed. Our wedding seemed like a huge event just because of such an amazing wedding invitation design.We recommend those reading this to use weddingo as their online wedding invitation source.
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="https://www.weddingo.in/demo/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/compressedimg/aditikarthik.jpg" data-progressive="images/couples/aditikarthik.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Aditi</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text">Karthik</p>
                                                                        </div>
                                                                        <p class="testimonial-text">Weddingo were amazing designers and great with production! I got exactly what I envisioned. Abhay brought my initial thoughts to life and it was fun to see our custom invite pack get made. I highly recommend Weddingo to anyone out there.</p>
                                                                        <div class="view-btn"><a class="view-link" href="https://aditiwedskarthik.weddingo.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive 	progressive__img progressive--not-loaded" src="images/couples/compressedimg/nileshsuchita.jpg" data-progressive="images/couples/nileshsuchita.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Suchita</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> Nilesh</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                            Richard and his team are just magic! They did the most beautiful invitation for our wedding. From the family section to gallery, and the venue details. Easy to work with,great ideas and suggestions, highly experienced and professional.
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="https://nileshwedsuchita.weddingo.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/compressedimg/priyankachirag.jpg" data-progressive="images/couples/priyankachirag.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Priyanka</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text">Chirag</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                            We loved the way weddingo presented our online wedding invitation card. It was something different and amazing. Our friends were amazed. Our wedding seemed like a huge event just because of such an amazing wedding invitation design.
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="https://priyankawedschirag.weddingo.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/compressedimg/roshwita.jpg" data-progressive="images/couples/roshwita.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Shweta</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> Rohit</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                            Great work by Weddingo team. Thanks for making such an unique invitation for our wedding. This was not only an invitation but a website which depicted our love story and our bonding. Whenever we look at the website it cherishes all our memories.
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="https://roshweita.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->


                                                            <!-- <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/compressedimg/shalinivijesh.jpg" data-progressive="images/couples/shalinivijesh.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Shalini</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> Vijesh</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                            We loved the way weddingo presented our online wedding invitation card. It was something different and amazing. Our friends were amazed.
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="https://shaliniwedsvijesh.weddingo.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/compressedimg/shraddhasiddhesh.jpg" data-progressive="images/couples/shraddhasiddhesh.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Shraddha</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> Siddhesh</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                            I found designing our wedding invitations with weddingo to be fun and exciting. They had a huge range of templates to choose from which is ideal for any Wedding theme
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="https://shraddhawedssiddhesh.weddingo.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="testimonial-main-wrapper">
                                                                    <div class="photo-wrapper">
                                                                        <img class="couple-photo img-responsive progressive__img progressive--not-loaded" src="images/couples/compressedimg/nehawedsanket.jpg" data-progressive="images/couples/nehawedsanket.jpg" alt="Wedding Invitation Photo" />
                                                                    </div>
                                                                    <div class="testimonial-wrapper">
                                                                        <div class="couple-names">
                                                                            <p class="couple-name-text">Neha</p>
                                                                            <p class="couple-name-text" style="font-size: 50px;"> &</p>
                                                                            <p class="couple-name-text"> Sanket</p>
                                                                        </div>
                                                                        <p class="testimonial-text">
                                                                            Our wedding invitation was flawless. Weddingo went above & beyond the call of the duty to make sure that they was exactly as we wanted. We were so happy with them, couldn't be happier with the prompt and friendly service.
                                                                        </p>
                                                                        <div class="view-btn"><a class="view-link" href="http://sanketwedsneha.weddingo.in/" target="_new">
                                                                                <p class="view-text">View </p><img class="view-btn-logo" src="images/logo2.png" alt="weddingo-logo"> <svg class="view-btn-arrow" xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
                                                                                    <path d="M2 24v-24l20 12-20 12z" /></svg>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                            </div>




                                                   

                                                   
                                                </div>
                                                <div data-id="yvivjfy"
                                                class="elementor-element elementor-element-yvivjfy elementor-align-center elementor-mobile-align-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-button"
                                                data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                data-element_type="button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a href="portfolio/"
                                                            class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink"
                                                            role="button">
                                                            <span class="elementor-button-content-wrapper" onclick="feedback()">
                                                                <span class="elementor-button-text">OUR
                                                                    COUPLES</span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                         
                            <section data-id="khalzzr"
                                class="elementor-element elementor-element-khalzzr elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}"
                                data-element_type="section">
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="eilqgcy"
                                            class="elementor-element elementor-element-eilqgcy elementor-column elementor-col-100 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="knvgt7e"
                                                        class="elementor-element elementor-element-knvgt7e the-title-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-nikah-head-title"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="nikah-head-title.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="head-title head-title-2 text-center clearfix">
                                                                <p class="the-title write-to-us">
                                                                    Write to us, we at weddingo would love to hear from
                                                                    you
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-id="qvdmiqd"
                                                        class="elementor-element elementor-element-qvdmiqd elementor-align-center animated fadeInUp elementor-invisible elementor-widget elementor-widget-button"
                                                        data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}"
                                                        data-element_type="button.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-button-wrapper">
                                                                <a href="contact/"
                                                                    class="elementor-button-link elementor-button elementor-size-md elementor-animation-sink"
                                                                    role="button">
                                                                    <span onclick="approach('')" class="elementor-button-content-wrapper">
                                                                        <span class="elementor-button-text">GET IN
                                                                            TOUCH</span>
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section data-id="c82a5bd"
                                class="elementor-element elementor-element-c82a5bd elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                data-element_type="section">
                                <div class="elementor-container elementor-column-gap-no">
                                    <div class="elementor-row">
                                        <div data-id="d5e6f63"
                                            class="elementor-element elementor-element-d5e6f63 elementor-column elementor-col-100 elementor-top-column"
                                            data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="478b2a8"
                                                        class="elementor-element elementor-element-478b2a8 elementor-widget elementor-widget-nikah-instagram-block"
                                                        data-element_type="nikah-instagram-block.default">
                                                        <div class="elementor-widget-container">
                                                            <div
                                                                class="nikah-instagram-feed instagram-builder no-caption column-10">
                                                                <div id="nikah-instagram-feed-478b2a8"
                                                                    class="nikah-insta-grid">
                                                                </div>
                                                            </div>






                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <footer id="footer" class="footer clearfix">
            <div class="footer-wrap clearfix">

                <div class="footer-bottom clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="foot-col column column-3 item-col-1 vertical text-left clearfix">
                                <div id="copyright" class="copyright-text foot-col-item">
                                    © Copyright <span id="current_year"></span>, Design by <a
                                        href="https://pixoloproductions.com/" target="_blank" class="comname"
                                        rel="noopener">Pixolo Production</a>
                                </div>
                            </div>
                            <div class="foot-col column column-3 item-col-2 vertical text-center clearfix">
                                <div class="logo-footer foot-col-item">
                                    <a href="https://www.weddingo.in">
                                        <img src="wp-content/uploads/sites/96/weddingo.png" style="width: 207px;"
                                            alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="foot-col column column-3 item-col-3 vertical text-right clearfix">
                                <div class="social-footer foot-col-item">
                                    <ul>
                                        <li class="twitter soc-icon">
                                            <plink target="_blank" redirect-to="https://twitter.com/weddingo11"
                                                title="Twitter" class="fa fa-twitter" rel="nofollow"></plink>
                                        </li>
                                        <li class="facebook soc-icon">
                                            <plink target="_blank"
                                                redirect-to="https://www.facebook.com/weddingowebsite/" title="Facebook"
                                                class="fa fa-facebook" rel="nofollow"></plink>
                                        </li>
                                        <li class="instagram soc-icon">
                                            <plink target="_blank" redirect-to="https://www.instagram.com/weddin.go/"
                                                title="Instagram" class="fa fa-instagram" rel="nofollow"></plink>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </footer>
        <style>
            .wel-popup-wrapper {
                width: 100%;
                height: 100%;
                position: absolute;
                z-index: 999;
                top: 0px;
                left: 0;
                right: 0;
                background: rgba(0, 0, 0, 0.4);
                display: flex;
                align-items: center;
                justify-content: center;
                position: fixed;

            }

            .wel-popup {
                margin: auto;
                width: 75%;
                height: auto;
                background: #fff;
                padding: 40px 0;
                max-width: 800px;
                box-shadow: 1px 1px 5px 6px rgba(0, 0, 0, 0.2);
                position: relative;
                border-radius: 5px;
            }

            .wel-close-btn {
                position: absolute;
                top: 0;
                right: 0;
                padding: 15px;
                cursor: pointer;
            }

            .wel-popup .popup-img {
                width: 200px;
                padding: 0 30px;
                margin: auto;
            }

            .wel-popup-title1 {
                text-align: center;
                font-weight: 600;
                letter-spacing: 1px;
                margin: 25px;
                font-size: 16px;
            }

            .wel-popup-title2 {
                text-align: center;
                font-size: 40px;
                font-weight: bold;
                letter-spacing: 2px;
                margin: 20px;
                line-height: 40px;
            }

            .popup-options-div .popup-options {
                display: block;
                margin: 15px auto;
                font-size: 16px;
                border: 1px solid #1eb650;
                color: #1eb650;
                width: 400px;
                padding: 15px;
                text-transform: uppercase;
                border-radius: 50px;
                font-weight: bold;
                transition: .3s all;
                cursor: pointer;
                font-family: Raleway;
                letter-spacing: 1px;
                text-align: center;
            }

            .popup-options-div .popup-options:hover {
                color: #fff;
                background: #1eb650;
            }

            .popup-options-div input {
                display: none;
            }

            .wel-form-input {
                display: block;
                margin: 15px auto;
                width: 400px;
                border: 1px solid #1eb650;
                background: none;
                height: 48px;
                font-size: 16px;
                color: #1eb650;
                font-family: Raleway;
            }

            .wel-form-input::placeholder {
                color: #1eb650;
            }

            .wel-form-btn {
                display: block;
                margin: 15px auto;
                background: #1eb650;
                color: #fff;
                width: 400px;
                font-size: 16px;
                padding: 15px;
                font-family: Raleway;
                cursor: pointer;
                transition: .5s all;
            }

            .wel-form-btn:hover {
                background: #19a246;
            }

            @media only screen and (max-width:600px) {
                .wel-popup {
                    width: 90%;
                    padding: 20px 0;
                }

                .wel-popup .popup-img {
                    width: 150px;
                }

                .wel-popup-title1 {
                    font-size: 12px;
                    margin: 15px;
                    line-height: 22px;
                }

                .wel-popup-title2 {
                    font-size: 32px;
                    line-height: 32px;
                    margin: 10px;
                }

                .popup-options {
                    width: 75% !important;
                    font-size: 10px !important;
                    padding: 5px !important;
                }

                .wel-form-input {
                    width: 85%;
                }

                .wel-form-btn {
                    width: 85%;
                }
            }
        </style>
         <div class="wel-popup-wrapper"> 
            <div class="wel-popup">
                <svg class="wel-close-btn" width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                        <path d="M6.68183 5.49529L10.7559 1.42123C11.0812 1.0962 11.0812 0.569042 10.7559 0.244043C10.4309 -0.0813093 9.90405 -0.0813093 9.5787 0.244043L5.50462 4.3181L1.43021 0.244013C1.10554 -0.0813379 0.578027 -0.0813379 0.253028 0.244013C-0.072001 0.569042 -0.072001 1.0962 0.253028 1.4212L4.32743 5.49529L0.244013 9.57871C-0.0813377 9.90406 -0.0813377 10.4309 0.244013 10.7559C0.406513 10.9188 0.619647 11 0.832782 11C1.04557 11 1.2587 10.9188 1.42123 10.7559L5.50462 6.6725L9.5787 10.7466C9.74155 10.9091 9.95434 10.9907 10.1675 10.9907C10.3803 10.9907 10.5934 10.9091 10.7559 10.7466C11.0813 10.4216 11.0813 9.89476 10.7559 9.5694L6.68183 5.49529Z" fill="#1eb650" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="11" height="11" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
                <img class="popup-img" src="images/weddingoo.png">
                <div class="welcome-div">
                    <p class="wel-popup-title1">WELCOME TO WEDDINGO! WE THINK YOU’LL LOVE IT HERE.</p>
                    <p class="wel-popup-title2">Who Are You ?</p>
                    <div class="popup-options-div">
                        <input type="radio" name="customer" value="bride" id="option-input-one">
                        <label for="option-input-one" class="popup-options">I'M A Bride</label>

                        <input type="radio" name="customer" value="groom" id="option-input-two">
                        <label for="option-input-two" class="popup-options">I'M A Groom</label>

                        <input type="radio" name="customer" value="familyOrfriend" id="option-input-three">
                        <label for="option-input-three" class="popup-options">Looking For Friend And Family</label>

                        <input type="radio" name="customer" value="weddingPlanner" id="option-input-four">
                        <label for="option-input-four" class="popup-options">I'M A Wedding Planner</label>
                    </div>
                </div>
                <div class="wel-details-div">
                    <p class="wel-popup-title1">Contact us today, and get reply with in 24 hours!</p>
                    <input id="namevalue" class="wel-form-input" placeholder="Your name" type="text" tabindex="1" autofocus>
                    <input id="emailvalue" class="wel-form-input" placeholder="Your Email Address" type="email" tabindex="2">
                    <input id="contactvalue" class="wel-form-input" placeholder="Your Phone Number" type="tel" tabindex="3">
                    <button class="wel-form-btn" name="submit" type="submit">Submit</button>
                </div>
            </div>
        </div> 



        <!-- <div class="total-popup" id="total-pop">


            <div class="overlay-pop">
                <div class="header-pop">
                    <svg class="icon-pop" width="241" height="52" viewBox="0 0 241 52" fill="none"
                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <rect width="241" height="52" fill="url(#pattern0)" />
                        <defs>
                            <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                                <use xlink:href="#image0"
                                    transform="translate(-0.00113773) scale(0.00116004 0.00537634)" />
                            </pattern>
                            <image id="image0" width="864" height="186"
                                xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA2AAAAC6CAYAAADI3y5wAAAEGWlDQ1BrQ0dDb2xvclNwYWNlR2VuZXJpY1JHQgAAOI2NVV1oHFUUPrtzZyMkzlNsNIV0qD8NJQ2TVjShtLp/3d02bpZJNtoi6GT27s6Yyc44M7v9oU9FUHwx6psUxL+3gCAo9Q/bPrQvlQol2tQgKD60+INQ6Ium65k7M5lpurHeZe58853vnnvuuWfvBei5qliWkRQBFpquLRcy4nOHj4g9K5CEh6AXBqFXUR0rXalMAjZPC3e1W99Dwntf2dXd/p+tt0YdFSBxH2Kz5qgLiI8B8KdVy3YBevqRHz/qWh72Yui3MUDEL3q44WPXw3M+fo1pZuQs4tOIBVVTaoiXEI/MxfhGDPsxsNZfoE1q66ro5aJim3XdoLFw72H+n23BaIXzbcOnz5mfPoTvYVz7KzUl5+FRxEuqkp9G/Ajia219thzg25abkRE/BpDc3pqvphHvRFys2weqvp+krbWKIX7nhDbzLOItiM8358pTwdirqpPFnMF2xLc1WvLyOwTAibpbmvHHcvttU57y5+XqNZrLe3lE/Pq8eUj2fXKfOe3pfOjzhJYtB/yll5SDFcSDiH+hRkH25+L+sdxKEAMZahrlSX8ukqMOWy/jXW2m6M9LDBc31B9LFuv6gVKg/0Szi3KAr1kGq1GMjU/aLbnq6/lRxc4XfJ98hTargX++DbMJBSiYMIe9Ck1YAxFkKEAG3xbYaKmDDgYyFK0UGYpfoWYXG+fAPPI6tJnNwb7ClP7IyF+D+bjOtCpkhz6CFrIa/I6sFtNl8auFXGMTP34sNwI/JhkgEtmDz14ySfaRcTIBInmKPE32kxyyE2Tv+thKbEVePDfW/byMM1Kmm0XdObS7oGD/MypMXFPXrCwOtoYjyyn7BV29/MZfsVzpLDdRtuIZnbpXzvlf+ev8MvYr/Gqk4H/kV/G3csdazLuyTMPsbFhzd1UabQbjFvDRmcWJxR3zcfHkVw9GfpbJmeev9F08WW8uDkaslwX6avlWGU6NRKz0g/SHtCy9J30o/ca9zX3Kfc19zn3BXQKRO8ud477hLnAfc1/G9mrzGlrfexZ5GLdn6ZZrrEohI2wVHhZywjbhUWEy8icMCGNCUdiBlq3r+xafL549HQ5jH+an+1y+LlYBifuxAvRN/lVVVOlwlCkdVm9NOL5BE4wkQ2SMlDZU97hX86EilU/lUmkQUztTE6mx1EEPh7OmdqBtAvv8HdWpbrJS6tJj3n0CWdM6busNzRV3S9KTYhqvNiqWmuroiKgYhshMjmhTh9ptWhsF7970j/SbMrsPE1suR5z7DMC+P/Hs+y7ijrQAlhyAgccjbhjPygfeBTjzhNqy28EdkUh8C+DU9+z2v/oyeH791OncxHOs5y2AtTc7nb/f73TWPkD/qwBnjX8BoJ98VQNcC+8AAAILaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnRpZmY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vdGlmZi8xLjAvIj4KICAgICAgICAgPHRpZmY6UmVzb2x1dGlvblVuaXQ+MjwvdGlmZjpSZXNvbHV0aW9uVW5pdD4KICAgICAgICAgPHRpZmY6Q29tcHJlc3Npb24+MTwvdGlmZjpDb21wcmVzc2lvbj4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHRpZmY6UGhvdG9tZXRyaWNJbnRlcnByZXRhdGlvbj4yPC90aWZmOlBob3RvbWV0cmljSW50ZXJwcmV0YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgoPRSqTAABAAElEQVR4AexdB3wUVROf9N4gIRB6772DFKmCFew0BUFUlG4BREHFBgJiA2kqnw0FEVGKgvReQi+hhJZCAoH0nm/m8OByuUtud9/u7V1m+B13t/vevHn/d7nb2Zn3H5cCFGBhBBgBRoARYAQYAUaAEWAEGAFGgBFQHQFX1UfgARgBRoARYAQYAUaAEWAEGAFGgBFgBAwIsAPGHwRGgBFgBBgBRoARYAQYAUaAEWAENEKAHTCNgOZhGAFGgBFgBBgBRoARYAQYAUaAEWAHjD8DjAAjwAgwAowAI8AIMAKMACPACGiEADtgGgHNwzACjAAjwAgwAowAI8AIMAKMACPADhh/BhgBRoARYAQYAUaAEWAEGAFGgBHQCAF2wDQCmodhBBgBRoARYAQYAUaAEWAEGAFGgB0w/gwwAowAI8AIMAKMACPACDACjAAjoBEC7IBpBDQPwwgwAowAI8AIMAKMACPACDACjAA7YPwZYAQYAUaAEWAEGAFGgBFgBBgBRkAjBNgB0whoHoYRYAQYAUaAEWAEGAFGgBFgBBgBdsD4M8AIMAKMACPACDACjAAjwAgwAoyARgiwA6YR0DwMI8AIMAKMACPACDACjAAjwAgwAuyA8WeAEWAEGAFGgBFgBBgBRoARYAQYAY0QYAdMI6B5GEaAEWAEGAFGgBFgBBgBRoARYATYAePPACPACDACjAAjwAgwAowAI8AIMAIaIcAOmEZA8zCMACPACDACjAAjwAgwAowAI8AIsAPGnwFGgBFgBBgBRoARYAQYAUaAEWAENEKAHTCNgOZhGAFGgBFgBBgBRoARYAQYAUaAEWAHjD8DjAAjwAgwAowAI8AIMAKMACPACGiEADtgGgHNwzACjAAjwAgwAowAI8AIMAKMACPgzhAwAowAI8AIMAKMACPACDACjAAjoAYCiVm3oKAgHzLzsyEXn41yOvUiTDj8GeRAPri6uEDBfyd6hbWGT5q8DJ6uHsamTvfMDpjTLSlPiBFgBBgBRoARYAQYAUaAEdAOgdyCPEjPzYQzqZchNS8DnSn6B5CdnwOzT/4EOQU5cCUzAXJd7jpghawzel94cE3cTvB194IZDUeCu4tboWbO8salAMVZJsPzYAQYAUaAEWAEGAFGgBFgBBgBdREgx2rJxT/BhYbB/07fugRJWcmw7fpRdLLy8BAeNJy8fd5gjQSPg9y3fhU6wZwmYwxdne0/joA524ryfBgBRoARYAQYAUaAEWAEGAEVEVh5eTN8dOb7uyMYnS1klzA4X3fPgCEUZvrehtekY1XsdsgqyIUvm06woYdjNWESDsdaL7aWEWAEGAFGgBFgBBgBRoARsCsCv13dWnh8im5JiHAV7mz93dq43fDy4dnWGzjoGXbAHHTh2GxGgBFgBBgBRoARYAQYAUbAHghk5GfJHtbDxR383Hwg1DPIJh1/xu2Clw5/Ajn5uTa1d4RGnILoCKvENjICjAAjwAgwAowAI8AIMAJ6QcCYcmjFHldMIQzxCIDKfuWQ4fBuvCfYzQ/uC2sH1fwqQFJOCow8MtOmyNna2N3g4+oF7zd8HrxcPa2M6jiH2QFznLViSxkBRoARYAQYAUaAEWAEGAG7IxDuWQaOwgWrdriCK1TxDIOVbT+w2CY1NwMe2vGaxXMG8g7zdEZ0+FbGbMEsxwL4qOGL4OHq2C7MXZfUMgR8lBFgBBgBRoARYAQYAUaAEWAEGIE7CIR7l7nz2tKLXMiDQ6nnYF7Uckun8VgBXMiKKxr9QserhneFuwyKZr1/i9kKow59YnbU8d6yA+Z4a8YWMwKMACPACDACjAAjwAgwAnZDIAD3cJUoGLWac245TDu5pEjTFyNnFXG+/Nx9YGX79+HDRi9CRYyeGSJhRXoC/J243+GJOdgBs7CwfIgRYAQYAUaAEWAEGAFGgBFgBCwjEOqFBBol7AMz9HRxgW8vroUPTn1rSB+kY79d2QLbE48WURzk4QfNg2pD6zL14c26QyDEzb9IG+MBIuZ4IXImUAFoRxR2wBxx1dhmRoARYAQYAUaAEWAEGAFGwE4IeLl62D4yOmpfR6+B99EJI1l8+U+Lztuvbd67o/O+Cu3g40ajoIw7OmFWHL31cXvh1aOfQ2Ze9p1+jvKCHTBHWSm2kxFgBBgBRoARYAQYAUaAEdABAhG+5YpY4YZsh97IVGjRYUInaiE6YfPO/gKZ+UUdprZlGyA1vXchnT3CW8G79Z+HKt7hVnVSsebJJxZAloM5YeyAFVpqfsMIMAKMACPACDACjAAjwAgwAsUh4GkhAhbk7gtv13kGynoEWnSYXDAdkfaEnUu9Wlg1Omcz6o+AQExBNJe+Ee3h6+avQhUvdPisRMKImGNM5Fzzrrp+zw6YrpeHjWMEGAFGgBFgBBgBRoARYAT0hQDRwZuLBzplT1XtCa/XHAj+rkjSYcVhKtQP2zwY3gEycrPgZHI0HLt1HnYkHIVt1w7DlmuHID7jBgS6+8EbtQdB84BaVnWuT9xnKNZcSLeO3zg2ib6OgWXTGAFGgBFgBBgBRoARYAQYAWdEINDDt8i08iHfcOyxKvdCmHcwTDmxEGKyE4uwHRbqiH7c/qRTEHkz6s5erpz8nDvuHTEjkrNHqY03c1OK1bU2bje8WDATvmr2aqEh9PiGI2B6XBW2iRFgBBgBRoARYAQYAUaAEdApAq6436uIoDNFzpIL/utargXMbPQShHsUXy+MdMRm3YDLmQmQkHPL8LiZlw63/nvEZF03nL+QEQtJOalFhjQ/sC5+r4GiPsvCPjPztvZ8bwE9e5rDYzMCjAAjwAgwAowAI8AIMAKMgJ4RIMINc8kryIfc/Lu08B1CG2M0ajwEuhWNlpn3Ffn+z9hdMOn4AsjJzxWpVqiuougJVc/KGAFGgBFgBBgBRoARYAQYAUbAmRDwNWMspLlR9OuWWZSqeUhdmFxnsNW9W6pggvvKiJjjtaNfqqJehFJ2wESgyDoYAUaAEWAEGAFGgBFgBBiBUoJAARFsmPFwUATsRnZyEQR6lm9TpG2RRqYHSLetD9N+Zq9XxW6DlyNnmx3Vx1sm4dDHOrAVjAAjwAgwAowAI8AIMAKMgGMgYOZ8GY12g8KxHdqLNeHIPOPpEp8b+FWD9xoOh5ScDEPbtLwMyC+4PRjR2Gfn5UBuQR74uHnBhbQYmHX+Z+tki+jE/Rm/C1wi58BnzcaVOLaWDdgB0xJtHosRYAQYAUaAEWAEGAFGgBFwQgRy8/MhNvMG1AyodGd2RMix+frhO+9LenE+PQZ2XD8KL9d6rKSmcASZEz9BB6wkWRO/E3Ij8+CzpuPA3cWtpOaanC/spmoyJA/CCDACjID9EcjOzra/EWyBAQFeC/4gMAKMACPg+AhQZOpG9q1CE7mUEQ+uGLkyFXLK/N2wTpgFySzIhk/O/gwzTn1j4WzhQzlEe/9fdKzwmaLv1sXtgUnH5oNe2BE5AlZ0jfgII8AIOCECycnJsGH9evj333/h1MmTkJOTA/4BAdCmTRvo2asXdOjQASi9gUV9BHgt1MeYR3BcBIhFLiYtHq6kx0NcRiLEZFzDqMJ1uI4U3Ym4vyYu+ybk4YXnLUzNysB6SUF4IeuDBXA9XdyhnEcQlPUMgBCPAAj3KgvlvctCOe8yUNGvHFTyLQ9+Fmo3OS5Sjm251HUug0yCnq7u4If1sMI9cZ1xrcO9ykAF71Co4IMP3zCo7FceynqHaAQMcc7jw+R3k0g4yAkzlVcPf34nhdB4nNpNxsLKs879CDdykVrePJ0Rf4oXRq9Bx80VJtUdYuxm+zP9lFvQ+evVzWhyAcxq8rLtulRqyQ6YSsCyWkaAEdAPAseOHYM3J0+GGzduFDIqNSUFNm3caHi0bdcOprz5JgQHBxdqw2/EIsBrIRZP1ub4CCRk3ICTN89jOtVp2JN0AvamnoNss4vY4mZJjhg9SC5l43dcmvXWNb3CoF1wPWgSVBvqBdWAukHVwcvN03oHPiMMAaXrfAPrYqHnbZBzWQlW7Qp3D4COwfWhBT4ahdTGNa4G3rhfSrTQDUvycUxvW5LzdTz5PPSHrneGi81JuvPa+CLMPQgerXwveOCNg2mnl0BafmYRh4n0fx39B9CestfqDjJ2LfE52N0PKniFwsn0i0V0krErYrbg31cOzMN0RHuKC3qC5j6iPe3hsRkBRoAREIrAmTNn4KUXXgBb0tzqN2gAn376KXj7WE6NEGpYKVTGa1EKF52nXASBfGSKO3nzHOxKOAx/J+xDh+t8kTZaHaDIWe+QZtAptDm0C2sCEX7hWg3t9OPoZZ1pjfuWaQGdw1pA+7BmEOZTcmFkWxYnOj0Wum59pUjmyP3l2sHnzScYVPx65V+YenIRZJoVRf6y6QToU74dOnAFsOLKZph+aimk5uNNBEseCbopz1brC2/XG1bErAN40+Kx3VMKReHCvULg5ar94KeYf+F42gUrOgEernAPzG06pohOrQ5wBEwrpHkcRoAR0ByBjIwMeHvqVJucLzLu5IkTsGjRInj5lVc0t9XZB3SEtSAbKUIXfeEC3Lx5E7y8vSE8PBwaN24MERERzr5EPD+VEbiUEgPrYnbAirgtcCYzXuXRbFNPKYyrru8zPOA0QFv/GvBIhS7Qo0I7CBV0oW6bJc7TSm/rTGu8InGP4QEnAfqENIUHKnSGLuVbKUpJpcgU7e0y95lMCzSvittWxPmilW4QWM2w4LQX7LFK92IKrRc6YUsgAdNsiwiO8U30WvB28TBEwqiPqdD4pkfSczOhBpKATK8/DF48NAsS8lCnuZHY4fe47Yb9YF80m1hkj5qpfrVeswOmFrKslxFgBOyOwN8bNsDVq1cl2bH855+hX//+ULFiRUn9uHHxCOh5LbKysmD58uXw/bJlkJ6OaT4WpE3btvDSqFFQo0YNC2f5ECNgGQHa57Mt/gD8fHk9rE86YrmRjo7uwWjcnqjzMCXqG+gf2hYeq9IT2oQ2LhLl0JHJujDFkdZ5bdJhoIf/KS94pkJ36F+lB9QIrCwPR3PvB7Xcyr2dA5uVlw3kDBUSdHz6hXeCMK/Cqf73R3SAMp6BMPbop3AN9zgWEew3P3o1OnM58Fb9oehw3XW57r663Yvy+lJzM6BD+cbwYaMX4bVjX8L13KK1yaj1uvi9MOHoZ9juBfBy1TYV17XIJPkAI8AIMAJOgsC6detkzWTXzp2y+nEn6wjodS0SEhJgzOjRsHDBAqvOF81q75498NzQofD3339bnySfYQT+Q4AuPFdG/w19t42GYUdmOoTzZbp4+Rgy+DVxNzx18F14dMersOHKDsjJzzVtwq8RAUde59T8LPji6l/Qfdd4GLv/I4i8fhJX3TxUZH2ZiSDD26Xo3jIiiCH55fImOIr7wcyld/m24OvmbX4Y2pRtAHMajwY/K+yI1OGbi2vh49PfF+lreqAAx8/CWmEk3cJbwnsNRkA5T3T4zD01aoDHVsVsg8nHvi5CHkKn1RR2wNREl3UzAoyA3RAwpJMdPSpr/KMy+8karBR00utaEBvjxAkT4MTx4zatQl5eHrw7fTr8u2mTTe25UelDgO76k+PVfesomHD6a4jKvObwIBxKuwgjj8+FftsnwtbY/QYWOYeflMIJ0Dr/dvEfp1nn36/vh37734IX974HR25gLqoN4oGsm8Ee/kVaZmOUiuRs2hXINbKGGFuhb+ZpJdJEqYsdMNo6udZAZPb0teowfRW96o4TZhoJMw5h/nwfptOSYxfmFmRV54qYzfD60a/Mu6r6nlMQVYWXlTMCjIC9EEjBi2u5QlERFnEI6HUt5s6ZAxfOF71DW9LM33v3Xahdpw5UqnS32GhJffi8cyNAhAubYvbA+1HfwYWsRKec7PGMq/DMkY+gS3Q9mNpgONQMquqU8yxuUsRbtyl2D8w4861TrjOlya7fdwTTT9vAmLoDoYq/9b2v+Rhpyi0oGhX1dvGE6NRY2JJ4uPDeK4w2PVz+HmgZXKc4iGFAtd7g7e6F5B2LIR0ssCNi2OqrC6sM7PePVOxs2akyG4Ecuy+QGOTt44vgZMalwnZhW2JcXBmzBXI0ZEfkCJjZIvFbRoARcA4EsrHOl1yhiA2LOAT0uBbHMer1j8x0Qqoh992334oDiDU5NAJnbl2AobvfghHHZjvlRbn54mxJPgW9dr8KX536CTJys8xPO+37qORoeG7PNBh+9BOnX+eViXuh+85x8MXJHzHN0vLvob+7DzQIQCfcLLWPnKfT6ZchOjOu8GcBsxtrIzlGoIdf4eMW3vWv1BVmNRoFZdwCiug3NMcxvzy/CqYcXWCht+VDrcvUh3cbPQ+VPcOs6vwjVrvtB+yAWV4nPsoIMAKMACPgxAhsxoLcSmQ97i9MwTpyLKUXgQzc50UXqL13vwZbk21L23IWtGiP2McXV8CAXZPh7K2LzjIti/MgJ/Pr08vhvl2vwb+3Tlhs44wHczGqO+vSSnho+wTYk1CUQMYX92rVIwfMTLJxr2AmOeZmjpkP7her7mM9omamBvpg6uDb9YZCGaxrZlFQ/75bp8GlwGwgHBgDWneE9i4mZN2EjQkH4PlDH0JsFtbKQ2fQkni6eVg6rMoxTkFUBVZWyggwAowAI6BnBHbu2KHIPEpFOn36NLRq1UqRHu7smAgcT4qCV5E97WRGrGNOQJDVkemX4P49r8MHdZ6DftV60KWvIM36UHMao5sTD8+DYxlX9GGQHaygos9ExvJ8RC8YU38Q+GLki4ScHFcL6334ZhRcSS+697FTuSbQt0J7STN4qGIniPAJg5GHPoIbuakW+xbxv7BVgDvuIUM5nxoDX5xfgemFW4s4hIYGdvyPHTA7gs9DMwKMACPACGiPQH5+Ply5ovyC6sb169obzyPaFQGiG192bjW8d/5H3AFj5Ta6XS3UfvDsgjwD4cgJZLx7teFQ8HLTls5bjRnTnr4fz/8F084tw31OyBzBAl/HbIDtScdgNhYvrhtUw4BIoMdtR8cUniR0lOhhLlQ3TI40D6kDHzZ8Ed44/hXcyEO9JfzZZWHR5zXxOyAG92LOjVoO13KR1t7G+wK07loJO2BaIc3jMAKMACPACDgVAi6u8i4onAqEUjSZpKxkmHrkc/jzxqFSNGvbp7o49h+ISrsMc1u8CiFeyDjnoJKcnQrTkRGP9kGxFEbgREYMPLRnMsyqNxIeqNIVa3cVrudVuPXdd0QDP6LaQ3cPSHhF7Ig9y7eBNEz5nXxyAWRAdrFOWC7eGvk1Zgv8Grul2HaWTPDCYs9aCf96aIU0j8MIMAKMACOgCwRc0XESwWAYEhKii/mwEeojEHUrGh7f9QY7XyVATXvhBu15C+LSHZNJNjrlKjy9ewo7X8WsM0U8R5/8Ej48ughjWra5ESFegdC8BPbDYoY0nHqkUmeYXvc5KOseaFtEq4RIWZHxMErWMqB4hsYifRQcsA05BQNwV0aAEWAEGAFGQG8ItG0vbS+CJfvrIBU9i/MjsCs+EvrvfRNoLwxLyQhQlOSpPVPhQvLlkhvrqMUhLET8KEZ3yH6WkhGglMTvL6wpsSHtC6zjI6Zkx+NVusG4Gk9AgOvtfWglDm6hga+rFyxtOQX3loUWOds6pF6RY2odYAdMLWRZLyPACDACjIBuEejUqZMi27p07QqBgXgnlsWpEVh9aRMMiJwBqfnI6sZiMwIXs6/D0AMz4EqqGRW5zRq0bbgxZhc8cWAa7jFK13ZgBx8tEot0lyQ+bl4wu9nokpoVe/58WgycTr0Es6J+gGmnl0JqHtYHs1UwskX7z+oEVIbnKveFQ92XQtfQZpCL7Ijmcm/5luaHVHvPe8BUg5YVMwKMACPACOgVgWbNmkH7Dh1g186dskx8duhQWf24k+Mg8OP5P2Fy1DeOY7DOLCUnbNC+afC/1tOgkn95nVl315w/L2+Bl098fvcAv7IdARvCODn5ORgDQy9IolzLvIFO12U4evMszDn7C+S65EnUcJulsUuZZtCxTGN4rsaDd/pno015SMZkLn7/MTyaH1fjPTtgaqDKOhkBRoARYAR0jYALcihPfPVVeGHkSEi4VpQyuTjjx40fDzVr1iyuCZ9zcASWnV0NbyEDHosyBMgJG3nwA/hf23eRmEN/EeNVFzfCuFPzlU2yNPcmLvoS9lqNr/kEuFI7G2V93B7YffM4HL4RBYeSo9CLwr62dy80CpnWLKBmIeeLGmy9FgnpeWZRbWysJbMpO2CFlorfMAKMACPACJQWBMLCwmDO3Lnw2sSJEBNj276Pl0aNgkf69SstEJXKeS6/sJadL4ErT3uqxh+aBV+1ngLemI6mF9lwdYcuna863uFQ1TsMyngEgo+bNz68wMPFDdIxDZbKICTlJENi9i04gzXoEizQvesFX6Mdg6reV2IEbPXV7fBXwi50gPJh27UjkFmATIfkdNng4BnGobYWHMECPDjnwq+QVpAJk+o9YzQJdt84BhnkgJk4du4Fbgac7zRS+QU7YCoDzOoZAUaAEWAE9ItAlSpVYMHChbBk8WL4beVKq4ZWr1EDxowdCy1atLDahk84PgJrL2+F188s0c1E2vrXgMaBNaGGX0Uo7x0Kod7Bhjpb3kgk4OPujdent68gqX5RWk4GpOamQXJOOsRnJGIx3Hg4lRoN25GZMEXKnhkVZr/51kl47+gCeKfpyxjQsCFvTQUbTFXuiD8EI4/NNT2k+WsfVw/oFtwIWgc3gDpB1aCqXwUoh8QQ7q5uNtuSjtTsV9Pi4ELqVTiGqXrbk47CIRv2Zdk8gMKGFb3CICsvB/zNvI08/LwmZSfD8AMfQgY6llcyEgwOZqHhLDhUhc7jG/r8R3iFQs+yLWBtwl6Iz04yb2Jwsr6OXgNhSIU/vMbDhvPHUy8Wcr7IEQt28wMPVzNDi2oTdkS7kYSZzIoYAUaAEWAEGAFxCAQFBQGlFQ4aPBgOHjwIZ6OiICkpCXz9/CC8XDlo1rw51KtXD9zcbL8wEmcda9IKgf0Jx5Be+wuthrM4TnO/qtAnrB20Cm2IxW6rga+UPSlWiOHIObuI9OqHbpyCf67tgbVJhy2OrfbB7+O3QYuL9aF/tZ5qD1Ws/tO3LsCIIzOLbaPWyQCMaA0o3wW6hbeFxiF10IlWFhH0RSe8Nn5O6NGrYkcYj4bHpyfCvsRjsCZuG6xPOqLWVGzSO7PJKCj7X024q+hk0e2Cd09/C9uuH4GCggJMA5RApvHfiCEe/oZIanmPEJje8Dmo5V8ZPNGZbR/XGN46tdiqE/b+mf9BItbye6P+YMh3Md//5QLPVL4Pwr3L2DQvEY3YAROBIutgBBgBRoARcHgEKCWxd+/ehofDT4YnIAmB6JQrMPzwx5CLzorWUh3v4A+q2BO6lW8L1QIqCh+eIk7VAysbHuT8UEFpYv379vI6OJZxRfh4xSmcdvY76F6hHQR5BRTXTLVz1zKuwwjck5aBJAxaCkUyn636IHSt0Fr1NMxw31BDkWQqlByTFg+/X/4XFl1ZqznDoztyD55OiTakFR65fgY+Ov+Tacaf7fBjJIzIMZqG1ASPfDeYXH8I1PGvUqR/DyzWTM7eK0fmQizuPTRPSSxA729B9O9QEaOMlMpZWAqgZmAltI9cRG2EHTBtcOZRGAFGgBFgBBgBRkCHCKRiyt6YyNlwKy9DU+s6BNSC56v3h47hLSSlnSk1ksgwHqve2xCJ2ompeJ+fWw57Us8rVWtTf0qF/Cl6LYys+4RN7UU2ysZUuFcj58BlS2lqIgcy0UVr/HLNJ6FduaZ30kVNTqv+MsIvHF6s9xQMrvkg/IaEI3OiV0CSRlT7OUjzPu3k0tvzRr/GZtfmv4ZueS7wMBZfDvb0g9ZB9eG+8u2KxYuIPlqWqQdzGr8C445+ZtEJI0KPqaeW3LbFzCAvjfcnsgNW7HLySUaAEWAEGAFGgBFwVgQoDYr2Jh1Jv6zZFFv7V4dxtQbY7aLcOFGKjN2DdY/ahzeDdVe2w/Qz32hC6rDs6t8wvM6j4IbEElrKvJP/g624H04LqepZFqbWfQburdBWF3ve/D38YHCth+DByl1h6dlVMO/KH8JgoL8h415EU6UuxF5oFBv2cxmaYrs+YW3hgYr3GPZsySmM3LZsQ5heb5jBCUsDTHE0G/u/bZNGy24/Y5sCV7OGhVsIf8cOmHBIWSEjwAgwAowAI8AIOAICVGj55wR5teCkzq+Mmy9MrzMU+lTupLnzUZyt5AjdX7kLtAtrCh+fWArLVcbjas5NOHnzPDQKqV2cWULPbY87AF9c/UuoTmvKRld6AEbUeRz8PXytNbHb8WCMfo5rOAT6VuoEbx2bD3uVRj4F+Cy30/5uK/Jw8YC4zOvwT/w+CHDzQUbEQ0CRS6zaBRW8ykA1JKO5hufpM1s3sAr6VgVQAVMKy3uXRbKPbIMjSPvBemI64myXV2DKsQWQmJdcxAkzXwByIillUkthB0xLtHksRoARYAQYAUaAEdAFAtFITDHpzGJNbHkstB28gYQBZZHFUK9Ctn3YfCy0jm4Ak6KWqLof7mzyJc0csMSMGzD+uPqFlit6BGP622hoHdZYr0t8x666QdVhWbv3YOGZX2HWpZV3jtvjBTlRRsmBXDiUctbwMBy7ewqdLlcDS6Fx/5aXm6ehiQced3dzh/z/Ciu7uboaHDGXAlekms8GN3zOw51oVgUDdWFuQRCKtP9aCjtgWqLNYzECjAAjwAgwAoyA3RHIK8iDaRgBUJuMwR0vDmfVfR4eqtpN0w3+cgGmVDLaH0ZkIMMjP1ZtX9y5FG1SPuni/sMTS1RPrewW1BA+ajYGywSEyIVe836ebh4wqv7TSG5RB14+9qlqay1lYqbOmKGfSRYjOVF5+Vgf7D/JNd2zmWs8KuMZP/OPlL8HGgbVkNFZfhf7F2OQbzv3ZAQYAUaAEWAEGAFGQDICv1/cBFuST0nuJ6VDZc8QWN1mBjxctbtDOF+mc2sV2gh+af0OhLurw1aYanrxbDqw4NebY/fBisQ9grUWVjekfFdDkWlHcr5MZ0D7AFe0fg+IjVOWFBNckqXPDp3cJNReE2UeO2CikGQ9jAAjwAgwAowAI6B7BCgl7e2ob1W1s6lvZfi5zXtQP7iWquOoqZxqS33fapoqTpi7BgQcqTlp8ObJr9WECF6s2AfebvIiUDTJkaVmUBX4qS1+Xn0qSJuGMUXQJFJloBi09J6OWXpIG1F8a9z/FWCH/XrsgIlfStbICDACjAAjwAgwAjpFYH7UL5Can6WadS39qsHSNm9DBb9yqo2hlWK6MFfDCavsG676FL47txpicm6pNs4rSLbxasOhumA5FDHJcj5l4ZvWb0tzwtCLMLAdGh0xMiQHQ2L0MB7LxRf4viAbHzkFUNWjHFT0CIXKnmGGvVdBLr7gikW6iAjD9FFkTpacuiKNZB4wZWyUqUJqN94DJhUxbs8IMAKMACPACDACDonAqZvnYHHsP6rZ3sinEixs/SaEYEFYZxFywr5pOQUe3/e2MMe1FVKFqymXU2Ph00u/qzbE4PCuMLbBYIv066oNqoFicsK+bjEZntz7pu3Oq6ljRDa6m8V23G6HvVz+c8hq+oTB+81Go+PqAnlY+DwfHS/ak0mvaQ+icR8YHjYIvb+UFoc+XB6U9y0D17GQeHz6dUOfbYlHkCwmDxoEVAOq53c9JxlSctMhEZk2M7HYdlpupoG841ZOKmQhwYdRDI4eviHdHYIbwqDKvY2nNHtmB0wzqHkgRoARYAQYAUaAEbAnAvPO/Kja8MSCt7DVFKdyvoxg1QuuCUuavQ4DDr2nmB2xrX8NTM2saVStyvPic79BNl6YqyH3BjWAKY1HOE3kyxyjSv7lYUHz1+HRfVPFYvifo7bp1nG4nBoDtM/QVqnhF2Gx6YBq2jtOFg2RcdDMTZWhgbswAowAI8AIMAKMACOgcwQir5+CtUmHVbHS28UdFrZ4He/QyyQyUMUqsUqJXn1+o3GKlb5Rb6iqkaNzty7Ct3H/KrbTkgJysj9uNhaMFOiW2jjDMarR9km9F1Sbypwz3xvSDVUbwAEUcwTMARaJTRSPANWLuHXrluGRloqh6exsyMzMNOwP9fH1BQ8PD/D18YEyZcpAYFCQqj8W4menH42E882bNyE5ORlSEefsrCzIxAeJD+LriTgHIb5BwcEQEKAO25Z+0LCvJbwW9sXf0UbPy8uDlJQUw/dibu7t1B0vLy9wc3Mz/K3Sd6Sjyfxzv6hm8kf1Rjg04YatwHSPaAezc0bC+FMLbO1SqN37tZ+FZmXrFTom+s2S8+qlHs5rMs6hqOaVYHt/lS6wL+k4fBe3WYkai313Yq2v3QmHoX25ZhbPl4aD7ICVhlUu5XOkC4mLFy/CqVOnIOrMGcPzmdOnIScnxyZkPD09oWatWtCwUSOoVbOm4XVNfHZ35z8fUwAJzwsXLsAZxJjwJbzPRkWB8eLNtK2l16FhYdC4cWNohDjXq18f6tatC4Q9i3QEeC2kY1aae9DfKH1HnjxxAk6cPGn4njx39myxf7vlwsOhSpUqhkfdevWAvhOrVq0K5KTpUWjv1/qkI6qYNjC8EzxY5V5VdOtRab+qPSDYMxDGYXHjWzbSyRvroRElv5pyJTUOfri2TZUhxlV+GFqENlBFtx6VuuAt6Yn1n4UtN47Cxezrwk1ceuF3dsCEoypBIV0cHz58GHbv3g2n8Ys/Ge+40UUXXYR16twZmjZtqvvoA83hBP5wRUZGwoXz5w13/OnufkREBDRo0ABat2kD/v7+ElCxT9P4+HjYuHEjHDxwAK5fv25wMKpVrw6tWrWCdm3bGqIU9rFM+qhZGGU5dOgQ7Ni+HTb/+68h0iVdy+0e2RgdowsTehjFz88POnfpAm3btTPgExiobQV1ox32fk5PT4cD+/fDdsR565YtkJaWJtukxIQE+HfTJsODlNDfUNd774WuXbtC8+bNwRvfs1hHgNfCOja2nqG/8S34Oabvjvi4OPDFaHjtOnWgQ8eO0Bl/j+gzaQ+h35g9e/bAtq1bDb81tzCqTJF5+n3sgn8fLVu2lPw7Sd+RR44cge3btsE///wDKRilliLX8PeCHvv37bvTzdvb2/A32/Geewzfi/Q9qRf5PvovVUwp7xEIE/AilS5WS5PcW6ENbAieA0twr9XimL+L3RdGe6beQIzqBFVXHaKfoteqMkZt73Iwos7jqujWs9IATz+Y0WAkDIp8X7iZf988BseToqAhpjuWRnFBJpD/eEa0n/5JdLhmzZxpuNtmbXS68Hpj8mSoUEFibQJrCgUfP3r0KHw6d67hjr811fQjPuy55+DRxx4zpG9Ya2ev43S3/H/LlsHSJUusmkAXHs+/8AL069cPXF31u3WQHOC169bB6lWrgC5ItRBKyenRsyf06NEDWrVurcs1FokDfWWcwr9dwvnPP/6wOZKoxAa6kHvw4YehT58+UB1vCtgiV65cgQFPPWVL0yJtKOK59JtvihzX2wFeCzErQunHX375JaxaudKqwooVK8K06dOBoj1aCjk57737ruEGn7VxO6KD+MakSTbdJLt69Sr8vWEDrPj1V0U3pqzZYjxOv3v0m9f3/vuBsLOnJGDdr3ZbXwAkwRZuxsJG46FHxfbC9TqSwiRkpTt0/QTsv3ECLmXEwS2svxXmFQz1/KtCh3LN8QK7liYOanJ2KrTb8jxkIPudaPmh2RRoH1560+VeOzgbfknYJRpWeLZ8N3i76YvC9TqCQrs5YEcw6jVm9GigO3slSUhICCxYuBDKly9fUlNNz9Od0qlTptg8Zs9evWAytqcLdr0I7Qt5f8YM2LB+vU0mPfX00/DSqFE2tdWqEV2EUhT1px9/hJ07dmg1rMVx6ELj6QEDoDs6Y3q6+2vRWIkH6bOyd+9e+OF//yv2YlCiWsnNKTI+ZMiQEi+EndkB47WQ/LGx2oGwfPutt2DL5s1W2xhPUHYG/RZRup0WQvsnR44YAbGxsSUOVwdTdud++qnVbIsrly/Dd999B+vWqhMhKM7AJ/FGyMBBgyAY93raQ/537g+YevY74UP3CkZSijZTJUcfhRvCCg0I/H5xI4w9NV84Gn1CmsIXbaZo4kQKN16QwkvIWnjvjrHCb2J4YkHuPV2+hmCv0pdF5DYNRdD62KyGUrrGjR1r2JhvSye6O3kZfzwoykA1AvQgdBdxLDqQ9ONtq5w/d85AONCgYUNbu6jejlJPli5ebPM4x44dM6S9VMD0Sj0IpQy9//77sGTRIsNnxN420ab1nTt3wurVq8EP7wBXr1HDKfaKUUrWjPfeg+8xUhqHqVn2lEu4V+UPxDcGL0rpQtha+icRf9BdfjlC5CuPPPKInK6q9+G1EAvxSox6/fzTTzYppRuGlGpOkVi1SSjoxhL9zR3H71xbhNLG6TNP0TBToXTF+V99ZYiincV9XfYQmsPq33+HsHLlDH+zWv6O52NtoQlH5kFSnviMiM+ajocwnzL2gJTHtIDA1GPzsXbVTQtnlB2a13QchHmX7nUO8gyA3Mw02JsSpQxMs955GJWu7RUODVQuS2A2rC7e2iWXbB/mjNtyR88Uod27dhmiHKbH7Pn6559/lpV6tQjvnhIbnB6ELiYWzJd+t4icC3tLAu4X+ujDD2Hk888b9iDZ2x7z8Wk/xexPPoGBGA3bhnss6GLKEYWcLYoOjHnlFTiK+0X0JOvxTj6lGC7BGwiZGRl6Mk0VW3gtxMNKDsvCBdLY3C5GR9ucMaDEYorq075KKUI3JogIh4S+c2jP2DMYLV65YoUUNaq0pf2hMzCVkjIutEoPp4mcRPKNc1kJwuf0dLl7VK9lJdxoJ1ZI1PP7025/9kVOs19oG17n/wB9unofcFVhr+OvMZtELpnD6LKLA0Y/LHKEfkz0IHSxt+4veRt6jRvl9TAPYqmj/QVShS4KbEkdlarXlvYUcaQUmiGYzvLnmjW2dLFrG8J3Cu7NmPb223Dt2jW72iJlcML5d9xHRzgTMYZehS4yv1m6FEZgmtZp/Dw7o/BaqLeq+5FARo4z8Oeff6pn1H+aN2J2ghwhQpwM/I2i/dVTcP/0jRs35KhRrc963DtKGTCJiYmqjWGqeGPsHtO3wl4Pqf6AMF2sSDkCW64dUK7EgoYBVftYOFo6D0X4hcPA8C7CJ7875RwQe2VpE7s4YMQuJUfIAdNDJOEUUmxTWqRc2YP7aPQgkZhWJkeIsphqaGktSUlJ8NbUqYY7qErY9rS2m8YjJ+bZZ54xsH3aY3wpY9KF0WR0Gj+ZNUvR51zKmErbUlTieXTCKDqrh+8IpfMx9ue1MCKhzjOlVMsRIqGh6JmasgeZgeXILkyBnjhhgiFNV05/LfpQ6jg5YZTJoKbQd8GKePGU5L1DmkC9UpgypeZaKdX9W6z4da7vUwFalG2g1DSn6t+/cjdV5rP92kFV9OpZqV0cMLmAUAoO7b2ytxCBiBLZgSlpUvaOKRmruL6U1ukoQjT/zw0bJjklR0/zS8X9Ya9NnAjfILuevSKIJeFBrJ4jhg+3O5lJSXZaOk8XW7M+/hi++Pxz3eJryW5rx3gtrCEj7niCgqi0mjeh6PtB7l7LY/g3rLd0YUsrRjdNJr3xhqHYs6XzIo6dT74Ml7LFRwAHVb1fhHmsQxACFD05lnFFkLa7ap6phCl3Lg51mXzXeJVeNS1TD5r6VhaufVPCPuE69a7QLp+scAVshkRdb2/ZpzCCRZEc+vGxp9A+NLmpoFSA2BrxgRpzIsrkUS++CFQnyhmECEM+QOIQIqPRk1Bq0MsvvQTXNUoNUmvuy3F/5ie4/y4PI7WOKrwW2qwckebIFUrzU0v09t2g1jypYPtMvGmi1g2p/dePCzed6n61DWsiXC8rlI/AQaS/V0N6VSxMaKPGGI6mkwh0BlbqLdzsjTePA5URKE3ibo/J1rCxjo8l26hIcE9kQ7SXUNqJXMfF1OZjx48bGPJMj2n5mvZ/yRVicSQnTG2hiMb3SHn+tcRN8mrbJUI/0f6noRM87Z13wMvLS4RK2ToI52XIbrjo669l69BbxzWYiii1sKwe5sBroYdVYBu0RGDzv/9CCywkrQbr6K4b4omDBlXoDh6u6v/+abkGjj7Wruvi15nSTENKITW6LZ+FDuWwHtoZW1ra3oZq9B3Doswdwpvb3snBW9olAlZDQQ0VqvNkz/S903jHToTsRyZIe4qSFJVGjRqpbjpdiC7GSJEzOl9G8HbgZ/njjz5S7e6vcZzino04O5PzZZyvLXWdjG318MxroYdVYBvsgcBnWL+M6vaJlNz8PNh0U94ev+Ls6BTesrjTfE5jBOh7818VHO2e5dpqPBPHGa4iknG09Ksm3OBjN8VS3As3ULBCuzhgVatWlV3Py5C+h3WA7CVHBFFx0/4re6aZ7FKw/6tu3bqqwk9fqN/iPqnvvv1W1XH0oJzSK2lPmD2kNOFsD3yljMlrIQUtbutsCOTk5MDnn30mlEDnYspVSMmTT5ZlCeMybr7QMKSWpVN8zE4IXEmLg/hc+anE1sxuF9bU2ik+jgj0LddeOA57ktRJJRVuqCCFdomjU8oVpbHZWmDSfK7EoFRdQRqjuT4p70URV9D+gXNYmLl+/fpShhfS9iYW5iQWL7lCxYXVFKpZQ7WdtBBfLJZctVo18PHxMRTJpjGJ4ZIwikHCFzU32hvn9y3SqDdp0gRat25tPKTJ82+//aYpzpWrVAE/Pz8ICAgAV1dXw+Z7ogCPR6p+R993pnTBeC2UIsj9RSDQELMb6DcpIiIC/P39wQ1TzTPwbzQB94Wex98r2v+shAG4OBspu4XS+5s1w/QmAXIu9bIALYVV9A1tBW4uboUP8ju7InAuRfw61/EOB4rysFhHoFVoQwDBZdd2Jp8Gily7u5aOvzG7OGC0pHTBKdcBo9otfe+/3/onQ6UzFH0TWWuI0gDt4YApITLx8PCASpUqqYQwAKXlfTp3rir63dzcoEPHjob9Bg3wIqN8hQoQHBxcbDSWNulT0fCzZ88CsYsRnbwaFPhUnHQpRsLIHi2EqKrnzp6tylC0P7B9hw7QslUrw+c7PDwcQkJCisWZSGHIEaOLvIMHDxpYGOnvrTQIr0VpWGX9zpG+E+/r0wdatGhRIrkSZW0cx/3Lf//9N/z5xx9CI1aE0FK88TZ33rxivytsRfJ8itiURhq3VQhTktuKv1bt1HDAOoagc8FSLAK1AqsUe17OycyCXIhJj4cq/hFyujtcH7s5YHXq1JENFl2wEGsSXVBrKfTDI1L24t3EJ558UqRKm3QpodFvjj/SahFwXMTU0ulYsFi0ULT14UcegQ7t20OQRAeHojX0oM9r3759Ydz48YYLkE3oiBHRA9VEEyEUAVowfz68jrTMagvhTIWhRUujxo1v44zOF2EmRehuOz1q4v7Qnr16GXCNjIyE9UhWsgHZGSlFzxmF18IZV9Ux5tQDyawGDR4MNSRkNHh6ekLz5s0NjyFDhsDSJUtg7V9/CZvwIaxNGRUVZfi+Var0VGq0UhVF+tcNqlbkmJ4OXMu4DqduXYDrWTfhFjLK5eIFrdZCtO3+7r5QzrsM1A+qAeG+oaqacDpV/JaUJkHyr09VnayOlPu6+0Bb/xqwJ/W8UKsuYUopO2BCIS2qTAkRB0UgKH1PiRNX1KKSj9CdeZFC6RyUgkVpcFqKkv1fDdGZUUMoJfPtt94Smt7SGKOszw4dCq0wEkPUqSKEIoCUIkOPZ599FtasWQPfYeRKxH6+P1EXOYr16tUTYapFHZmI8/Rp00AkhTbhPOy55wx30EXhTE4+rRs9nkPdxIb5+6pVFufkqAd5LRx15Rzb7ooVK8IbkydD06bK9riUx3Iyk1APOXLv4HeKqHRtYogV8dt+Jv2q8IWq6q9e9odcY/MK8mBjzG5YHL0a9gq+GJZrk2m/HsENYUSNR6FNWGPTw8Jen04TH+nUu6MtDDyFiloH1RfugF1Ni1doleN0twsJB8FDaWxKIilUmFdLoTvw27duFTok6VRCBy/HmASspUVpXnKltoLIZXFj0p4vJXaZ6vbH6MuUN9+Ez7AgL+2rEuUUmI5Br8uUKQN0F/iHH3+Ezl26mJ+W9f4n1KWmLMH9ZmfxDrMIIZynYiTt8y++gJZII60WznShNwELWH/73XeKLxpFzFuUDl4LUUiyHlsR6NW7NyzCqJVS58t0PPqOXYw6a9aqZXpY9us/MLNA6T4z2kdyOiNOtg2WOjbwiQAfd/uWDDG3KzbtGjy3exqMPDZXl84X2fsP1nd68uA78M6RryAjVywpSn5BPpzKiDGHRfH7ir68/8sWEGsFiC/IHJNxzZahnaKN3RwwQyQBUxnkitY07kSRGxcn9gud5q6EDl4OdkodPinpKrbaRymRP//0k63Ni23XCi8GvsOaVr3vu89A9FBsY0Eny+Eep3fefRdewCLGSmXTxo0QEyP+B4XsIgbPn374QamJhv4dcd/IMoxKUU0+tRwvc0OJ/IX2h4weM8b8lMO957VwuCVzeIOfwWyAyVOmGIhwRE+GvgPn4N7dWrVrK1ZN0Xkl+5TJgBuYgkd1hURKfT/xF5tK7LuQfBn67X4DtiTLr+mpZHypfZfGboLxB2cJdcKSsm5BNkYARUqQmw8EevqLVOm0usJ9xKeXXslkB0yTD4wStiOtadyPHRNfT4RAVpIOKGeRKMderoSGhUEFJK4QKZS6N3PmTCEqBwwcCB99/DGEhor/UijJQGL1GzBgALw+aVJJTUs8T0QkooVwniUIZ9o38i6ShpQtW1a0mSXqo32fjz3+OMxDyuqgoKAS2+uxAa+FHlfFuW2iVOxhw4apelOKCIQ++PBDQ2aAUjQPINGWEknB/U+iJcI7TLRK2fpSstNgxMEPVKFfl22UDR3XJR2Gj48vtaGlbU1on5toaehbUbRKp9VH+/xES1zWDdEqdavPbhEwQoQob+UKXcQQM51WQvu11JATSOxBlOdayS4FF/dt27YVHu34A5m0LkZHK57+yBdeAHpQZNWecj+ycw7GtEQlsmP7diXdLfalvWrRFy5YPCfl4CujR8PzI0cqSh+WMp61ts0wev7FV18BpSc6mvBaONqKOba9fZA8iBwwLSLVxHj6lgCCH6U3JlNyM4QvWphXiHCdchUuiloB57IS5Ha3a79v4jbB7muHhdiQmpsuRI+pkmo+Ym8ym+p2tteh3uL/JuKzbzkbTFbnY1cHrLbCdAW5NPZW0bBygopEqnFRbBxOq/1ssZjadhVrW8kVkfsGyIbk5GRYuGCBXHPu9Bv54oswcNAgTS4w7gxazItnkJxDCcnMISR7IYIGUUI4f40Mi0rl5VdegcefeEKpGmH9q2Bdsdlz5mhG3S/CcF4LESiyDlsRoL+RMWPHqhr5MrelBe4H7f/oo+aHJb2POnPGQFAlqZNJ45ScNJN3Yl4GeviJUaRQy63sFJh/VRzzpEJzZHX/8twvsvqZd0rJEe+AlfEINB+G31tBwN/D18oZ+YdjcrQLSMi3UkxPuzpggYGBhoLMcqdCNO5aCNHiimSNM7c5UkFaoLmu4t4rdfTqCmbn+23lSkU/sjTXBx58EJ5++unipq35OaJqJlZAuULkLFcUOMrm467CgsvEtqlE+vXvryvnyziXSpUrw4wPPtCN8220y9ozr4U1ZPi4GggQ26HWLLs0jyHPPAP0PahELl++LLs7kTOIFn+dOGD7E48L3/ckGquS9G3DgrtXUpXvqc/NF0+z7+lq3yyakrDT03kqOeDt4i7UpIz8HKH69KzMrg4YAUM003Jl7549qhTFNbdHyb4pc12W3lNNKS3qHB04cMDS8DYdC0BnuWrVqja1taURFTj+4fvvbWlqtU09LKas9d1dq8aYnaB0TSUsnzduiMmDFoEz0cy/NGqUbp2cxlh/bPyECWYroL+3vBb6WxNntoiKKzdSkOavBBtiiKU9uUpEiQOWpkIKoruLm5LpCOt7Oll5KrkwYxQoOn5T+RaS9DyxrIo0Hb0xXSqAWJOuwVjzTbRk5mWJVqlLfXZ3wJTUlSKnRSlbki2rIjX9UOqF97X4eLh06ZItpshuQ4Wrt2zZIrs/ORRENCFKNmzYoCiqSGQMVLTYy0tftMBGfMgucgzkirCaOoizkugXfZb1jLMR3wcfegja4GdUz0KfeV4LPa+Qc9n25FNP2XVCRHmvRBKxZIpcKRDMgCjXDjX6JSDDozNIbEai4mmoEen0QxZEFtsRKOMunjEyH6/tS4OIu6KWiZbStDbaL6OmUCTi2NGjkoaQ88N3ODJS0hhSG184fx5ScC+QXCHSA1FCzuCvy5crUvfc8OFQs2ZNRTrU7kxRQ7lSkK88hUYEzkS4QftI9C50c2Dc+PGKoo5qzpHXQk10Wbc5AsQwbO/vR6r1SdFzuXJNgQMmd0xH6Ofq4uIIZpZoY26B+PTBEge1oUGOTu2ywXS7NMnIz7bLuM4wqN0dMEpVqFqtmmws1aDsNjWG6vVIlR5YG0lqUcpdO3dKHUZS+yMSnUhz5fUE7v+KRGdTCRkIfWZoT5LeJR4jm3LFRUC08TDWV1OCc1mk83/4kUfkTkHzfhUrVoSnsRSAHoXXQo+r4rw2deveXReTa9e+vWw74mJjZff1dfOW3ddaRzUY96yNVdzxcC/x1N/Fjafnc95uyvYZWppbdinag2Rp/lKPqeGAlZZ9eHZ3wGixqairXDl/7hxcu3ZNbvcS+0kl+qCoRzV0KNtL/OHZvXu3qvvZqG6aXPHz84Pq1avL7V6kn5JUSFI2/PnnVSkmWsRQBQduYWmBM6dPy9YgYuP85s2bZY9PHUeMGAE+Po6VjkE1wpRu/lcEmpXOvBZWgOHDqiBQt25dVfRKVVpLQZaCknRdIgcQLTkqED7IsbF+kL4zP+TMSW4fd8EEEGSHGgyacufnCP3icuRnVlmbn7urPvZbWrNP1HHx31IyLGveooWMXne7HFUY3bmrqfCr3Nxc2Pzvv4UPlvCuU6dOQPuTpFK2U4oS1QRTQ+iHTEkds06dOwurr0X12zasWyd7mv4BAdCtWzfZ/bXq+OdffykiVgnwV5ZXLQRnndxFl7JmISEh0P+xx6R0Ub0tr4XqEPMAZghEYDRYD6KkTp8S5mE16LHTVSD2kLNGLUMbQoAKET45tti7j4+7+EinXhxte2Nry/i5+Xm2NJPUJtRdH+UeJBkts7EuHDAi4lBSJFIJu19xuJ3DQs+pyNYnRYzOpJy9bfv375cylM1tiaiEHDy50rp1a7ldi/Q7hbYoubP5OEY4RESHihgm8ADhrbS+WVhYmCKLTmP0TQnO/fr1A29v8T9uiiZlY+devXrZ2FKbZrwW2uDMo9xFgLIW9CA+vvIZ0ujGhVwJUKE+0bWsJLnmCO3ni07H2Kr9hOp0VGVqrHNcthgGYkfFVIrdN7PFR78iPEtPiq0uHDB/vNvfUgEd/TZk91PiYFj7wMmhnzeyOgYHB0uucbYFU8bUoKNXSlSiZCO1Oba0/0uJdOnaVUl31ftSpHE8Fj5V8nkk5sFQhQ6YUpz1sodEzoLVqlUL6jdoIKerKn14LfSzFqossA6VSmXiVWsKStKBqWi5XPFXgRr7Sob8Pb1y52Gt38AaD0Bb/xrWTpea4wEqREsupCuvT1ZaFiAxU7yzGu4ZVFrgA104YIR2hw4dZINOlN1nMVolWqTSz1O6BREBGIWo26VITEwMXLlyRUoXm9oq2XNVDfd+KUkjMTdQSbSyMhbdFbkXzdw2Je/j4uJgzuzZMAGZ+NLS0pSogtZt2ihO+VTidNN616jh2D/u9+ooTZXXopuivwfuXDoRyMMtAHKlrHeI3K5W+0XryAHzQvKJeS1eg+Z+4mpzWp24jk+EeMlnGrY2rVMZMarcdVprIQAALWpJREFUCLc2niMfT8gUXxIhwltZ9o8j4Sm2hLWCmTdp2lRBbwC6yBG58TgpKQmIuUyK3IP7v0xTKeVEjuhuOTkaooRY8C5GR8tWR3vaRAmllByRiKnp2MQuaYqv6TnT1xRFJGKWm0iEoWQfgalOS69TU1OBmLrIqZTqrFvSZzzWQuGeSMJZyUV/9x49bMLZaK8en1u1bKkLs3gtAPSyFrr4QLARmiBADkoVTGW6JDCd7ETGVaC6U2oQfMgBpZxPWfi2zXSYeWIpLIvfIkeFw/fxw1TTMKxDlZCbKmwumUhDn5R1C8p4BwvT6ayK4gTUcjPHhh0wc0Q0eE81S4hBUG6tKroAfurpp4VZKod+3rxWlhzqdmIrfPDBB4XNQ2l9MeOeNhEGxWKET0lqXoMS0sooCvXH6tXw55o1QPXbHFU6KGAFpTkTDkpwbtiokaNCd8fu6hjBoz1smZmZd47Z4wWvBYBe1sIe689j2g+Bur4RQh2wDKQnv5waC1UD7ma52G92t0cO8PSDd5q9DI/d6AnLL62H3xP2Qmp+lr3N0nT8Br6VYEvyKaFjXkyLZQfMBkRPpVywoZW0JhE+HAGThpiA1sQc2KVLF1jzxx+ytFG0ii66qUaUCJHDGmjuINDeNmJDlBJJ27ljh4E8QRTRxC4F9PO0JuZzUoLtVXTAlIi12moU8SLH69O5cyEnJ0fJEHbvSwVUlUZAldT+IgBqOnj6Ic2BPrt080Dt+no0VnHCa6GftShunfic8yFQ378a/H3zmNCJnU25pCsHzDi5JmXqAj3eRqr86NQrcCMzGTLzxDpiexKPwFcx8hmMjbaKfq4XUFW4A3bq1nloXra+aFOdTt+em2IdXwKoin8Fp8PJ2oR0k4JIBhLbnlwHjPpT1Kpr1670UpFQ9IAIMaQIOQehWLjWXNq0ayfJAaOxT5w4Aa0UkJIYbaD0OyWpcW3RdlGOINmkpF4bsXpZwpf0LvvuO1i0cCG9dHgZPGSI4jkkKKiLR1GjcNwD5gxCEWh7O2C8Frc/SXpYC2f4TPMcbEegVkAV2xvb2PLEzfPQPaK9ja21b+bh6g61A6sBiN8aBYZC1MruoaoCSG1/FdY5+bwqtjqT0uTsVDiOabmipbJf6XHAdEPCQYtonsIndWGlFk22pv8cFncmYg8pYq2YdOPGjaWoMbQ9iHuKRMhJdOSolplcoT1tIiUxIUG2OmK2syQ0R2dxvihaqoQN1IjPNQU4U7qYq6uuvhaM05L8XL6C/b/IeS1uL5se1kLyB4g7ODQC1VVIFdxz87hDY+KMxtcIELdn3ojPtqTjTMRhBMPKc1TyRStn5B+mfZtqEKvIt0jdnrq60qIiqkouQKloshKHwwi1HAIDayQiderUkUxoIIqOXmldMRFROCOm9EykGHLFWmHRtQqKOsu1RY1+lDI3fuJEIc6PEvpmpemPamAjV6fSWmpyxzXtx2txGw09rIXpuvBr50egVmBVcHcRe4mzIyUKEjP1UQ/M+VfQthmqEem8mH0dzidfts2AUtpqd8IR4TPvHOL4+8+lgCL220nKyFbaKkkhpKLJp04pz0mVQ9tuba8UpfARrbgUuXz5Mly6dElKlyJtaV/Upo0bixy39UC9+vWF0s/TuCkSi1qb2hqEddUsiZIUS0v67HVsHNLXi6LYv6XA0RW1h9JeOJqOG4ikPvYWXovbK6CHtbD3Z4HH1xYBbzcv6BhQR/ig+xLF7isTbmApU0hEJG1UqIm29/rRUoaktOn+lbBbWgcbWjcPrmdDK+dpojsHTEkEjJZl/759ilaH9ikdOyrtD49sJsINa9JGogNGepRGr86fPw9UV0yudFOhjpISggxLBT1pv5ySPTZysRHd74knn4QHH3pImNr8/HzZujw8PGT31VtHS58ZrW3ktbiNuB7WQuu15/Hsj0CHMk2EG7Et4aBwnaxQGQIdQ6Rv9ShpxE0Jyq4lS9LvyOcvpcbACayXJloahVjeaiJ6HL3o050DVqlSJaiBlPRy5Z+//1aUuytn/1U7JKsoTuTUA9u6RVldD6WOqNSoXXHzN55LT083vpT87IPkEOZCaXtEGuHIQqUTXho1SnKaanFzVhJpFEm6UpyNWpxzd5fPMSQqYsNrcXul9bAWWnzmeAx9IdA2TLwDtipxD6TmyP8t0xdCzmGNGuv8D+73S8y44RwACZ7FjmuHBGsEKO8RCJQ2XJpEdw4Ygd8DC8HKFUrdU5K+t3XrVslDE3V4cUI1znx8fIprUuQc7UNTUsvqX9wPJ1fKIwueqHQ4UxuUXNRac97q1K1rOoTDvKaC0mMx7fDFl14Ssu/LdOLFRWNN21l6nWHnulmWbJJ7LE8BAY0tBb9tsYvX4jZKelgLW9aL2zgXAg2Ca0CAm9ibdFQPbHt86YuC6dnpbBxSBzxd3IR/eDfF7RWu09EVFkAB/HxV/vYWa/N/ILStboqcW7NR9HFdOmBKoy9yolgELN2tlrqniOjRS4rYUfpN+w4dJK/dQXTC5Mi1+Hg4cVw+W1P3nj2FOwU0DyXsejlWLqZ7oK2OJtWqV4f5CxZA//79hUa+jDhQZFCu5Dp4HTXTeWdlZ5u+lfRayc0C04F4LW6joYe1MF0Xfl06EPBw9YD+YeJp43+8vK50AOggs/R194aHyrYWbu2yy+shv0B+Sr9wg3Sg8OiNM3A4XTxBSedyLXUwO21N0KUDRpTjoWHyq2Fv3rxZFopHsJgzkVdIkXs6dwZb9s3I2dsm1Rk02n1ApuNm7F9SSqWxndTnoKAgqV3utLdGZtCrVy/FhYvvDKLyC0rvG/Xyy7Bo8WKo36CBaqMpcR6URF1Vm5BMxUTKI1eCrZC+SNXHa3EbMT2shdS14/bOgUDv8tJvfpY0863Jp+HUzXMlNePzGiLQq7x4R/tYxhXYnyj/ZraG09dsqF8v/S18LD9XT2gV2lC4Xr0r1KUDRneNeyqIbMhN39u5c6fk9bKVqr1RI+n0mrQPjIopS5VtMtIojWMEIHOcNUZHYxu5z0ocsFgrhCLk1Hw0cyZERETINUv1fhUrVoRXxoyBX1asgCefegrUJiRQA2fVQVJhgAQF9dBEsUHyWtxeWD2shQofMVbpAAi0CG0AQW7StgDYMq3vLqyxpRm30QiBDuWagbeL/H2/1sz85sLv1k6VuuNXUuNgWbwyfgJLoD0V3hl8MIpZ2kSXDhgtQucuXRStxT6JbIiZ6Oj8vWGD5DGpeK4tUrVqVQgrV86WpnfaEGvgYYzKSRGKFMmNnNE4ffr0sSmiJ8UmY9tyEudv7EfP0dHRpm8LvSbilkVLlgCxCYrau1NoABlvamP9t6HDhsGX8+fD9z/+CI8//jgEBATI0CS9i5Lo8cWL4osrSp+BmB7xmIorV6T+rVobh9fiNjJ6WAtra8THnRsBLzdPeKZCd+GT/PHadjh365JwvaxQHgJ+Hr5AF/KiZW3SYTh847RotQ6p79sLq1Wx+76Ijqro1btS8bcLBM24Xr16hjTERJl3sf/dtAl69+5tszWR6OhkSiQgqImpkkRYYYvQ/qd7770Xlv/8sy3N77ShKJiUlMDde/ZITqO8Mxi+UOr4muoyf63kopaKOJNzaa0eGJEdvPzKKzBg4EAg5/v06dMGinolLHTm9pu+J0eP0stoXamAeNmyZQ2fVypkTA6hktQz03HkvFbi6BLOiYmJEBoaKmdoXfVRUhMwPDxcyFx4LW7DqIe1ELKgrMQhEXi4cjeYd0V8xOrzqJ9gTqvXHBITqUan5UnPxpE6htL2/XGdv4nbpFRNkf6fnvkBFredppsbvEUM1ODABSxMvShGfPphA58IaFFWvS0ZGkAjewjdOmCUhti3b1/47ttvZU1u544dkJSUZLg4tkXBFhn7xsihkiKtW7eW7IARrf7o0aPB20YWRXI85Qo5EmqlH5JNlIqnRC5cuADNmjcvVgWljpHjLcX5LlahA55Umo5JUTBHd8Co/pZcMh5a8goVKghZeV4LAL2shZAFZSUOiUCNwMrQLaghbLoldj/Pquv74KlrR6BtOfF093oD2hHIKBohG2Iz3yoQmS42MvnvrROwKXYPdI8ovuSQ3tZMlD3EjTDzlLxr8ZJsGFLpvlLHfmjERLcpiGRgJyS4UCJ799pGIUoU5xvWr5c8VMuW0lhbGjVuDFJZ0SgqR9E5W+T69etAjqdc6fvAA6CkXk9J49JFrS2EJdb0RJ09a+0UHzdBgHBWkopJ0UNHl4uYsmqtdEFJc6M9eqIiYLwWAHpZi5LWnc87NwJDqz+kygSnn1oEGblZquhmpdIQoN+956s/Iq2Tja2nn14CKdlpNrZ2rmYbY3YDpWKKFioR0adSJ9FqHUafrh2wOriPhii75Yqt0SCie6f9VlKE6Oel1qCiPlLSCY322FqUec/u3cYusp6lRvSkDkLOZyuMAsoVJXvb5I7piP28vLygpNp0xc1LCYlLcXq1PCe3hAPZ2LxFC2E3IngtAPSyFlp+/ngs/SFAJA2NfCoJN+xkRiwsOLNcuF5WKA+BbhXaQmXPEHmdi+l1OTsJZp/8rpgWznnqemYSTD71tSqTG1mxLwR6+qui2xGU6toBo7sZDz0k/66VMQ2xpIXYLKNo8b3du8uK5nS4556SzClyntIQiSSkJNm4UX5xPNrPVrt27ZKGUHxeiWNAKWXORJOuGMxiFJSUqllMVzh+7BgoYa0rTrdW5+T8TRttU/IZNeowfea1kF8UXvRamK4Lvy5dCLi6uMLomo+rMulPL6+GPQlHVNHNSqUhQKQro6r2k9bJxta0v2wTRoNKi1Da6fSjCyAhN1X4lKlw9uPVegnX60gKde2AEZCdlaYhIilFcZKcnAwb//mnuCYWz7Vt08bi8ZIONm/WrKQmRc5TGuKhyMgix00PUPHlfTamXJr2M75+EB1dJWlrRj0lPTeVMX9TnUrmaKrH2V8ruegnbJSkstob20uXLklmDzW1uXETsfs5eC3kp66IXgvTdebXpQ+BezE60tBH2V5ka6iNOTIX4tITrJ3m4xoi8FCVblDFs4wqI4498QVEp1xVRbfelH579nf448YBVcwaXfkhKOdTVhXdjqJU9w5YOWQj69Cxo2w8//rzz2L77sLaX3l5ecW2sXRS7p3ZisiQV71GDUsqiz1WEkX+FmRLVCKdOmmTh0tRNqo1Jld+/fVXRSyPcsd1tH7169cHb2/5dTVWrVrlsDhvkFFOwri+hBmlPosUXgt5aKqxFvIs4V7OgoC7qxtMrD1AlenE56bA6EMzIS0nXRX9rNR2BHzcvWBcjSds7yChZUpeJryM63wrO0VCL8druiV2H7xz/gdVDPd39YKnqvdVRbcjKdW9A0ZgPvDgg7IxPXToEFy+fNlq/z/++MPqOWsnyCG0RodurY/xOEWZesgoMk1ROmJ1tCTEMrbqt98snbLp2L3dukFYWJhNbZU2IhIOYreUK6dPnYKTJ0/K7V5q+tHFq5zPmRGgc0h4IrUGnbGvPZ8pov3rcvn7Mbr36KHIcbU0d14LS6iUfEyNtSh5VG7h7Ah0Kd8a7g1qoMo096VegNcj50J2nrQ95aoYU8qV3l+5CzT1rawKCsczrsLYgzORfCVTFf32VnoE6569eGyOama8Vv0JKOsdrJp+R1HsEA5YG0z3Cw6Wv1ibrVDMEzvXERsZBk0XtEvXrqZvJb9u37695D5EA7p9+3aL/U6cOFGsk2mxk8nB+5H9UEtRip/c0gRazlEPYynF+dtvvnG4KNgfq1fLZj+kNVOrDh6vhfS/CLXWQrol3MOZEKCboK/Xf0a1Kf154xBMjvzUqZyw3Pw8OJ1yUTXM1FDs4eoOk+o9q4Zqg87Nt07C+IOznM4JO5F0FoYcnAEZ+ercRKjjHQ6PV7e9Rq9qC6gDxQ7hgBEtdP/HHpMN1++YTmUpzfBfGeQbZATV81IiNWvWlFVniC4uyREzFyLpkCuU4tkCWd+0lIYNG0INxECu0P6kyBL2xMnV7Uz9qEwCra9cObB/P+zHotaOIkQcosQ5DwoKAqmlJWzFhtfCVqRut1NzLaRZwq2dEYG6QTVgFDKwqSUrEvfAJIyEOUOEhFIqJ6Kj8Wui45FPtA1rAk+EdVBrmWEdUrOTE+Ys9PSR10/CgAPvwC0Vi26/Ve858HbzUm1NHEmxQzhgBGgPTA2SK0RQceRIYYYiop2Xk7ZHe7+UFqmlO3D39ekjeTqnMPXu/PnzhfqlpaVBSfvcCnUwe/Poo48Ko9w2U231Lc3/8SeU5WfP+/RTyMpynNorxN54DNkFDyCTIxU6prRRtYVquj32uDLWr09mzVIUUVJ7jkb9dGPii88/hwwb2EKNfcyf++PfAt3sUUN4LaShquZaSLOEWzsrAi/UfQKqeqpHArAycS+M2PsOEI23o0p8eiIM2zsdfr++31GnAK9itDPIzUc1+8kJI4xi066pNoYWitdf2Q6PH5imqvM1MLwTdAxvrsV0HGIMh3HAKiF5RUcZFO7GVVi3dq3xpeGZLoTlUJp3U+AImhogpx4Y9Tcn46BoELEkyhFyhHr26iWnq+I+VHNMCRnH2ago+OnHHxXbobaCuLg4eGf6dHgEWSZfeuEFGDdmDAweOBAe7d8ffsG9SraUF1Bi43333afIqYiJiYFvli5VYoImfSnNeJOCMgxkpJybIlImx2thO1pqr4XtlnBLZ0XA38MP3m8wUtXp7UiJgn67XgfaU+Nosj3uADywayLsTS1801fUPKgsgBYS6lMGZtR9TtWh9qddgId2vwZ7rhW+0a/qoIKUZ+Vlw+zj38ILxz+FXKSdV0vC3P1hXL0haql3SL3a/AUIguZRBWmI5ICZ1jZauWKFLKtoP5oIoSLOoTKIL8julJTb7Dt01//nn3+WbQ7t/VIazZM7uK+vLzz77LNyuxv6LV60yBBRUqRExc6nT5+GYUOHgqUU0euJifDZvHkwevRoiI2NVc0K2jv5xJNPKtJPju6mTZsU6VCzc3R0NHwwY4aiIfref7+stGApg/Ja2IaWFmthmyXcytkR6IB341+MuE/VaVIB34f3vQmLzvwKOfm5qo4lQjmlTX5y7BsYfPhDSMxNE6HSoo6KPuUsHlfjYN/KnVVNRSSbCaunDr0Lc08sw9RTx8jOOXfrEgze/SZ8dmWNGrAX0jm74ctMvFEIEQCHcsBor1LVatXMpmDbW3JW1q9bZ2hMaXy7d+2yraNJqwa4dykiIsLkiPyXbm5ustgAKdpl3Lt29OhROIMX+XLl4YcflttVSD+60FJCrkJGvDl5MlwphuVSiKEylJBT8OqECZD6n7NsTQWllY4fOxZu3rxprYni45TuqTS17v333gNioNSbEDPolEmTZEeBjfN58qmnjC9Vfea1KBlerdaiZEu4RWlA4JX6A1VjyzPFb8aFn+HpnZPgqI6jYTvjD8HDOybA51f/NDVdldeNQ+qooteSUhdwgUkNn1OtNpjpmFSU++Ed42HXtUgowH96lHR0shecXg69dk8EYu5UW+gmxz3lW6o9jMPpdygHzNXVFZT8OP+Id/Jpz9SaNfK8/T4y9m0V94mQy/L1w//+B7SHjchF5EqTpk2BonD2FD8/P3jhpZcUmUDrOX7cOKBUOb3I1atXYcwrr9jsVFF7iuapJSEhITB8xAhF6rOzs2Espk9SVE8vQk7ra6++qogBlObyEN6IqF69uibT4rUoHmYt16J4S/hsaUHAx90b5jQdD1SbSG05kBYND2E07K3Iz+FqWrzaw9ms/yIWFn7t4GwYGPk+RGWqv5epV3BjiPDTLgJGQAR7BcJnuM7uGqQ+EoYDDs2A0fs+hJM3z9m8Dmo3zEFmwzWXNsN920bDh9G/QL4GDmKHgFowusEgtafmkPodygEjhGnvEF24y5EUrBH0008/we8ya2YpKQhtyV4qSiyHDZCcje+//77IfjBLY1g79iSmpdEeMHtLL9yDVrdePUVm0D6rsZjKp4dI2FmsnzUanS9rNdusTZScaal9rOmydPyRRx6BihUrWjpl8zGjs2tOaGOzAoENac0njB+vOCpHdemGDNE2L53XwvIHwR5rYdkSPlraEKgeWAnmNRqt2bSXxW+BzttfgQ+OLoQLyVc0G9d8IHK8ph/+CrruHAu/JEjPCjLXZ+v7ETX729pUaLsmZerCR3WGC9VZnLI1Nw5C3z1vwPgDMyHy+imLLNbF9Rd1jpgsf7+4Ce7fNhZeOfkFUFqsFkL7vj5pNp5ZD62A7XAOGDlfTw+QX8n+WyQUoOiRVGnfoYPwYsXkAD0gswbXEgUREyI0ofnoQYgdbuLEiYpNoQvy4c89B7t3248qdxemtb7w/POQcE3eHcTiCoYrBcjbxwcmYLRIqdBNDIru/fXXX3b7MaHU25EY0Ys6c0bpdGAkEqMooeqXYwCvhWXU7LEWli3ho6URgXsrtIGp1Z/WbOoUffg6ZgN02zUOXtr7HmyK2Q2UGqa25OI+NCKLePXAJwbH65s4bff39glpCi1DG6o9Tav6+1XrASMr9rZ6Xo0TvyEjZr/9U+HRHa/C8gvr4FrGdTWGKaQzHwk1iPyFCDY6bnkBxp76SpPoptEIijQubP4GlPcNMx7iZzME3M3eO8RbIo8gZrbcXO02tFKkRg25p1MnIEp1LWXg4MGaU88XNz+KgD2LZBVK2fbS09PhNXTmBuH8Bg4aJDtSWpytls4R9fmihQsNrIaWztt6LD8vz9amstq1atUK+iH74m8rV8rqb+xENfU+fP992LtnD7yCkceyZdWjcjaOSc9UduAHjPwuXbLE9LDs1/S5e6RfP9n9lXTktSiMnj3XorAl/K40I/Bs7UfgKqaPLYndqCkMa5HKnB5+rp7wYGhr6BTaAhqF1IJKfuVBBFvgzaxkOJYUBTsTD8PK+O0Qn3ubyEvTSeJgAW7e8EaDYbgjy37ZNzT2xAZDIS7zuub0+ofSLsKhM4sB8NEuoCb0DG0DTXAvXJ2gahDo6a9oOYjnICb9GqY8nod9N47BmoQ9EJNzS5FOJZ3nNxoLTTHiyGIdAYd0wOiCjy4kicZbCyHCjDZt26oyVPny5aE1Mivu27tXFf3mSqnAabdu3cwP2/09OUx78IL+5IkTim3537JlsPGff2A07llq17490PqpIfSFR2QuVH/q0qVLiofw91f2BWyLARRlOHjwIFyMjralebFtiPZ9HxZqfh6jfkQb7uXlVWx7uSeNOM+fPx8umNXBk6uTPhOvv/GGYnISueNTP16L2+jpYS2UrCP3dR4EyNl5HckabuakANXx0lrS8rPhp2s7DA8am1K42gbWgXr+1aCSbzko51MWQjwDwN/dD7zdvQxujCc6bVnYjyIeqZhqlpqbjhGWGxCbkQBn0y7DgVtRcDzjqtZTKTIeRUQWNJ4AVfzFEJkVGUDCAXdXN3i/2Ri4se892JZsn33Nu1POAT3gPw6Mml5h0Bydsio+5aGCTygEewZCAJZKoHX2dL17qU5R0oy8LEjJSYNErDFHNwyiUq/AASwXoGYBZQnwwoe1h0H3iPZSupTKti54caNPmpYSloP2+wx4Wpt0gd54cTllypQSLJJ/+u+//4Z3sVaUFvIypo8ppSVXy04ioxg+bJiBKEXUGNVr1IBnke6+PTpilPolQqiIcmRkJHyPZCiiHGdvb29YjeQw9Ky2kBMzYvhwIGINUVKhQgUY/Mwz0LlzZwgMDBSiluzbizcmiAb/yOHDQnQalYzHSCntxbK38FoA2HMtiFjmINaElCOLMBJbp446TG4UVe/ds6ccswx9tm7fLruvyI5Ua5NqIMqRMmXKwKrVq+V0VdyHqNjHH5wFVGSXRQwCixqP191FeUp2Gow68IHdnDAxyOpLyzs1B8PgWvL+5vU1E/WtcVgHjKCZNXMmrP79d9VRmjV7Noiq/2XJWCI36IdMbHILKlvSaekY1d5a/uuvwi6QLY2h9BhdcE9EcgXR4oPO1/0PPghtMdpI6U5S6e+pYPIZ3HNEBBRUU05ExMt0jhTRHafCvE3HMH29betWmIIU/qKFiBT69O0LbTFiTGUbpKYn0t/CKaS7P4RROvrbVoOevzcWp56EcydWVT0Ir4X91oIdMHX/AhzVASNU2AkT99n4rP4oeKBKV3EKBWpKwvTMMQc/ZidMAKbTaw6CIbXsW95IwDQ0U+HQDhhdBA9SQMhhC8pULHn5L7+ovmfq888+g+UKiirbMhdKeaJUP73LarzrOevjj1U1sxYyUNLdayIkKYMprZQ+R05aAUa30tHZysJ6a3TxT1E5qulF9bpo75MaQkQk3//wA1QQVGPOVht/xc+12vsPq1SpAoR11apVoWxoqAFjX8TZBZ0f2rNH+7oIZ4poE4Mk1bVTMyjfsFEjmD1njsEOW3HSoh2vhRYoFx2DHbCimIg84sgOGOFATtibhz+zSzqiyHWwp64FuBeoV8WO9jShxLEpEjbx0Cew4ebREttyA8sIfIzsko9X7235JB+1iMDdxFKLp/V9kC7uKD1wPUYk1JL+GJmgC2S1pVfv3qo6YBT9elBmKojaczfX/xDamYSpK2rWxjobFQX00INMRHZCrZ0vmvejjz0GqampsGQxbgpWSegmiehooVxTqYj7ezNm6M75ovnwWshdVe7HCKiHANUI+6DZWAg5vgQWx/6j3kBOqNnbxR2+bjIROjlAAd4ATz+Y2/I1eOfo/Dv775xwSVSZEu3t+6LhaN072apMXqFSfeTgKJjEAJUjYN179FBgne1dqSZYo8aNbe8gseVgrHUkam+OxKFlNR+C+4mGIa28s8sw3IvV9/777TJNKoPwDO6PKw04E9nNRxhVlZoSqdXC8FpohTSPwwhIQ8DTzQMmNx6hKUW9NAv117q8RyD80vodh3C+jOiRsz2j2WiYWKWf8RA/l4AAkcSsaPUOO18l4GTttMM7YNWrV4eHcP+UGkLOF5ELaCF0AUbFkdWQACRFcJTol3H+xgvS4ciw56zywksvwTPoaNpTjDi/NGqUPc1QdWwqQD133jyI0DjFU+qkeC2kIsbtGQFtECB2xGF1+sPSJq+Cv6s6bK/azET9UVr6VYMVbT9AGv3a6g8meARa51H1B8BXDceAj6uHYO3OpY5o9Fe1+xCouDWLPAQc3gGjaQ8YOBDo4kW0aF0jyFDsuVw50dMwsAA6UvTLCACt6RCM3L2hAlmEcQx7PBPt9ptTpwJFb9X43EqdE9nwFDKKTn/nHdUo+6XaJKo97fn6/Msvde98GefLa2FEgp8ZAf0h0BWLNf+BF53NfKvozzgdWDSsQndY1u5diPATfx2j5fTuq3QP/NHmA2jgY3/KfC3nbetYwyN6wtK203Gdw23twu0sIOAUDhjd2RZNrV6vfn1o0qSJBcjUO+Tp6Wm4KBc5AlH5PoDsf44sfZFV7/MvvpDMXKjHORMRxWdYN4z2/OlN7sX6cF8tWACUrucMQvtDP0EGU72mHRaHMa9FcejwudKAgCveqNKjVAuoBD+0fx9eqthHj+bZxSaKChLT4ZtNRgKl8jmD1AyqCr90+BBGVtTfb7W98C3j5gtLGk+EKY2fB283jgQrXQencMAIBHLAyIERJbQHyR7RCSpoSymDomT4iBG6JB2QOr8mTZvC4qVLVSuILdUeOe073nMPLFy0SNW9fnLsMu1TDyn6v0Ybu3XvbnrYoV5ThJFqS02aNAmIfMZRhddC/ZWTWo7C1CI/Pz/Tt0JfUz1A+hzLEWLu1YsoKc4epqN5mOPpg8VxX200DH5qPhWqe4Wany5V73sGN4L1HWYbaOZdDKWhnWf6vu4+8Eaj4bCs2SSo6lnWeSYmYyb9QtvA2o6z4d6ItjJ6cxdLCDiNA0Zf1s8OHWppjpKPUfFeKtxrD6EfdVFU8cT6RjWPnEVojYlIYSzWy6J6U44iRG//2uuvw4z334dQjIDpXeii9O1p02Dq228LvRmgxbzrN2gAVCCXiizrpc6XknnzWihBr+S+4eHyU2iUOG8lWUY3/+j7W45UxtIaehH6PZN7Q1Gr/ddKsGpbrgms7vgJjK70gBI1Dtk31B2ZA+u9CPPbTHX4lMOSFuCe8BbwB67zqIp9S2rqdOcjPIIMUa9PWk6Ecj6l2wkVvbhO44ARMFTMllLulMrQYcNk331UOjb1f+CBB2T/aJmO/xKSPDiSo2Jqu7XXdFeYSgMs+/576NK1q7VmujlODjDZSmmgjuQQ0AVgz549Ydn//gda74WUs3gU6SLHnFJVa9asKUeFbvvwWqi3NBRZlyPNW7QANSNgZFO7du3kmAatsdi8nqRTp06yzGnRsqWsflp38vfwhXENn4EN7WdCr2D1mIy1nldx4z0f0Qs23PMpPFy1GxBxRWkQoqqf2GgorG/3MVDUz9nF08UNXqv6KKzvNM8Q9XK26KYe1s+hCzFbAnD9unUw4733LJ2y6VhdTMGaj/tg5KZ/2DSIDY1WrlwJc3H/ilxp2aqVoeCsPdIo5dosp9+BAwcMdayOHjkip7tqfQj/4UgxTyQQziBnzpwx4Lxzxw5dTYc+308igchTTz0l5OaLriZnxRheCyvAyDhMhcAHIhnOtfh4Sb3fxd8YtW8AXbhwAZ4ZPFiSXfT38DMWWNfTPs7IyEgY/fLLkuZB2wl+WbECQkJCJPWzd2MqIr8t/gDMO/szHEiLtrc5wsd/LLQdvFT7CageWFm4bkdS6Ozr/Gz5bvBczX5Qyd859oPr9bPldA5Ybm4ujETq8ii8YJQjn8yZA61bt5bTVWifzMxMw49vbGysLL0Lsbhu3f+3d/chVd1hAMcfmm6pV1eNXpYvTVFbspfYaA0cmhRrY8SCFWwMWhFR+2cxtragNmjV39Eggvyrf9o/tUFtjKJmBZHWyA2X6NTSzLLUfLua+Nae56bkqNu953j1vvg9IF7uPb9zz/k8995znvN7W7jQVdloKzQ8PCyXL1+WI1rTVH7lSlh33y7K1q5dK6/qAC6xmPxevXpVjuoF3pnT4Z2U1C7QrN+nTUERSRebk/nhIxah0T579qx8v3Nn0BtbvHix7Nu/f1Ju0v2o72Pft2AXa4YfafP62cXqD7t2OfrN+GLrVlmjv6PRugw/GJaS25fk4LVjMZGIfTwnX9ZlrpJFM2KrdcF4P18P41wmBzTO5T0N491cWMtP0/57n84t1MRrtSxITg3rvkyVN4+5BMwCV1VVJZ9v3ixDQ0OO4viBNv37dvt2R2UmcmWr3flST0ROF5t0eVMMz5/1NA+rHTh18qT8euKE9Pb2Pm3VkL1mzV5Xa7PI5TpwRXr61LgzePPmTTmtSdgvepe6vb09ZJaBNmSjk36o/bsKCgokOTk50OpT4nViMf4wH9SpCn46ciTghizZt2auc8bRdyzgm4xZoe/+fflm2zaxWqRAi01jsmfv3ohsdt7V1SVfaRPhaj03B1pWaNNnm6Yjmpps+zsmSz4vtVbI4evH5ff2v/2tFpHP28iGG3S48Y8yVnBBHiBCvji3aJzrozPOm1LfkzUL3o35vnwBwjjpL8dkAmaKF7Sp1Hc7dojViAWzFBQW+n70bfSpSFosmdize3fQu2RJ5Nd6wg53E8qgd3iCVrSmReXl5XKprExKSkqkrbU1pO9kHeQtCViq/TTydOCHuLi4kG4/WjZm36/KykopvXhRzmlNQmNjY0h33WoRrTlnfn6+r29LRgbz7/gDJhb+ZAI/b7Xox44eFUvE/J0z7HNoI2tOVvI1utd2I+mATl1x4vjx0ace+2+1RZu3bJHxjDr42EZD/ERnZ6fs02b1f5w543fLn2kNns39GGt9l+2Ab3Tfkt+azsvhplNyZ7Dbr0G4XyhMeVnWpC6XIp3zLEn7t7E4E4iWOOcn58gn6Stl2bwlxNlZiEO2dswmYCbUUF8vxcXFcv7cOb9gdkdz3fr18r4O/x6pSYvV6hQfOiRlpaV+j8MuTDds3ChFRUUxcefQ74G6eMEurpqamqS2pkZqa2t9f3V1dUH3+0jTUcVycnN9gztk5+RItg7yMNkXYS4Oe9KL2F3Au3fvSo061404m7nZB7PYoAZZamuDaGTpSKRmnZmZOeGDHQSzb9G2DrFwF7FWvVFTpjdt/qmokLa2Nt9nL01rtZfqwBbWnzOczYqvX7vm2zf77ero6JAZ2j8qV78jdhMomm5M2PmsVM9lNxoafMdhLQgWac22DTryos7pGevL4PCg/HWvSk43l8rPdy5Iy6A37IdsSdfKOW9LoV6MR/skymHHHNmBSIzzEk+mrJr7jhTNe4v+XRHwQYnpBGzUt6WlRaqrq6VJm00NDAz4TqI2OetLenGXnZ0dNbUXzc3NvuOwfmGDdhzTpslsHdbchs23C9ZITSBH4xBp//v7+8WaxvT09IjVmI1drCbU4/FIis7JNlVrt8Z6jOexfee6u7vF6/WK9W0cu9id7iQdwTBJrSd6VLmx7ztVHxOLqRp5jjsSBYYeDElNZ7382aatCO5VyLmOSvEO//9cNBH7nZcwXwpnviZvvpAnb+jfzOeen4i3YZsjAuGKc5bOUbds1uuyZNYrvljPThj/KOEENXQCUyIBCx0XW0IAAQQQQAABBEIvYLUm9d1NUu+9JXXeRvnX2yB1vbel+v5t6XsQXHeKsXs1Lz5F8hLTJStpvuR6FkhWcppkelJl1vQZY1fj8SQLhDrOs+M8slCT6uykNI1zhmSOxHluYuTPOzrJ9BH1diRgERUOdgYBBBBAAAEEEHgkYKPtdfZ7pWegV7oGvNI71CdD2rS+b+hhbZk1jU2Mmy5xOndTclyieOKTJOXZZEmIe+7RRngU8QIB46wjFSbGT5dnNM6e+ASNtU50rrGmr17Eh/aJO0gC9kQWnkQAAQQQQAABBBBAAAEEQi8wNaYwD70bW0QAAQQQQAABBBBAAAEEHAuQgDkmowACCCCAAAIIIIAAAggg4E6ABMydG6UQQAABBBBAAAEEEEAAAccCJGCOySiAAAIIIIAAAggggAACCLgTIAFz50YpBBBAAAEEEEAAAQQQQMCxAAmYYzIKIIAAAggggAACCCCAAALuBEjA3LlRCgEEEEAAAQQQQAABBBBwLEAC5piMAggggAACCCCAAAIIIICAOwESMHdulEIAAQQQQAABBBBAAAEEHAuQgDkmowACCCCAAAIIIIAAAggg4E6ABMydG6UQQAABBBBAAAEEEEAAAccCJGCOySiAAAIIIIAAAggggAACCLgTIAFz50YpBBBAAAEEEEAAAQQQQMCxAAmYYzIKIIAAAggggAACCCCAAALuBEjA3LlRCgEEEEAAAQQQQAABBBBwLEAC5piMAggggAACCCCAAAIIIICAOwESMHdulEIAAQQQQAABBBBAAAEEHAuQgDkmowACCCCAAAIIIIAAAggg4E6ABMydG6UQQAABBBBAAAEEEEAAAccCJGCOySiAAAIIIIAAAggggAACCLgTIAFz50YpBBBAAAEEEEAAAQQQQMCxAAmYYzIKIIAAAggggAACCCCAAALuBP4DFydOG+WQfoMAAAAASUVORK5CYII=" />
                        </defs>
                    </svg>

                    <svg class="x-pop" width="24" height="25" viewBox="0 0 24 25" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M18.725 25L12.075 15.69L5.495 25H0.315L9.415 12.505L0.805 0.499998H5.95L12.215 9.215L18.41 0.499998H23.345L14.77 12.33L23.975 25H18.725Z"
                            fill="white" />
                    </svg>


                </div>


                <div class="text-pop">
                    <p class="big-text-pop">
                        A Piece of Paper is not the <span class="yellow"> Invitation </span> your guests deserve!
                    </p>

                    <div class="small-text-pop">
                        <p class="small-text-pop">
                            We make you the hot topic of the day by creating your custom <b>online wedding
                                invitations.</b>
                        </p>
                         comment start <p class="small-text-pop">
                            Excite your friends with our <span class="yellow-green"> Game of Thrones </span> and <span
                                class="yellow-green">
                                F.R.I.E.N.D.S.
                            </span> Themed invitations.
                        </p>  comment end (add here)

                    </div>

                    <button class="check-pop"><a
                            href="https://wa.me/918291121661?text=Hey,%20I%20just%20checked%20out%20Weddingo.%20I%20am%20interested%20in%20creating%20an%20invitation%20for%20myself."
                            target="_new" class="check-font-pop">Whatsapp us to check out our Invitation
                            Themes!</a></button>

                </div>

                <div class="footer-pop">
                    <u class="footer-text-pop">CONTINUE BROWSING THE WEBSITE</u>
                </div>


            </div>
        </div> -->

        <!-- <style>
            .total-popup {


                background-image: url(images/image2.png);
                background-blend-mode: overlay;
                background-repeat: no-repeat;
                background-size: cover;
                margin: 0px;
                position: fixed;
                top: 0;
                z-index: 1000;
                height: 100%;
                width: 100%;
                opacity: 0;
                /* visibility: visible;
                transition: 0.8s all ease-in-out; */
            }



            .overlay-pop {
                background-color: rgba(33, 181, 82, 0.86);
                height: 100%;
                width: 100%;
                transition: all 5s ease;
            }

            .header-pop {
                display: flex;
                justify-content: space-between;
            }

            .icon-pop {
                height: 74px;
                width: 342px;
                -webkit-transition: 1s;
                transition: 1s;
            }

            .x-pop {
                margin: 28px;
                transition: all 0.5s ease;

            }

            .text-pop {
                color: white;
                padding: 0% 17% 0% 17%;
                transition: all 5s ease-in;
                font-family: 'Montserrat', sans-serif;
                font-style: normal;


            }

            .big-text-pop {
                font-size: 56px;
                font-family: 'Montserrat', sans-serif;
                line-height: 81px;
                margin: 0;
                padding-top: 8%;
                padding-bottom: 2%;
                font-weight: 900;
            }




            .yellow {
                color: #FFF500;
            }

            .yellow-green {
                color: #CDFF04;
                font-weight: bold;
            }

            .small-text-pop {

                padding-right: 42%;
                font-family: 'Montserrat', sans-serif;
                font-size: 20px;
                line-height: 35px;
                padding-bottom: 2%;
            }

            .check-pop {
                width: 551px;
                height: 48px;

                background: #FFF500;
                border-radius: 8px;
                border: none;
                transition: all 0.5s ease;
            }

            .check-font-pop {
                font-weight: 800;
                font-size: 20px;
                line-height: 130.4%;
                font-family: 'Montserrat', sans-serif;




                color: #000000;
            }

            .check-font-pop:hover {
                color: black;
            }

            .footer-pop {
                font-family: 'Montserrat', sans-serif;
                color: #000000;
                text-align: center;
                padding-top: 5%;
                font-weight: 900;
                font-size: 18px;
                padding-bottom: 5%;
                transition: all 0.5s ease;
            }

            .footer-text-pop {
                transition: all 0.5s ease;
            }

            .x-pop,
            .check-pop,
            .footer-text-pop,
            .footer-text-pop :hover {
                cursor: pointer;

            }

            .x-pop:hover {
                width: 20px;
                height: 21px;
                padding: 2px;
            }

            .check-pop:hover {

                background: #ccc404;




            }

            .check-font-pop:hover {
                font-size: 18px;
            }

            .footer-text-pop:hover {
                font-size: 17px;

            }

            @media only screen and (max-width: 730px) {
                .total-popup {
                    background-position: -278px;

                }

                .icon-pop {
                    height: 37px;
                    width: 170px;

                }

                .x-pop {
                    margin: 10px 6px 0 0;
                    transition: all 0.5s ease;

                }

                .text-pop {
                    padding: 0% 4% 0% 4%;

                }

                .big-text-pop {
                    font-size: 36px;
                    line-height: 40px;
                }

                .small-text-pop {
                    padding-right: 2%;
                    font-size: 19px;
                    line-height: 27px;
                    padding-bottom: 9%;
                    padding-top: 5%;

                }

                .check-pop {
                    width: 100%;
                    height: 51px;
                    background: #FFF500;
                    border-radius: 8px;
                    border: none;
                    transition: all 0.5s ease;
                }

                .check-font-pop {
                    font-weight: 800;
                    font-size: 17px;
                    line-height: 130.4%;
                    font-family: 'Montserrat', sans-serif;
                    color: #000000;
                    transition: all 0.5s ease;
                }


                .footer-pop {
                    font-family: 'Montserrat', sans-serif;
                    color: white;
                    text-align: center;
                    padding-top: 15%;
                    padding-bottom: 7%;
                    font-weight: 900;
                    font-size: 15px;
                    transition: all 0.5s ease;
                }





            }
        </style> -->

        <script>


            jQuery(document).ready(function () {
                jQuery('.total-popup').animate({ 'opacity': '1' }, "slow");
            });

            var a = 0;
            jQuery(window).scroll(function () {

                var oTop = jQuery('#counter').offset().top - window.innerHeight;
                if (a == 0 && jQuery(window).scrollTop() > oTop) {
                    jQuery('.counter-value').each(function () {
                        var jQuerythis = jQuery(this),
                            countTo = jQuerythis.attr('data-count');
                        jQuery({
                            countNum: jQuerythis.text()
                        }).animate({
                            countNum: countTo
                        },

                            {

                                duration: 5000,
                                easing: 'swing',
                                step: function () {
                                    jQuerythis.text(Math.floor(this.countNum));
                                },
                                complete: function () {
                                    jQuerythis.text(this.countNum);
                                    //alert('finished');
                                }

                            });
                    });
                    a = 1;
                }

            });

        </script>
        <script>



            // jQuery(document).ready(function () {
            //     jQuery(".x-pop").click(function () {
            //         jQuery("#total-pop").animate({ opacity: "0" }, 800, function () {
            //             jQuery("#total-pop").fadeOut();
            //         });
            //     });

            //     jQuery(".footer-text-pop").click(function () {
            //         jQuery("#total-pop").animate({ opacity: "0" }, 800, function () {
            //             jQuery("#total-pop").fadeOut();
            //         });
            //     });
            // });






        </script>
        <script>

        var name = '';
        var email = '';
        var contact = '';
        var radioValue = '';

        console.log(document.cookie);
        jQuery(".wel-popup-wrapper").hide();  // comment this TO SHOW-WELCOME-POPUP

        if (localStorage.customertype || document.cookie) {
            console.log("there is localstorage");
            jQuery(".wel-popup-wrapper").hide();
        } else {
            console.log("no localstorage");
        }

         jQuery(".wel-popup-wrapper").hide();// comment this TO SHOW-WELCOME-POPUP


        jQuery(".wel-details-div").hide();

        jQuery(".wel-close-btn").click(function() {
            jQuery(".wel-popup-wrapper").hide();
            var day = new Date();
            console.log(day);

            var nextDay = new Date(day);
            nextDay.setDate(day.getDate() + 1);
            console.log(nextDay);
            document.cookie = "value=1; expires=" + nextDay;
            console.log(document.cookie);

        });

        jQuery(".popup-options").click(function() {
            setInterval(function() {
                radioValue = jQuery("input[name='customer']:checked").val();
            }, 100);
            jQuery(".wel-details-div").show();
            jQuery(".welcome-div").hide();
        });

        jQuery(".wel-form-btn").click(function() {
            jQuery(".wel-popup-wrapper").hide();
            console.log("Your are a - " + radioValue);
            name = jQuery("#namevalue").val();
            email = jQuery("#emailvalue").val();
            contact = jQuery("#contactvalue").val();

            localStorage.customertype = radioValue;
            localStorage.name = name;
            localStorage.email = email;
            localStorage.contact = contact;

            jQuery.ajax({
                type: 'GET',
                url: "https://www.weddingo.in/addclientdata.php", //path to php script
                data: {
                    client: radioValue,
                    name: name,
                    email: email,
                    contact: contact
                }
            })

        });

        jQuery(window).keypress(function(event) {
            if (event.keyCode == 13) {
                if (radioValue != '') {
                    jQuery(".wel-form-btn").click();
                }
            }
        });

        </script>
        <style>
            @-webkit-keyframes mymove {
                0% {
                    bottom: -160px;
                }

                50% {
                    bottom: 50px;
                }

                100% {
                    bottom: 0px;
                }
            }

            @keyframes mymove {
                0% {
                    bottom: -160px;
                }

                50% {
                    bottom: 50px;
                }

                100% {
                    bottom: 0px;
                }
            }

            @font-face {
                font-family: Cushy;
                src: url(Cushy.otf);
            }

            .whatsapp-popup {
                width: 255px;
                height: 80px;
                background: #fff;
                bottom: -160px;
                position: fixed;
                right: 0px;
                margin: 50px;
                border-radius: 13px;
                padding: 12px;
                box-shadow: 0px 0px 3px 0px #b5b5b5;
                -webkit-animation: mymove 5s;
                /* Safari 4.0 - 8.0 */
                -webkit-animation-delay: 12s;
                /* Safari 4.0 - 8.0 */
                animation: mymove 5s;
                animation-delay: 12s;
                animation-fill-mode: forwards;
            }

            .whatsapp-popup:after {
                content: "";
                width: 21px;
                height: 21px;
                position: absolute;
                background: #fff;
                transform: rotate(45deg);
                bottom: -10px;
                right: 33px;
                box-shadow: 1px 1px #0000001c;
            }

            .whatsapp-popup:hover {
                cursor: pointer;
            }

            .top-text {
                font-size: 13px;
                margin: 0px;
                font-weight: bold;
                text-align: center;
                font-family: Poppins, Georgia, serif !important;
                color: #403e3e;
            }

            .close-btn {
                float: right;
                cursor: pointer;
            }

            .close-btn>g>path:hover {
                fill: #000 !important;
            }

            .notify-content {
                display: flex;
                justify-content: center;
                align-items: baseline;
                cursor: pointer;
            }

            .content-text {
                font-size: 43px;
                margin: 0px;
                font-weight: bold;
                line-height: 60px;
                padding: 0 4px 0 0 !important;
                font-family: Cushy !important;
                color: #403e3e;
            }

            .sec-color {
                color: #1EB650;
            }

            .whatsapp-icon-btn {
                width: 45px;
                height: 45px;
                fill: #1EB650;
            }

            @media only screen and (max-width:768px) {
                .whatsapp-popup {
                    width: 195px;
                    height: 55px;
                    margin: 25px;
                }

                .top-text {
                    font-size: 10px;
                    margin: 0px;
                    line-height: 16px !important;
                }

                .close-btn {
                    width: 9px;
                    height: 9px;
                }

                .whatsapp-icon-btn {
                    width: 35px;
                    height: 35px;
                }

                .content-text {
                    font-size: 32px;
                    line-height: 41px;
                }

            }
        </style>
        <div class="whatsapp-popup" onclick="approach('whatsapp')">
            <svg class="close-btn" onclick="closepopup();" width="11" height="11" viewBox="0 0 11 11" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0)">
                    <path
                        d="M6.68183 5.49529L10.7559 1.42123C11.0812 1.0962 11.0812 0.569042 10.7559 0.244043C10.4309 -0.0813093 9.90405 -0.0813093 9.5787 0.244043L5.50462 4.3181L1.43021 0.244013C1.10554 -0.0813379 0.578027 -0.0813379 0.253028 0.244013C-0.072001 0.569042 -0.072001 1.0962 0.253028 1.4212L4.32743 5.49529L0.244013 9.57871C-0.0813377 9.90406 -0.0813377 10.4309 0.244013 10.7559C0.406513 10.9188 0.619647 11 0.832782 11C1.04557 11 1.2587 10.9188 1.42123 10.7559L5.50462 6.6725L9.5787 10.7466C9.74155 10.9091 9.95434 10.9907 10.1675 10.9907C10.3803 10.9907 10.5934 10.9091 10.7559 10.7466C11.0813 10.4216 11.0813 9.89476 10.7559 9.5694L6.68183 5.49529Z"
                        fill="#585858" />
                </g>
                <defs>
                    <clipPath id="clip0">
                        <rect width="11" height="11" fill="white" />
                    </clipPath>
                </defs>
            </svg>

            <a href="https://wa.me/918291121661?text=Hey,%20I%20just%20checked%20out%20Weddingo.%20I%20am%20interested%20in%20creating%20an%20invitation%20for%20myself."
                target="_new">


                <p class="top-text">Reach out to us on <span class="sec-color">Whatsapp</span></p>


                <div class="notify-content">
                    <p class="content-text">weddin<span class="sec-color">G</span></p>
                    <svg class="whatsapp-icon-btn" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="90px" height="90px"
                        viewBox="0 0 90 90" style="enable-background:new 0 0 90 90;" xml:space="preserve">
                        <g>
                            <path id="WhatsApp"
                                d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522
		c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982
		c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537
		c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938
		c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537
		c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333
		c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882
		c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977
		c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344
		c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223
		C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z" />
                        </g>
                    </svg>

                </div>

            </a>
        </div>




        <script>
            function closepopup() {
                document.getElementsByClassName("whatsapp-popup")[0].style.display = "none";
            }

        </script>
        <!-- <style>
            .hiring-popup {
                position: fixed;
                bottom: 0;
                left: 0;
                width: 150px;
                cursor: pointer;
            }

            .hiring-popup img {
                margin: auto;
            }

            .hiring-popup-text {
                color: white;
                text-align: center;
                text-transform: uppercase;
                text-shadow: 0px 0px 0px #292929;
            }
        </style> -->
        <!-- <div class="hiring-popup">
            <a href="#header">
                <img src="images/hiring-img.png">
                <p class="hiring-popup-text"></p>
            </a>
        </div> -->

<!-- GO TOP BUTTON START -->


        <style>

                #myBtn {
                display: none;
                position: fixed;
                bottom: 20px;
                left: 30px;
                z-index: 99;
                font-size: 18px;
                border: none;
                outline: none;
                
                color: white;
                cursor: pointer;
               
                }

                html {
                scroll-behavior: smooth;
                }

                
        </style>

                        <button onclick="topFunction()" id="myBtn" title="Go to top">
                        <svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_d)">
<circle cx="40" cy="36" r="36" fill="black" fill-opacity="0.42"/>
<path d="M19.7812 42.1908L24.4924 46.902L39.972 31.4224L55.4516 46.902L60.1627 42.1908L39.972 22.0001L19.7812 42.1908Z" fill="white"/>
</g>
<defs>
<filter id="filter0_d" x="0" y="0" width="80" height="80" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
<feOffset dy="4"/>
<feGaussianBlur stdDeviation="2"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
</filter>
</defs>
</svg>
                    
                    </button>

                        <script>
                        //Get the button
                        var mybutton = document.getElementById("myBtn");

                        // When the user scrolls down 20px from the top of the document, show the button
                        window.onscroll = function() {scrollFunction()};

                        function scrollFunction() {
                        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                            mybutton.style.display = "block";
                        } else {
                            mybutton.style.display = "none";
                        }
                        }

                        // When the user clicks on the button, scroll to the top of the document
                        function topFunction() {
                        document.body.scrollTop = 0;
                        document.documentElement.scrollTop = 0;

                        }
                        </script>

<!-- GO TOP BUTTON END -->


        <script type="text/javascript">
            function revslider_showDoubleJqueryError(sliderID) {
                var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                jQuery(sliderID).show().html(errorMessage);
            }

        </script>






        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

        <script>
            jQuery(".home-testimonials-slider").slick({
                //			autoplay: true,
                //			autoplaySpeed: 3000,
                "prevArrow": '<div class="prev-arrow"><svg enable-background="new 0 0 58 58" version="1.1" viewBox="0 0 58 58" xml:space="preserve" xmlns="https://www.w3.org/2000/svg"><circle cx="29" cy="29" r="29" fill="#EBBA16"/><g fill="#fff"><polygon points="44 29 22 44 22 29.273 22 14"/><path d="M22,45c-0.16,0-0.321-0.038-0.467-0.116C21.205,44.711,21,44.371,21,44V14   c0-0.371,0.205-0.711,0.533-0.884c0.328-0.174,0.724-0.15,1.031,0.058l22,15C44.836,28.36,45,28.669,45,29s-0.164,0.64-0.437,0.826   l-22,15C22.394,44.941,22.197,45,22,45z M23,15.893v26.215L42.225,29L23,15.893z"/></g></svg></div>',
                "nextArrow": '<div class="next-arrow"><svg enable-background="new 0 0 58 58" version="1.1" viewBox="0 0 58 58" xml:space="preserve" xmlns="https://www.w3.org/2000/svg"><circle cx="29" cy="29" r="29" fill="#EBBA16"/><g fill="#fff"><polygon points="44 29 22 44 22 29.273 22 14"/><path d="M22,45c-0.16,0-0.321-0.038-0.467-0.116C21.205,44.711,21,44.371,21,44V14   c0-0.371,0.205-0.711,0.533-0.884c0.328-0.174,0.724-0.15,1.031,0.058l22,15C44.836,28.36,45,28.669,45,29s-0.164,0.64-0.437,0.826   l-22,15C22.394,44.941,22.197,45,22,45z M23,15.893v26.215L42.225,29L23,15.893z"/></g></svg></div>'
            });


            jQuery(".dropbtn").on('click', function () {
                jQuery(".dropdown-content").toggleClass("active-dropdown");
                console.log("happening");

            });


            jQuery(document).ready(function () {
                console.log("ready!");
                jQuery("#main-menu-state").prop("checked", false);
            });

        </script>
        <script type='text/javascript' src='wp-content/themes/nikah/js/wow.js?ver=4.9.8'></script>
        <script type='text/javascript' src='wp-content/themes/nikah/js/main.js?ver=4.9.8'></script>
        <script type='text/javascript'
            src='wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var elementorFrontendConfig = {
                "isEditMode": "",
                "is_rtl": "",
                "breakpoints": {
                    "xs": 0,
                    "sm": 480,
                    "md": 768,
                    "lg": 1025,
                    "xl": 1440,
                    "xxl": 1600
                },
                "urls": {
                    "assets": "http:\/\/www.weddingo.in\/wp-content\/plugins\/elementor\/assets\/"
                },
                "settings": {
                    "page": [],
                    "general": {
                        "elementor_global_image_lightbox": "yes",
                        "elementor_enable_lightbox_in_editor": "yes"
                    }
                },
                "post": {
                    "id": 8,
                    "title": "Home",
                    "excerpt": ""
                }
            };
        /* ]]> */

        </script>
        <script type='text/javascript' src='wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.1.2'></script>
        <script type="text/javascript" src="wp-includes/js/global.js"></script>


        <!-- GOOGLE ANALYTICS -->

        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-67216527-7"></script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-67216527-7');
            </script> 

            <!-- all functions -->

          <!-- <script> 
          //view invitations page
            function invitations() {
                gtag('event', 'view-invitations', {
                'event_category': 'Exploration'
                
                });

            }

            //view couple name generator page
            function couplename() {
                gtag('event', 'visit-couplename', {
                'event_category': 'Engagement'
                
                });

            }

            //click on contact us
            function approach() {
                gtag('event', 'contacted', {
                'event_category': 'Approach'
              
                });

            }

            //click on contact after seeing plan
            function purchase(price) {
                gtag('event', 'interested', {
                'event_category': 'Ecommerce',
                'price': price
                
                });

            }

            //click on purchase
            function purchase() {
                gtag('event', 'interested', {
                'event_category': 'Ecommerce',
                
                
                });

            }

            //view our couples page
            function feedback() {
                gtag('event', 'feedback', {
                'event_category': 'Couples'
               
                });

            }

            //view individual weddingo
            function weddingo(link) {
                gtag('event', 'visit-weddingo', {
                'event_category': 'Couples'
                'link':link
                });

            }

            //view demo template (preview)
            function demo(link) {
                gtag('event', 'visit-demo', {
                'event_category': 'Exploration'
                'link': link 
                });

            }

         
          
          </script> -->


            <!-- end functions -->


        <!-- END ANALYTICS -->

</body>

</html>