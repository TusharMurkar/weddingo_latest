 $(window).load(function () {
     // shuffle
     //    var $main = $('#shuffle-wrapper');
     //    $main.shuffle({
     //        itemSelector: '.item' // the selector for the items in the grid
     //    });



     console.log('window load called !');


 });

 $(function () {

     // window.scroll menu ite activated    
     $(window).scroll(function () {
         activeMenuItem();
     });



     // function for active menuitem
     var sections = $('section'),
         nav = $('#nav'),
         nav_height = nav.outerHeight();

     function activeMenuItem() {
         var cur_pos = $(window).scrollTop() + 2;
         sections.each(function () {
             var top = $(this).offset().top - nav_height,
                 bottom = top + $(this).outerHeight();

             if (cur_pos >= top && cur_pos <= bottom) {
                 nav.find('ul > li > a').parent().removeClass('active');
                 nav.find('a[href="#' + $(this).attr('id') + '"]').parent().addClass('active');
             } else if (cur_pos == 2) {
                 nav.find('ul > li > a').parent().removeClass('active');
             }
         });
     }

     // smooth-scrolling click menu item
     $(function () {
         $('#navbar > ul > li > a').click(function () {
             if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                 var target = $(this.hash);
                 target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                 if (target.length) {
                     $('html, body').animate({
                         scrollTop: target.offset().top - 100
                     }, 500);
                     return false;
                 }
             }
         });
     });


     // home-slider
     $('.slider').owlCarousel({
         singleItem: true,
         autoPlay: true,
         navigation: true,
         navigationText: ['<i class="fa fa-angle-left">', '<i class="fa fa-angle-right">'],
         mouseDrag: false
     });


     // event animation
     $('#event .content .row:first .col:first').each(function () {
         var $this = $(this);
         var myVal = $(this).data("value");

         $this.appear(function () {
             $('#event #first-row .col:first').addClass('animated fadeInLeft');
         });
     });

     $('#event #second-row .col:last').each(function () {
         var $this = $(this);
         var myVal = $(this).data("value");

         $this.appear(function () {
             $('#event #second-row .col:last').addClass('animated fadeInRight');
         });
     });

     $('#event #last-row .col:last').each(function () {
         var $this = $(this);
         var myVal = $(this).data("value");

         $this.appear(function () {
             $('#event #last-row .col:first').addClass('animated fadeInLeft');
         });
     });


     // journal
     $('.journal .frame a').click(function (e) {
         e.preventDefault();
     })


     // gift-slider
     $('.gift-slider').owlCarousel({
         autoPlay: true,
         items: 3,
         itemsDesktop: [1199, 3],
         itemsDesktopSmall: [991, 2],
         itemsTabletSmall: [767, 1],
         pagination: false,
         mouseDrag: false
     });


     // about Lightbox pluging
     $('.gallery .col a').nivoLightbox();


     // video-bg Lightbox pluging
     $('.video-bg a').nivoLightbox();






 });

 $(document).ready(function () {
     var i = 0;

//
//     var hisfamily = [
//         {
//             'name': 'Abhay Amin',
//             'photo': 'images/home/groomsmen-bridesmaids/img-1.jpg',
//             'relation': 'Brother',
//             calls: 'anna'
//        },
//         {
//             'name': 'Navinchandra Amin',
//             'photo': 'images/home/groomsmen-bridesmaids/navin.png',
//             'relation': 'Father',
//             calls: 'dada'
//        },
//         {
//             'name': 'Usha Amin',
//             'photo': 'images/home/groomsmen-bridesmaids/mummy.jpg',
//             'relation': 'Mother',
//             calls: 'mummy'
//        },
//         {
//             'name': 'Raju Amin',
//             'photo': 'images/home/groomsmen-bridesmaids/ajja.jpg',
//             'relation': 'Grandfather',
//             calls: 'ajja'
//        },
//         {
//             'name': 'Shanti Amin',
//             'photo': 'images/home/groomsmen-bridesmaids/amma.jpg',
//             'relation': 'Grandmother',
//             calls: 'amma'
//        },
//         {
//             'name': 'Vijaya Karkera',
//             'photo': 'images/home/groomsmen-bridesmaids/aunty.jpg',
//             'relation': 'Aunty',
//             calls: 'aunty'
//        },
//         {
//             'name': 'Narendra Karkera',
//             'photo': 'images/home/groomsmen-bridesmaids/uncle.jpg',
//             'relation': 'Uncle',
//             calls: 'uncle'
//        }
//    ];
//
//     var herfamily = [
//         {
//             'name': 'Nikita Chakraborty',
//             'photo': 'images/home/groomsmen-bridesmaids/img-3.jpg',
//             'relation': 'Sister',
//             calls: 'anna'
//        },
//         {
//             'name': 'Sunity Chakraborty',
//             'photo': 'images/home/groomsmen-bridesmaids/ishidad.jpg',
//             'relation': 'Father',
//             calls: 'baba'
//        },
//         {
//             'name': 'Sabarna Chakraborty',
//             'photo': 'images/home/groomsmen-bridesmaids/img-4.jpg',
//             'relation': 'Mother',
//             calls: 'maa'
//        }
//    ];
//
//
//     var photos = [
//         {
//
//             'caption': 'Image 1',
//             'src': 'gallery(1).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 2',
//             'src': 'gallery(2).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 3',
//             'src': 'gallery(3).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 4',
//             'src': 'gallery(4).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 5',
//             'src': 'gallery(5).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 6',
//             'src': 'gallery(6).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 7',
//             'src': 'gallery(7).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 8',
//             'src': 'gallery(8).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 9',
//             'src': 'gallery(9).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 10',
//             'src': 'gallery(10).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 11',
//             'src': 'gallery(11).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 12',
//             'src': 'gallery(12).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 13',
//             'src': 'gallery(13).jpeg',
//             'group': 'ishita'
//        },
//         {
//             'caption': 'Image 14',
//             'src': 'gallery(14).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 15',
//             'src': 'gallery(15).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 16',
//             'src': 'gallery(16).jpeg',
//             'group': 'akshay'
//        },
//         {
//             'caption': 'Image 17',
//             'src': 'gallery(17).jpeg',
//             'group': 'akshay'
//        },
//    ];


//
//
//
//     var arrno = 0;
//     var harrno = 0;
//     changehisfamily();
//     changeherfamily();
//
//     window.setInterval(function () {
//         changehisfamily();
//     }, 3000);
//     window.setInterval(function () {
//         changeherfamily();
//     }, 5000);
//
//     function changehisfamily() {
//         /// call your function here 
//         arrno = arrno + 1;
//         if (arrno > 6) {
//             arrno = 0;
//         };
//         $('#hisfamimg2').attr('src', hisfamily[arrno].photo);
//         $('#hisfamname2').html(hisfamily[arrno].name);
//         $('#hisfamcall2').html(hisfamily[arrno].calls);
//         $('#hisfamrelation2').html(hisfamily[arrno].relation);
//     };
//
//     function changeherfamily() {
//         /// call your function here 
//         harrno = harrno + 1;
//         if (harrno > 2) {
//             harrno = 0;
//         };
//         $('#hisfamimg3').attr('src', herfamily[harrno].photo);
//         $('#hisfamname3').html(herfamily[harrno].name);
//         $('#hisfamcall3').html(herfamily[harrno].calls);
//         $('#hisfamrelation3').html(herfamily[harrno].relation);
//     };




     $(".popupgobtn").click(function () {
         var flip = true;
         i++;
         var code = $('#invitecode').val();
         var codenumber = code.substring(code.length - 4, code.length);
         var name = code.substring(0, code.length - 4);
         var lastletter = name.substring(name.length - 1, name.length);
         var fly = false;
         if (lastletter == '7') {
             name = name.substr(0, name.length - 1);
             fly = true;
         };
         name = capitalizeFirstLetter(name);
         //HALDI CHECK
         if (codenumber == '2211') {
             var priority = 4;
             //            $('.popupoverlay').hide();
             window.scrollTo(0, 0);
         };
         if (codenumber == '1122') {
             var priority = 3;
             //            $('.popupoverlay').hide();
             window.scrollTo(0, 0);
         } else {
             //WEDDING CHECK
             if (codenumber == '4949') {
                 var priority = 2;
                 //                $('.popupoverlay').hide();
                 window.scrollTo(0, 0);
             } else {
                 //RECEPTION
                 if (codenumber == '9869') {
                     var priority = 1;
                     console.log(priority);
                     //                    $('.popupoverlay').hide();
                     window.scrollTo(0, 0);
                 } else {
                     flip = false;
                     if (i % 2 == 0) {
                         $('.popupinvalidcode').html('*please check again');
                     } else {
                         $('.popupinvalidcode').html('*Invalid code,please check again');
                     };

                 };
             };
         };

         if (flip) {


             document.getElementsByClassName("popup")[0].style.transform = "rotateY(0deg)";
             setTimeout(function () {
                 $(".popupoverlay").addClass("animated fadeOutUp");
                 $(".body-div").addClass("disp-block animated slideInUp");
                 var center = $(window).width() / 2;
                 var btnFromLeft = $(".yes-btn").offset().left + $(".yes-btn").width();
                 var distCtoBtn = center - btnFromLeft;

                 console.log(btnFromLeft);
                 console.log(distCtoBtn);

                 var perpixchange = 90 / distCtoBtn;

                 $(document).on("mousemove", function (event) {
                     var dist = event.pageX - center;
                     if (Math.abs(dist) <= distCtoBtn) {
                         var angle = dist * perpixchange;
                         angle = 90 + angle;
                         $('.smily-mouth').css('transform', 'rotateX(' + angle + 'deg)');
                     } else {
                         if (dist < 0) {
                             $('.smily-mouth').css('transform', 'rotateX(0deg)');
                         } else {
                             $('.smily-mouth').css('transform', 'rotateX(180deg)');
                         };
                     }
                 });

                 /* OPEN RSVP HEART BUTTON ON SCROLL */
                 var heartbtnopened = false;
                 $(window).scroll(function () {
                     if (!heartbtnopened) {
                         if ($(window).scrollTop() > ($('.rsvp-h-btn').offset().top - 400)) {
                             heartbtnopened = true;
                             $('.rsvp-h-btn').removeClass('heart-beat');
                             $('.rsvp-h-yes').addClass('rsvp-h-yes-pos');
                             $('.rsvp-h-no').addClass('rsvp-h-no-pos');
                         }
                     }
                 });

                 /* WHEN CLICKED ON HEART YES-NO BUTTON */
                 $('.h-btn').click(function () {
                     $('.rsvp-h-btn').css('transform', 'scale(0)');
                     setTimeout(function () {
                         $('.h-btn').hide();
                         $('.h-message').show();
                         $('.rsvp-h-btn').css('transform', 'scale(1)');
                     }, 1000);
                 });









                 var photostr = '';
                 for (var q = 0; q < photos.length; q++) {
                     photostr = photostr + '<div class="photo-div col col-xs-6 col-sm-3 item" data-groups=\'["' + photos[q].group + '"]\'><a onclick="onClick(this)" data-lightbox-gallery="gallery1"><div class="img-caption">' + photos[q].caption + '</div><div class="overlay"></div><img src="images/home/gallery/' + photos[q].src + '" alt="Gallery image" class="img img-responsive" ></a></div>';
                 };
                 $('#shuffle-wrapper').html(photostr);


                 setTimeout(function () {
                     //SET PHOTO SLIDER 

                     //GALLERY INITIALIZATION

                     // shuffle
                     var $main = $('#shuffle-wrapper');
                     $main.shuffle({
                         itemSelector: '.item' // the selector for the items in the grid
                     });




                 }, 0);

                 /* reshuffle when user clicks a filter item */
                 $('#filter a').click(function (e) {
                     e.preventDefault();

                     // set active class
                     $('#filter a').removeClass('active');
                     $(this).addClass('active');

                     // get group name from clicked item
                     var groupName = $(this).attr('data-group');

                     // reshuffle grid
                     $('#shuffle-wrapper').shuffle('shuffle', groupName);
                 });

             }, 1500);





             //            //                document.getElementsByClassName("body-div").classList.add("animated", "slideInUp", "body-div-show");
             //            
             //        
             //
             //            function foldanimation() {
             //
             //                //var folded = new OriDomi(document.getElementsByClassName('popupoverlay')[0]);
             //                var folded = new OriDomi('.popup', {
             //  vPanels:         5,     // number of panels when folding left or right (vertically oriented)
             //  hPanels:         3,     // number of panels when folding top or bottom
             //  speed:           1200,  // folding duration in ms
             //  ripple:          2,     // backwards ripple effect when animating
             //  shadingIntesity: .5,    // lessen the shading effect
             //  perspective:     800,   // smaller values exaggerate 3D distortion
             //  shading:         'soft' // change the shading type
             //});
             //
             //               folded.foldUp();
             //                
             //                setTimeout(function(){
             //                    $('.popupoverlay').remove();
             //                }, 2000);
             //                
             //            }


         }


         if (priority > 0) {
             map1(19.207955, 72.977376);
             //name setting
             if (fly == true) {
                 console.log(name);
//                 $('.inviname').html(name + ' & fly');
             } else {
                 $('.invite-name').html('Hey ' + name);
                 
             };

             if (priority == 4) {
                 $("#ishihaldi").html('13th July');
                 $('#ishihalditime').html('');
                 $('#haldiaddress').html('Swastik Garden Club House, Pokhran rd. 2, Thane West');
                 $('#haldidesc').html('Join us as we begin the celebration of the marraige of Ishita.');
             };

             if (priority < 3) {
                 console.log('wedding check');


                 //wedding check
                 $('.haldidate').hide();
                 $('.date').html('13.07.2016');
                 if (priority < 2) {
                     console.log('reception check');
                     //reception checkdate
                     $('#ceremonyaddress').html('Raheja Plaza, Near R-City Mall, </br> next to Kalpataru Aura, LBS Marg, </br> Ghatkopar West');
                     $('#bottomdate').html('15th July 2016');
                     $('.weddingdate').hide();
                     $('.date').html('15.07.2016');

                     map1(19.095044, 72.917962);
                 };
             };

         };
     });

     function capitalizeFirstLetter(string) {
         return string.charAt(0).toUpperCase() + string.slice(1);
     };






     // set the google map
     function map1(latitude, longitude) {
         console.log("setting map");
         var myLatLng = new google.maps.LatLng(latitude, longitude);
         var mapProp = {
             center: myLatLng,
             zoom: 18,
             scrollwheel: false,
             mapTypeId: google.maps.MapTypeId.ROAD
         };
         var map = new google.maps.Map(document.getElementById("googleMap-1"), mapProp);

         var marker = new google.maps.Marker({
             position: myLatLng,
             icon: 'images/home/marker.png'
         });

         marker.setMap(map);
         map.set('styles', [{
             "featureType": "water",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#d85665"
                }, {
                 "lightness": 17
                }]
            }, {
             "featureType": "landscape",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#f1dee2"
                }, {
                 "lightness": 20
                }]
            }, {
             "featureType": "road.highway",
             "elementType": "geometry.fill",
             "stylers": [{
                 "color": "#fff"
                }, {
                 "lightness": 17
                }]
            }, {
             "featureType": "road.highway",
             "elementType": "geometry.stroke",
             "stylers": [{
                 "color": "#fff"
                }, {
                 "lightness": 29
                }, {
                 "weight": 0.2
                }]
            }, {
             "featureType": "road.arterial",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#ffffff"
                }, {
                 "lightness": 18
                }]
            }, {
             "featureType": "road.local",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#d83547"
                }, {
                 "lightness": 16
                }]
            }, {
             "featureType": "poi",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#f5f5f5"
                }, {
                 "lightness": 21
                }]
            }, {
             "featureType": "poi.park",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#dedede"
                }, {
                 "lightness": 21
                }]
            }, {
             "elementType": "labels.text.stroke",
             "stylers": [{
                 "visibility": "on"
                }, {
                 "color": "#ffffff"
                }, {
                 "lightness": 16
                }]
            }, {
             "elementType": "labels.text.fill",
             "stylers": [{
                 "saturation": 36
                }, {
                 "color": "#333333"
                }, {
                 "lightness": 40
                }]
            }, {
             "elementType": "labels.icon",
             "stylers": [{
                 "visibility": "off"
                }]
            }, {
             "featureType": "transit",
             "elementType": "geometry",
             "stylers": [{
                 "color": "#f2f2f2"
                }, {
                 "lightness": 19
                }]
            }, {
             "featureType": "administrative",
             "elementType": "geometry.fill",
             "stylers": [{
                 "color": "#fefefe"
                }, {
                 "lightness": 20
                }]
            }, {
             "featureType": "administrative",
             "elementType": "geometry.stroke",
             "stylers": [{
                 "color": "#fefefe"
                }, {
                 "lightness": 17
                }, {
                 "weight": 1.2
                }]
            }]);
     };

 });
