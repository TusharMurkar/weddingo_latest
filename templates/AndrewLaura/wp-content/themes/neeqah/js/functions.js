(function($) {
  'use strict';

  // Main Menu
  $('#main-menu ul.main').superfish({
      delay: 1,
      animation:   {
          opacity: 'show', 
          height: 'show'
      },
      speed: 'fast',
      dropShadows: false,
      onBeforeShow: function() {
          if($(this).parents("ul").length > 1){
              var w = $(window).width();  
              var ul_offset = $(this).parents("ul").offset();
              var ul_width = $(this).parents("ul").outerWidth();

              // Shouldn't be necessary, but just doing the straight math
              // on dimensions can still allow the menu to float off screen
              // by a little bit.
              ul_width = ul_width + 50;

              if((ul_offset.left+ul_width > w-(ul_width/2)) && (ul_offset.left-ul_width > 0)) {
                 $(this).addClass('too_narrow_fix');
              }
              else {
                 $(this).removeClass('too_narrow_fix');
              }
          };
      }
  });

  // Countdown - Countup
  if( $('#countdown').length > 0 ) {
      var timeTarget = new Date(_warrior.countdown_time),
          timeCurrent = new Date(),
          timeDiff = (timeTarget.getTime()) - (timeCurrent.getTime());

      if ( timeDiff  > 0 ) {
          $('#countdown').countdown({
              until: timeTarget,
              format: 'YODHMS',
              layout: $('#timer').html()
          });
      } else {
          $('#countdown-widget .section-title').text(_warrior.countup_title);
          $('#countdown').countdown({
              since: timeTarget,
              format: 'YODHMS',
              layout: $('#timer').html()
          });
      }
  }

  // Animate
  // $('.home-events article.hentry, .home-blog article.hentry, .groom-tweets, .bride-tweets').addClass('wow fadeIn');
  // $('.timeline ul li.odd, .about_couple .groom').addClass('wow fadeInLeft');
  // $('.about_couple .bride, .timeline ul li.even').addClass('wow fadeInRight');
  // $('.section-title, .posts-grid ul li').addClass('wow fadeIn');
  $('.section-title .icon, .wedding-info .and').addClass('wow pulse');
  $('.wedding-info .name, .wedding-info .section-title .section-subtitle').addClass('wow fadeIn');
  $('#couple ul li .thumbnail').addClass('wow bounce');
  $('.posts-grid ul li, ul.gallery.grid li, ul.grid li').addClass('wow fadeInLeft');

  var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        delay: 200,
        offset: 0,
        mobile: false
  });
  wow.init();

  // Strecth header background
  if ( _warrior.bg_header != '' ) {
      $('#main-slider.static').backstretch( _warrior.bg_header, {centeredY: false} );
  }

  // Gallery Mixitup
  $('ul.grids').mixitup();

  // Mobile Menu
  $('#main-menu ul.main').mobileMenu({
      defaultText: 'Menu',
      className: 'select-menu',
      subMenuDash: '&ndash;'
  });
   
  $('.thumbnail').hover(function() {
      $(this).find('.overlay').stop().fadeToggle();
  });

  var eventsThumb = $('.thumbnail').outerWidth();
  $('.events li').hover(function() {
      $(this).find('.thumbnail img').stop().animate({
        left  : 0
      })
      $(this).find('.overlay').stop().addClass("hovered");
  }, function() {
      $(this).find('.thumbnail img').stop().animate({
        left  : -eventsThumb
      })
      $(this).find('.overlay').stop().removeClass("hovered");
  });

  // Full screen Sliders
  $('#main-slider').superslides({
      play: 5000,
      animation: 'fade',
      pagination: false,
      inherit_height_from: '#main-slider'
  });

  // Guestbook slider
  if( $('.home-widget #guestbook').length > 0 ) {
      $('.home-widget #guestbook').owlCarousel({
          items: 3,
          navigation: true,
          navigationText: false,
          pagination: false,
          itemsDesktop: [1000,3],
          itemsDesktopSmall: [900,2],
          itemsTablet: [600,1],
          itemsMobile: [480,1]
      });
  }

  var menu = $('#header'),
      pos = menu.outerHeight(),
      winHeight = $(window).height();

  $(menu).addClass('default');

  $('.dropdown').hide();

  $('nav#main-menu ul li').hover(function() {
      if ($(this).find('.dropdown').length > 0 ) {
          $(this).addClass('hovered');
          $(this).find('.dropdown').stop().slideDown(200);
      } else {
         
      }
  }, function() {
      var li = $(this);
      setTimeout(function () {
          li.removeClass('hovered');
      }, 250);
      $(this).find('.dropdown').stop().slideUp(200);
  });

  // Comments
  $('.comment-reply-title').wrapInner('<span></span>');

  // Resize main background
  resizeWindow();

  function resizeWindow(e) {
      // Opacity background height
      var mainSliderHeight = $('#main-slider .slides-control').outerHeight();
      $('.bg-opacity').css('height', mainSliderHeight);

      // Grid height
      var gridItem = $('ul.grid li').outerHeight();
      $('.home-widget.home-gallery .section-title, .home-widget.home-blog .section-title').css('height', gridItem);
  };
  $(window).bind('resize', resizeWindow);

  $(window).load(function() {
      // Grid height
      var gridItem = $('ul.grid li').outerHeight();
      var gridItemHeight = gridItem+'px';
      $('.home-widget.home-gallery .section-title, .home-widget.home-blog .section-title').css('height', gridItemHeight);

      // Posts grid height
      $('.posts-grid li').responsiveEqualHeightGrid();
      
      // Lightbox
      $('dl.gallery-item dt a[href*=".jpg"], dl.gallery-item dt a[href*=".jpeg"], dl.gallery-item dt a[href*=".png"], dl.gallery-item dt a[href*=".gif"]').addClass('lightbox'); 
      $('.lightbox').magnificPopup({
          type:'image',
          showCloseBtn: true
      });

      // If Google Map widget is loaded
      if( $('#map-wrapper').length > 0 ) {
        initializeMap();
      }
      // Opacity background height
      var mainSliderHeight = $('#main-slider .slides-control').outerHeight();
      $('.bg-opacity').css('height', mainSliderHeight);
  });

  // Google Map
  function initializeMap() {
    var name = $("#map-wrapper").data("map-name"),
      address = $("#map-wrapper").data("map-address"),
      image = $("#map-wrapper").data("map-image"),
      lat = $("#map-wrapper").data("map-lat"),
      lng = $("#map-wrapper").data("map-lng"),
      zoom = $("#map-wrapper").data("map-zoom");
    
    var infoWindow = new google.maps.InfoWindow;
    var html = "<div class='map-thumbnail'><img src='" + image + "' width='60' /></div><div class='map-detail'><b>" + name + "</b> <br/>" + address + '</div><div style="clear:both;"></div></div>';
    
    if ( lat !== "" && lng !== "" ) {
      var isDraggable = $(document).width() > 480 ? true : false;
      var map = new google.maps.Map(document.getElementById("map-wrapper"), {
        center: new google.maps.LatLng(lat,lng),
        zoom: zoom,
        mapTypeId: 'roadmap',
        scrollwheel: false,
        draggable: isDraggable
      });

      var marker = new google.maps.Marker({
        map: map,
        position: map.getCenter(),
        icon: _warrior.map_marker_icon,
        shadow: _warrior.map_marker_shadow
      });
      
      bindInfoWindow(marker, map, infoWindow, html);
    } else {
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode( { 'address': address}, function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          var isDraggable = $(document).width() > 480 ? true : false;
          var map = new google.maps.Map(document.getElementById("map-wrapper"), {
            center: results[0].geometry.location,
            zoom: zoom,
            mapTypeId: 'roadmap',
            scrollwheel: false,
            draggable: isDraggable
          });

          var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location,
            icon: _warrior.map_marker_icon,
            shadow: _warrior.map_marker_shadow
          });

          bindInfoWindow(marker, map, infoWindow, html);
        }
      });
    }
    }
  
   function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
      
      infoWindow.setContent(html);
      infoWindow.open(map, marker);
    }

})(jQuery);