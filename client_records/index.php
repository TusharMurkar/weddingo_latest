<?php
$string = file_get_contents("../clientdata.json");
$json=json_decode($string);
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style type="text/css">
		*{
			margin: 0;
			padding: 0;
		}
		body{
			font-size: 18px;
		}
		.container{
			max-width: 1200px;
			margin: 20px auto;
		}

		.client-wrapper{
			margin: 20px 0;
		}

		@media only screen and (min-width: 600px) {
			.container{
				width: 100%;
			}
		}
	</style>
</head>
<body>
	<div class="container">

		<h2>Weddingo Clients</h2>
<?php
	for($x=0;$x < count($json); $x++){
?>

	<div class="client-wrapper">Name : <?php print_r($json[$x]->name); ?></div>
	<div class="client-wrapper">Client : <?php print_r($json[$x]->client); ?></div>
	<div class="client-wrapper">Email : <?php print_r($json[$x]->email); ?></div>
	<div class="client-wrapper">Contact : <?php print_r($json[$x]->contact); ?></div>
	<br>
	<br>

<?php
}
?>
</div>
</body>
</html>