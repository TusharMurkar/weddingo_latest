console.log("TEST");

var linkregex = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;

if (document.getElementsByTagName) {
    var plinks = document.getElementsByTagName("plink");
} else {
    console.log("I DO NOT SUPPORT getElementsByTagName");
}

var plinks = document.getElementsByTagName("plink");


console.log(plinks);
console.log(plinks.length);
console.log(plinks[0]);

for (var index = 0; index < plinks.length; index++) {

    plinks[index].style.cursor = "pointer";
    plinks[index].addEventListener("click", function (e) {

        console.log(e);

        if (e.originalTarget) {
            var el = e.originalTarget;
        };
        if (e.toElement) {
            var el = e.toElement;
        };
        console.log(el.attributes['redirect-to'].nodeValue);
        console.log(el.attributes['0']);
        console.log(el.attributes);


        var link = el.attributes['redirect-to'].nodeValue;
        if (link != '' && link.match(linkregex) != null) {
            window.open(el.attributes['redirect-to'].nodeValue, "_blank");
        };
    }, true);

};




/********************************************FUNCTION FROM MAIN JS**********************************************************************/

//(function ($) {
//
//    'use strict';
//
//    new WOW().init();
//
//    /*-----------------------------------------------------------------------------------*/
//    /* Document Ready
//    /*-----------------------------------------------------------------------------------*/
//    function animateIt() {
//
//        //Animations
//        setTimeout(function () {
//            $('#header.animated').addClass('fadeInDown')
//        }, 360);
//
//    }
//    $('#primary-menu li').addClass('menu__item');
//
//    if ($('#preloader')[0]) {
//        $(window).load(function () {
//            $('#status').fadeOut();
//            $('#preloader').delay(350).fadeOut('fast');
//            $('body').delay(350).css({
//                'overflow': 'visible'
//            });
//            animateIt();
//        });
//    }
//    $(window).scroll(function () {
//        if ($(this).scrollTop() > 0.1) {
//            $('.sticky-header-wrap.header_fixed_noscroll').addClass("show");
//            $('.sticky-header-gap.header_fixed_noscroll').addClass("show");
//            $('.sticky-header-wrap.header_fixed_scroll').addClass("scrolled");
//        } else {
//            $('.sticky-header-wrap.header_fixed_noscroll').removeClass("show");
//            $('.sticky-header-gap.header_fixed_noscroll').removeClass("show");
//            $('.sticky-header-wrap.header_fixed_scroll').removeClass("scrolled");
//        }
//    });
//
//    var windowWidth = $(window).width();
//    var windowHeight = $(window).height();
//
//    function nikahFullscreen() {
//        var contentWrapper = $('.error404 #content-wrapper .outer');
//        var headerWrapper = $('.error404 #header').height();
//        var footerWrapper = $('.error404 #footer').height();
//
//        contentWrapper.css('height', windowHeight - headerWrapper - footerWrapper);
//    }
//
//    window.onload = function () {
//        nikahFullscreen();
//    };
//
//    $(window).load(function () {
//
//        var $gridMain = $('.blog .main-blog-loop').imagesLoaded(function () {
//            // init Masonry after all images have loaded
//            $gridMain.isotope({
//                transitionDuration: '0.65s',
//                initLayout: true,
//                columnWidth: 'article',
//                itemSelector: 'article',
//                fitWidth: true,
//                stagger: 30,
//            });
//        });
//
//        // Infinite Scroll
//        $gridMain.infinitescroll({
//            loading: {
//                finishedMsg: 'There is no more',
//                msgText: 'loading',
//                speed: 'normal'
//            },
//
//            state: {
//                isDone: false
//            },
//            navSelector: '#load-more-loop',
//            nextSelector: '#load-more-loop a',
//            itemSelector: '.blog .main-blog-loop article',
//
//        }, function (manpost1Block) {
//            var $newElems = $(manpost1Block).css({
//                opacity: 0
//            });
//            $newElems.imagesLoaded(function () {
//                $newElems.animate({
//                    opacity: 1
//                });
//                $gridMain.isotope('appended', $newElems, true);
//            });
//            $('.blog .main-blog-loop').css('margin-bottom', 0);
//        });
//
//        $gridMain.infinitescroll('unbind');
//        $("#load-infinite-loop").on('click', function () {
//            $gridMain.infinitescroll('retrieve');
//
//            $('.blog .main-blog-loop').css('margin-bottom', 120);
//
//            $(window).resize(function () {
//                $container.isotope('layout');
//            });
//            return false;
//        });
//
//    });
//})(jQuery);


//  all functions 


//view invitations page
  function invitations() {
      gtag('event', 'view-invitations', {
      'event_category': 'Exploration'
      
      });

  }

  //view couple name generator page
  function couplename() {
      gtag('event', 'visit-couplename', {
      'event_category': 'Engagement'
      
      });

  }

  //click on contact us
  function approach(type) {
      gtag('event', 'contacted', {
      'event_category': 'Approach',
      'type': type
    
      });

  }

  //want to purchase
  function purchase(price) {
      gtag('event', 'interested', {
      'event_category': 'Ecommerce',
      'price': price
      
      });

  }



  //view our couples page
  function feedback() {
      gtag('event', 'feedback', {
      'event_category': 'Couples'
     
      });

  }

  //view individual weddingo
  function weddingo(link) {
      gtag('event', 'visit-weddingo', {
      'event_category': 'Couples',
      'link':link
      });

  }

  //view demo template (preview)
  function demo(link) {
      gtag('event', 'visit-demo', {
      'event_category': 'Exploration',
      'link': link 
      });

  }






//   end functions
